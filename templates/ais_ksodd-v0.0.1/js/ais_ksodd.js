//**********************************************************************************************************************
//*                                                  AIS KSODD MAPBOX                                                  *
//*                               All rights reserved © 2018 | MosgortransNIIproekt, SUE                               *
//**********************************************************************************************************************

// Conventions:
//      cnt - count
//      f   - flag
//      k   - key
//      d   - dat
//      fn  - function
//      itm - item
//      w   - window
//      dft - draft/project
//      z   - zoom
//      c   - center
//      bb  - bounding box
//      btn - button
//      inp - input

(function(w) {
    let App = w.App || {};
    const PATH = "../templates/ais_ksodd-v0.0.1/";
    const DOMAIN = "https://niikeeper.com/ksoddAPI/v2.1.0/";
    const DIVISIONS = {
        "ЦАО": {
            "name": "Центральный АО",
            "districts": ["Арбат", "Басманный район", "Замоскворечье", "Красносельский район", "Мещанский район", "Пресненский район", "Таганский район", "Тверской район", "Хамовники", "Якиманка"]
        },
        "САО": {
            "name": "Северный АО",
            "districts": ["Аэропорт", "Беговой", "Бескудниковский район", "Войковский район", "Восточное Дегунино", "Головинский район", "Дмитровский район", "Западное Дегунино", "Коптево", "Левобережный", "Молжаниновский район", "Савёловский район", "Сокол", "Тимирязевский район", "Ховрино", "Хорошёвский район"]
        },
        "СВАО": {
            "name": "Северо-Восточный АО",
            "districts": ["Алексеевский район", "Алтуфьевский район", "Бабушкинский район", "Бибирево", "Бутырский район", "Лианозово", "Лосиноостровский район", "Марфино", "Марьина Роща", "Останкинский район", "Отрадное", "Ростокино", "Свиблово", "Северный", "Северное Медведково", "Южное Медведково", "Ярославский район"]
        },
        "ВАО": {
            "name": "Восточный АО",
            "districts": ["Богородское", "Вешняки", "Восточный", "Восточное Измайлово", "Гольяново", "Ивановское", "Измайлово", "Косино - Ухтомский", "Метрогородок", "Новогиреево", "Новокосино", "Перово", "Преображенское", "Северное", "Измайлово", "Соколиная Гора", "Сокольники"]
        },
        "ЮВАО": {
            "name": "Юго-Восточный АО",
            "districts": ["Выхино - Жулебино", "Капотня", "Кузьминки", "Лефортово", "Люблино", "Марьино", "Некрасовка", "Нижегородский район", "Печатники", "Рязанский район", "Текстильщики", "Южнопортовый район"]
        },
        "ЮАО": {
            "name": "Южный АО",
            "districts": ["Бирюлёво Восточное", "Бирюлёво Западное", "Братеево", "Даниловский район", "Донской район", "Зябликово", "Москворечье - Сабурово", "Нагатино - Садовники", "Нагатинский", "Затон", "Нагорный район", "Орехово - Борисово Северное", "Орехово - Борисово Южное", "Царицыно", "Чертаново Северное", "Чертаново Центральное", "Чертаново Южное"]
        },
        "ЮЗАО": {
            "name": "Юго-Западный АО",
            "districts": ["Академический район", "Гагаринский район", "Зюзино", "Коньково", "Котловка", "Ломоносовский район", "Обручевский район", "Северное Бутово", "Тёплый Стан", "Черёмушки", "Южное Бутово", "Ясенево"]
        },
        "ЗАО": {
            "name": "Западный АО",
            "districts": ["Внуково", "Дорогомилово", "Крылатское", "Кунцево", "Можайский район", "Ново - Переделкино", "Очаково - Матвеевское", "Проспект Вернадского", "Раменки", "Солнцево", "Тропарёво - Никулино", "Филёвский Парк", "Фили - Давыдково"]
        },
        "СЗАО": {
            "name": "Северо-Западный АО",
            "districts": ["Куркино", "Митино", "Покровское - Стрешнево", "Северное Тушино", "Строгино", "Хорошёво - Мнёвники", "Щукино", "Южное Тушино"]
        },
        "ЗелАО": {
            "name": "г. Зеленоград",
            "districts": ["Матушкино", "Савёлки", "Старое Крюково", "Силино", "Крюково"]
        }
    };
    const FLAG = false;
    App.PATH = PATH;
    App.DOMAIN = DOMAIN;
    App.DIVISIONS = DIVISIONS;
    App.FLAG = FLAG;
    let auth = new App.Authorization;
    let ksodd = new App.Ksodd;
    let path = w.location.pathname.split("/")[w.location.pathname.split("/").length - 1];
    (path === auth.serverLocation(FLAG).auth ?
        auth.authentication(submit, login, pass, auth.serverLocation(FLAG)) :
        (path === auth.serverLocation(FLAG).mapbox ?
            auth.verification("token", auth.serverLocation(FLAG), ksodd) :
            (path === auth.serverLocation(FLAG).leaflet ?
                console.log("leaflet") :
                alert("Что-то пошло не так...")
            )
        )
    );
    w.App = App;
})(window);


// function downloadKsodd(projectPath, domain, token) {
//     if (!localStorage.getItem("oldToken")) {
//         localStorage.setItem("oldToken", "xMKNeZPavLESGxkSJlGeDg8PwIYS54yz")
//     }
//
//     // Getting DOM elements
//     let menu = document.getElementById("menu");
//
//

//


//     map.on("load", function () {
//         (async () => {
//             try {
//                 // load divisions GeoJSON
//                 let responseDiv = await fetch("https://niikeeper.com/nii_api/v0.5.9/get_polygon?token=" + localStorage.getItem("oldToken") + "&type=divisions");
//                 let data = await responseDiv.json();
//                 data.Features.forEach(function (d) {
//                     divObj.features.push(d);
//                 });
//                 // load divisions count information
//                 let responseCnt = await fetch("https://niikeeper.com/nii_api/v0.5.9/get_divisions_cnt?token=" + localStorage.getItem("oldToken"));
//                 let dataCnt = await responseCnt.json();
//                 // set divObj properties
//                 for (let k in dataCnt.Summary) {
//                     divObj.features.forEach(function (d) {
//                         if (d.properties.name === k) {
//                             d.properties.cnt = {};
//                             d.properties.cnt = dataCnt.Summary[k];
//                         }
//                     });
//                 }
//                 divObj.features.forEach(function (d) {
//                     map.addSource(d.properties.name, {
//                         type: "geojson",
//                         data: d
//                     });
//                 });
//             } catch (e) {
//                 console.log(e);
//             }
//         })();
//         (async () => {
//             try {
//                 // load districts GeoJSON
//                 let responseDiv = await fetch("https://niikeeper.com/nii_api/v0.5.9/get_polygon?token=" + localStorage.getItem("oldToken") + "&type=districts");
//                 let data = await responseDiv.json();
//                 data.Features.forEach(function (d) {
//                     distObj.features.push(d);
//                 });
//                 // load districts count information
//                 let responseCnt = await fetch("https://niikeeper.com/nii_api/v0.5.9/get_districts_cnt?token=" + localStorage.getItem("oldToken"));
//                 let dataCnt = await responseCnt.json();
//                 // set distObj properties
//                 for (let k in dataCnt.Summary) {
//                     distObj.features.forEach(function (d) {
//                         if (d.properties.name === k) {
//                             d.properties.cnt = {};
//                             d.properties.cnt = dataCnt.Summary[k];
//                         }
//                     });
//                 }
//                 distObj.features.forEach(function (d) {
//                     map.addSource(d.properties.name, {
//                         type: "geojson",
//                         data: d
//                     });
//                 });
//                 menu.firstChild.src = projectPath + "img/menu_l.svg";
//             } catch (e) {
//                 console.log(e);
//             }
//         })();
//         map.addSource("divisionsAll", {
//             type: "geojson",
//             data: projectPath + "js/json/divisions.js"
//         });
//         map.addSource("districtsAll", {
//             type: "geojson",
//             data: projectPath + "js/json/districts.js"
//         });
//         loadLeftPanel(divisions, map, divObj, distObj);
//         (async () => {
//             try {
//                 let response = await fetch(domain + "segmentBBoxData", {
//                     method: "POST",
//                     headers: {
//                         "Authorization": "Bearer " + token
//                     },
//                     body: JSON.stringify({
//                         "racks": false,
//                         "stretches": false,
//                         "lineMarkup": false,
//                         "textLabelsLineMarkup": true,
//                         "dotMarkup": false,
//                         "names": ["620_746", "726_746", "628_668_746", "630_746", "716_746", "746_(668-726)", "746_(726-630)", "746_(608-620)", "746_(630-716)", "746_(716-635)", "746_(620-668)"]
//                     })
//                 });
//                 let data = await response.json();
//                 console.log(data);
//                 for (let k in data) {
//                     let i = 0;
//                     data[k].textLabelsLineMarkup.features.forEach(function (d) {
//                         map.addSource(k + "_" + i, {
//                             type: "geojson",
//                             data: d
//                         });
//                         // let mark = document.createElement("div");
//                         // mark.innerHTML = d.properties.text;
//                         // mark.className = "marker";
//                         // // console.log(mark);
//                         // // mark.style.width = "max-content";
//                         // // mark.style.transformOrigin = "left bottom";
//                         // mark.style.transform = "rotate(45deg)";
//                         // new mapboxgl.Marker(mark, {
//                         //     "rotation-alignment": "map"
//                         // }).setLngLat(d.geometry.coordinates[1]).addTo(map);
//                         // // console.log(label);
//                         let ang = (d.properties.a * 180 / Math.PI);
//                         // let markerData = "<svg width='14' height='14' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg' version='1.1'><polygon stroke='black' stroke-width='1' points='20,90 50,10 80,90 50,70' transform='rotate(" + ang + ", 50, 50)'/></svg>";
//                         // let markerData = "<svg width='14' height='14' viewBox='0 0 100 14' xmlns='http://www.w3.org/2000/svg' version='1.1'><text x=7 y=7 transform='rotate(" + ang + ", 50, 50)'>Hello!</text></svg>";
//
//                         // let markerData = "<svg width='100' height='20' viewBox='0 0 100 20' xmlns='http://www.w3.org/2000/svg' version='1.1'><text style='font-size:10px' x='0' y='10'>Hello</text></svg>";
//                         // let el = document.createElement('div');
//                         // el.innerHTML = d.properties.text;
//                         // // el.className = 'marker';
//                         // // el.style.backgroundImage = "url(data:image/svg+xml;base64," + btoa(markerData) + ')';
//                         // // el.style.width = "70px";
//                         // el.style.height = "20px";
//                         // new mapboxgl.Marker(el, {
//                         //
//                         // }).setLngLat(d.geometry.coordinates[1]).addTo(map);
//                         // let point = {
//                         //     "type": "point",
//                         //     "coordinates": d.geometry.coordinates[1]
//                         // };
//                         map.addSource(k + "_" + i + "s", {
//                             "type": "geojson",
//                             "data": {
//                                 "type": "Point",
//                                 "coordinates": d.geometry.coordinates[1]
//                             }
//                         });
//                         if ((map.getZoom() >= 19) && (!map.getLayer(k + "_" + i + "s")) && (!map.getLayer(k + "_" + i))) {
//                             if (map.getSource(k + "_" + i)) {
//                                 map.addLayer({
//                                     "id": k + "_" + i,
//                                     "type": "line",
//                                     "source": k + "_" + i,
//                                     "paint": {
//                                         "line-color": "#e254bd",
//                                         "line-opacity": 1,
//                                         "line-width": 1
//                                     }
//                                 });
//                             }
//                             // map.addLayer({
//                             //     "id": k + "_" + i + "s",
//                             //     "type": "symbol",
//                             //     "source": k + "_" + i + "s",
//                             //     "layout": {
//                             //         // "text-opyional": true,
//                             //         // "text-font": ["Lato Black"],
//                             //         // "text-padding": 20,
//                             //         // "text-anchor": "top",
//                             //         // // "text-offset": [0, -2.3],
//                             //         "text-size": 10,
//                             //         // "text-allow-overlap": true,
//                             //         // "text-field": "HEllo!",
//                             //         // "text-justify": "center",
//                             //         // "icon-offset": [0, -22],
//                             //         // "icon-image": "marker",
//                             //         // "icon-allow-overlap": true,
//                             //         "text-field": "Hello!",
//                             //         "text-rotate": ang,
//                             //         // "text-font": ["Lato Black"],
//                             //         // "text-padding": 20,
//                             //         // "text-anchor": "top",
//                             //         // "text-offset": [0, -2.3],
//                             //         // "text-size": 14,
//                             //         "text-rotation-alignment": "map",
//                             //         // "icon-image": "airport-15",
//                             //         // "icon-rotation-alignment": "map",
//                             //         // "icon-rotate": 45
//                             //     },
//                             //     "paint": {
//                             //         "text-color": "#000"
//                             //     }
//                             // });
//                         }
//                         map.on("moveend", function() {
//                             if ((map.getZoom() >= 19) && (!map.getLayer(k + "_" + i + "s")) && (!map.getLayer(k + "_" + i))) {
//                                 if (map.getSource(k + "_" + i)) {
//                                     map.addLayer({
//                                         "id": k + "_" + i,
//                                         "type": "line",
//                                         "source": k + "_" + i,
//                                         "paint": {
//                                             "line-color": "#e254bd",
//                                             "line-opacity": 1,
//                                             "line-width": 1
//                                         }
//                                     });
//                                 }
//                                 // map.addLayer({
//                                 //     "id": k + "_" + i + "s",
//                                 //     "type": "symbol",
//                                 //     "source": k + "_" + i + "s",
//                                 //     "layout": {
//                                 //         "text-size": 10,
//                                 //         "text-field": "Hello!",
//                                 //         "text-rotate": ang,
//                                 //         "text-rotation-alignment": "map",
//                                 //     },
//                                 //     "paint": {
//                                 //         "text-color": "#000"
//                                 //     }
//                                 // });
//                             } else if (map.getZoom() < 19) {
//                                 // if (map.getLayer(k + "_" + i + "s")) {
//                                 //     map.removeLayer(k + "_" + i + "s");
//                                 // }
//                                 if (map.getLayer(k + "_" + i)) {
//                                     map.removeLayer(k + "_" + i);
//                                 }
//                             }
//                         });
//                         i++;
//
//                     });
//                 }
//             } catch (e) {
//                 console.log(e);
//             }
//         })();
//
//     });
// }