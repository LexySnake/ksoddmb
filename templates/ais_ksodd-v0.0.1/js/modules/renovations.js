function getRePopup(re) {
    let box = document.createElement("div");
    box.className = "segment_popup";
    let table = document.createElement("table");
    table.style.marginBottom = "0";
    table.innerHTML =
        "<tbody>" +
        "<tr>" +
        "<th>Описание:</th>" +
        "<td style=\"max-width:400px;\">" + re.properties.descriptio +
        "</tr>" +
        // "<tr>" +
        // "<th>Адрес:</th>" +
        // "<td style=\"max-width:400px;\">" + text(re.properties.address) + "</td>" +
        // "</tr>" +
        "</tbody>";
    box.appendChild(table);
    return box.outerHTML;
}
(function(w) {
    let App = w.App || {};
    function Renovations() {
        this.group = L.layerGroup();
        this.el = document.getElementById("renovations")
    }
    Renovations.prototype.download = function(map) {
        (async(meme) => {
            try {
                let response = await fetch("http://petiteweb.ru/renovations_11.json");
                let responseP = await fetch("http://petiteweb.ru/renovations_2.json");
                let data = await response.json();
                let dataP = await responseP.json();
                meme.el.addEventListener("click", function() {
                    console.log(data);
                });
                data.features.forEach(function(o) {
                    let popup = L.popup({
                        className: "mgtniip_popup ",
                        autoClose: true
                    }).setContent(getRePopup(o));
                    let circle = L.circle(L.latLng(o.geometry.coordinates[1], o.geometry.coordinates[0]), {
                        radius: getOlProperties(map.getZoom())[1],
                        weight: getOlProperties(map.getZoom())[0],
                        color: "#f00"
                    }).bindPopup(popup).addTo(meme.group);
                    map.on("zoomend", function () {
                        circle.setRadius(getOlProperties(map.getZoom())[1]);
                        circle.setStyle({
                            weight: getOlProperties(map.getZoom())[0]
                        });
                    });
                });
                dataP.features.forEach(function(o) {
                    let popup = L.popup({
                        className: "mgtniip_popup ",
                        autoClose: true
                    }).setContent(getRePopup(o));
                    L.geoJSON(o, {
                        onEachFeature: function (feature, polyline) {
                            polyline.setStyle({
                                weight: getOlProperties(map.getZoom())[0],
                                color: "#f00",
                                // fillColor: "#"
                            });
                            map.on("zoomend", function () {
                                polyline.setStyle({
                                    weight: getOlProperties(map.getZoom())[0]
                                });
                            });
                        }
                    }).bindPopup(popup).addTo(meme.group);
                });
                meme.el.addEventListener("click", function() {
                    if(meme.el.checked) {
                        meme.group.addTo(map);
                    } else {
                        meme.group.remove();
                    }
                });
            } catch(e) {
                throw new Error(e);
            }
        })(this);
        
    };
    App.Renovations = Renovations;
    w.App = App;
})(window);