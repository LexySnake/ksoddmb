function loadLeftPanel(divisions, map, divObj, distObj) {
    let divEl = document.getElementById("divisions_all");
    let distEl = document.getElementById("districts_all");
    let units = document.getElementById("control_units");
    divEl.addEventListener("click", function() {
        if (divEl.checked === true) {
            map.addLayer({
                "id": "divisions",
                "type": "line",
                "source": "divisionsAll",
                "paint": {
                    "line-width": divObj.properties.lineWidth,
                    "line-color": divObj.properties.lineAllColor
                }
            });
            if (units.hasChildNodes()) {
                for (let i = 0, cnt = units.children.length; i < cnt; i++) {
                    let unit = units.children[i].firstChild;
                    if (unit.classList.contains("div_active")) {
                        map.moveLayer("divisions", unit.id + "_fill");
                    }
                }
            }
        } else {
            map.removeLayer("divisions");
        }
    });
    distEl.addEventListener("click", function() {
        if (distEl.checked === true) {
            map.addLayer({
                "id": "districts",
                "type": "line",
                "source": "districtsAll",
                "paint": {
                    "line-color": "#800",
                    "line-opacity": 1,
                    "line-width": 1
                }
            });
        } else {
            map.removeLayer("districts");
        }
    });
    menu.addEventListener("click", function() {
        if (!units.hasChildNodes()) {
            divObj.features.forEach(function(d) {
                let divLine = document.createElement("div");
                divLine.className = "line";
                let divUnit = document.createElement("div");
                divUnit.className = "div_unit";
                divUnit.id = d.properties.name;
                divUnit.innerHTML = divisions[d.properties.name].name;
                let divInfo = document.createElement("div");
                divInfo.className = "info";
                let distBox = document.createElement("div");
                divisions[d.properties.name].districts.forEach(function(dist) {
                    let distLine = document.createElement("div");
                    distLine.className = "line";
                    let distUnit = document.createElement("div");
                    distUnit.className = "dist_unit";
                    distUnit.innerHTML = dist;
                    let distInfo = document.createElement("div");
                    distInfo.className = "info";
                    distLine.appendChild(distUnit);
                    distLine.appendChild(distInfo);
                    distBox.appendChild(distLine);
                });
                divUnit.addEventListener("click", function() {
                    let divSiblings = document.getElementsByClassName("div_active");
                    for (let i = 0, cnt = divSiblings.length; i < cnt; i++) {
                        if (divSiblings[i] !== divUnit) {
                            map.removeLayer(divSiblings[i].id + "_border");
                            map.removeLayer(divSiblings[i].id + "_line");
                            map.removeLayer(divSiblings[i].id + "_fill");
                            divSiblings[i].classList.remove("div_active");
                        }
                    }
                    divUnit.classList.toggle("div_active");
                    if (divUnit.classList.contains("div_active")) {
                        map.addLayer({
                            "id": d.properties.name + "_fill",
                            "type": "fill-extrusion",
                            "source": d.properties.name,
                            "paint": {
                                "fill-extrusion-color": divObj.properties.fill,
                                "fill-extrusion-height": 1,
                                "fill-extrusion-base": 1,
                                "fill-extrusion-opacity": 0.4
                            }
                        });
                        map.addLayer({
                            "id": d.properties.name + "_border",
                            "type": "line",
                            "source": d.properties.name,
                            "paint": {
                                "line-width": 5,
                                "line-color": "#fff"
                            }
                        });
                        map.addLayer({
                            "id": d.properties.name + "_line",
                            "type": "line",
                            "source": d.properties.name,
                            "paint": {
                                "line-width": divObj.properties.lineWidth,
                                "line-color": divObj.properties.lineUnitColor
                            }
                        });
                        // let coordinates = map.getSource(d.properties.name)._data.geometry.coordinates;
                        // let bounds = coordinates.reduce(function(bounds, coord) {
                        //     return bounds.extend(coord);
                        // });
                        // new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
                        // console.log(bounds);
                        // console.log(map.getSource(d.properties.name)._data.geometry.coordinates);
                        // map.fitBounds(bounds);
                        // map.fitBounds(d.properties.name + "_border");
                    } else {
                        map.removeLayer(d.properties.name + "_border");
                        map.removeLayer(d.properties.name + "_line");
                        map.removeLayer(d.properties.name + "_fill");
                    }
                });
                divUnit.addEventListener("mouseover", function() {
                    map.addLayer({
                        "id": d.properties.name + "_hover",
                        "type": "fill",
                        "source": d.properties.name,
                        "paint": {
                            "fill-color": divObj.properties.fillHover,
                            "fill-opacity": 0.3
                        }
                    });
                });
                divUnit.addEventListener("mouseout", function() {
                    map.removeLayer(d.properties.name + "_hover");
                });
                divLine.appendChild(divUnit);
                divLine.appendChild(divInfo);
                units.appendChild(divLine);
                units.appendChild(distBox);
            });
            acc();
            let maxW = 0;
            for (let i = 0, cnt = units.children.length; i < cnt; i++) {
                let unit = units.children[i].firstChild;
                maxW = Math.max(maxW, unit.offsetWidth);
            }
            for (let i = 0, cnt = units.children.length; i < cnt; i++) {
                let unit = units.children[i].firstChild;
                unit.style.width = maxW - 12 + "px";
                units.style.width = maxW + 14 + 8 + "px";
            }
        }
    });
}