(function(w) {
    localforage.setDriver([localforage.INDEXEDDB, localforage.WEBSQL, localforage.localStorage]);
    let App = w.App || {};
    let db = App.db || {};
    let segments = db.segments || localforage.createInstance({name: "segments"});
    function Ksodd() {
        this.divObj = {
            "type": "FeatureCollection",
            "features": [],
            "properties": {
                "lineWidth": 2,
                "lineAllColor": "#23bb6b",
                "lineUnitColor": "#49ee97",
                "fill": "#00ff79",
                "fillHover": "#23bb6b"
            }
        };
        this.distObj = {
            "type": "FeatureCollection",
            "features": [],
            "properties": {
                "lineWidth": 2,
                "lineAllColor": "#3362ab",
                "lineUnitColor": "#45aef8",
                "fill": "#00b4ff",
                "fillHover": "#3362ab"
            }
        };
        this.bb = {
            "_ne": {"lng": 37.967819, "lat": 56.020919},
            "_sw": {"lng": 36.888006, "lat": 55.490751}
        };
        this.segObj = {
            "type": "FeatureCollection",
            "features": [],
            "properties": {
                "lineWidth": 2,
                "lineAllColor": "#3362ab",
                "lineUnitColor": "#45aef8",
                "fill": "#00b4ff",
                "fillHover": "#3362ab"
            }
        };
        this.map = {};
        this.segAll = [];
    }
    Ksodd.prototype.getPage = function(dft) {
        // set user information
        document.getElementById("user_login").innerHTML = localStorage.getItem("user") || "MTP_user";
        document.getElementById("logout").addEventListener("click", function() {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            w.location = dft.serverLocation(App.FLAG).auth;
        });
        document.getElementById("leaflet_edition").addEventListener("click", function() {
            w.location = dft.serverLocation(App.FLAG).leaflet;
        });
        document.getElementById("mapbox_edition").addEventListener("click", function() {
            w.location = dft.serverLocation(App.FLAG).mapbox;
        });
        // download screen to png
        document.getElementById("png").firstChild.addEventListener("click", function() {
            document.getElementById("analytics").classList.toggle("hidden");
            let node = document.body;
            domtoimage.toPng(node, {style: {fontFaces: {fontFamily: "SegoeUInormal", src: "../templates/mtp-v0.0.1/fonts/SegoeUI/SegoeUI400.ttf"}, fontFamily: "SegoeUInormal", fontSize: "12px"}}).then(function(pic) {
                this.nextElementSibling.href = pic;
                this.nextElementSibling.click();
            }.bind(this));
        });
        // print page
        document.getElementById("print").addEventListener("click", function() {
            w.print();
        });

        // set path-link
        let link = document.getElementById("link");
        let linkPath = document.getElementById("link_path");
        link.addEventListener("click", function() {
            linkPath.value = w.location.href;
        });
        document.getElementById("copy_link").addEventListener("click", function() {
            linkPath.select();
            document.execCommand("copy");
        });
        console.log("Ку-ку!");
        this.setMap();

    };
    Ksodd.prototype.downloadAllSegments = function(arr) {
        (async(meme) => {
            try {
                console.time("getSegments");
                let segResponse = await fetch(App.DOMAIN + "segmentBBoxData", {
                    method: "POST",
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    },
                    body: JSON.stringify({
                        "racks": true,
                        "stretches": true,
                        "lineMarkup": true,
                        "textLabelsLineMarkup": true,
                        "dotMarkup": true,
                        "names": arr
                    })
                });
                let segData = await segResponse.json();
                let segStResponse = await fetch(App.DOMAIN + "getPolygon?type=segment_streets", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segStData = await segStResponse.json();
                let segIntResponse = await fetch(App.DOMAIN + "getPolygon?type=segment_intersections", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segIntData = await segIntResponse.json();
                let segCntResponse = await fetch(App.DOMAIN + "getSegmentsCount", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segCntData = await segCntResponse.json();
                segIntData.features.forEach(function(d) {
                    meme.segObj.features.push(d);
                });
                segStData.features.forEach(function(d) {
                    meme.segObj.features.push(d);
                });
                meme.segObj.features.forEach(function(d) {
                    d.properties["cnt"] = {};
                    d.properties["objects"] = {};
                    for (let k in segCntData.Summary) {
                        if (d.properties.name === k) {
                            d.properties["cnt"] = segCntData.Summary[k];
                        }
                    }
                    for (let k in segData) {
                        if (k === d.properties.name) {
                            d.properties["objects"] = segData[k];
                        }
                    }
                    if ((d.properties.name.split("_")[1]) && (d.properties.name.split("_")[1] !== "") && (d.properties.name.split("_")[1].slice(0,1) === "(")) {
                        d.properties["type"] = "Пролёт";
                    } else {
                        d.properties["type"] = "Перекрёсток";
                    }
                    segments.setItem(d.properties.name, d);
                });
                console.timeEnd("getSegments");
                this.loadMap();
            } catch(e) {
                throw new Error(e);
            }
        })(this);
    };
    Ksodd.prototype.downloadSingleSegment = function(segment) {
        (async(meme) => {
            try {
                console.time("getSingleSegment " + segment);
                meme.segObj = {
                    "type": "FeatureCollection",
                    "features": [],
                    "properties": {
                        "lineWidth": 2,
                        "lineAllColor": "#3362ab",
                        "lineUnitColor": "#45aef8",
                        "fill": "#00b4ff",
                        "fillHover": "#3362ab"
                    }
                };
                let segResponse = await fetch(App.DOMAIN + "segmentBBoxData", {
                    method: "POST",
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    },
                    body: JSON.stringify({
                        "racks": true,
                        "stretches": true,
                        "lineMarkup": true,
                        "textLabelsLineMarkup": true,
                        "dotMarkup": true,
                        "names": [segment]
                    })
                });
                let segData = await segResponse.json();
                let segStResponse = await fetch(App.DOMAIN + "getPolygon?type=segment_streets", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segStData = await segStResponse.json();
                let segIntResponse = await fetch(App.DOMAIN + "getPolygon?type=segment_intersections", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segIntData = await segIntResponse.json();
                let segCntResponse = await fetch(App.DOMAIN + "getSegmentData", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let segCntData = await segCntResponse.json();
                segStData.features.forEach(function(dS) {
                    if (dS.properties.name === segment) {
                        meme.segObj.features.push(dS);
                    } else {
                        segIntData.features.forEach(function(dI) {
                            if(dI.properties.name === segment) {}
                            meme.segObj.features.push(dI);
                        });
                    }
                });
                meme.segObj.features.forEach(function(d) {
                    if (d.properties.name === segment) {
                        d.properties["cnt"] = {};
                        d.properties["objects"] = {};
                        for (let k in segCntData.Summary) {
                            if (d.properties.name === k) {
                                d.properties["cnt"] = segCntData.Summary[k];
                            }
                        }
                        if ((segment.split("_")[1]) && (segment.split("_")[1] !== "") && (segment.split("_")[1].slice(0,1) === "(")) {
                            d.properties["type"] = "Пролёт";
                        } else {
                            d.properties["type"] = "Перекрёсток";
                        }
                        for (let k in segData) {
                            if (k === segment) {
                                d.properties["objects"] = segData[k];
                            }
                        }
                        segments.setItem(segment, d);
                    }
                });
                console.log(meme.segObj);
                console.timeEnd("getSingleSegment " + segment);

            } catch(e) {
                throw new Error(e);
            }
        })(this);
    };
    Ksodd.prototype.setMap = function() {
        mapboxgl.accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
        this.map = new mapboxgl.Map({
            container: "map",
            style: "mapbox://styles/mapbox/streets-v9",
            // style: {
            //     "version": 8,
            //     "sources": {
            //         "raster-tiles": {
            //             "type": "raster",
            //             "url": "mapbox://mapbox.streets",
            //             "tileSize": 256
            //         }
            //     },
            //     "glyphs": "mapbox://fonts/mapbox/{fontstack}/{range}.pbf",
            //     "layers": [{
            //         "id": "mbTile",
            //         "type": "raster",
            //         "source": "raster-tiles",
            //         "minzoom": 0,
            //         "maxzoom": 22
            //     }]
            // },
            center: [37.6155600, 55.7522200],
            // pitch: 90,
            bearing: 0,
            zoom: 11
        });
        let language = new MapboxLanguage({defaultLanguage: "ru"});
        this.map.addControl(language);
        let hash = w.location.hash.split("#")[1];
        let c = [];
        let z = 10;
        if (hash && hash.split(",")) {
            c = [hash.split(",")[0], hash.split(",")[1]];
            z = hash.split(",")[2];
        } else {
            c = [this.map.getCenter().lng, this.map.getCenter().lat];
            z = this.map.getZoom();
        }
        setPermalink(this.map, c, z, true);
    };
    Ksodd.prototype.download = function() {
        for (let k in App.refInfo.Result.segments) {
            this.segAll.push(App.refInfo.Result.segments[k]);
        }
        let x = 0;
        let newSegments = [];
        for (let i = 0, cnt = this.segAll.length; i < cnt; i++) {
            segments.getItem(this.segAll[i]).then(function(d) {
                if (!d) {
                    x++;
                    newSegments.push(this.segAll[i]);
                    // let ololo = function(seg) {
                    //     (async() => {
                    //         try {
                    //             let segCntResponse = await fetch(App.DOMAIN + "getSegmentData&segment_id=" + seg, {
                    //                 headers: {
                    //                     "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    //                 }
                    //             });
                    //             let segCntData = await segCntResponse.json();
                    //             console.log(segCntData)
                    //         } catch(e) {
                    //
                    //         }
                    //     })();
                    // };
                    // ololo(segAll[i]);
                    //this.downloadSingleSegment(segAll[i]);
                }
                if (i === cnt-1) {
                    console.log("Проверка сегментов", x);
                    (x === cnt ? this.downloadAllSegments(this.segAll) : (x === 0 ? this.loadMap() : newSegments.forEach(function(s) {
                        this.downloadSingleSegment(s);
                    }.bind(this))));
                }
            }.bind(this));
        }

        // this.segAll.forEach(function(d) {
        //     segments.getItem(d).then(function(data) {
        //         if (!data) {
        //             (async() => {
        //                 try {
        //
        //                     segments.setItem(d, segData[d]);
        //                 } catch(e) {
        //                     throw new Error(e);
        //                 }
        //             })();
        //         } else {
        //             //console.log(data);
        //         }
        //     })
        // });
    };
    Ksodd.prototype.loadMap = function() {
        //this.map.on("load", function() {
            console.log(this.segAll.length);
            console.time("loadingSources");
            this.segAll.forEach(function(segment) {
                segments.getItem(segment).then(function(s) {
                    // console.time("loadingSources" + segment);
                    if(s) {
                        if (!this.map.getSource("seg" + segment)) {
                            this.map.addSource("seg" + segment, {
                                type: "geojson",
                                data: s
                            });
                            this.map.addLayer({
                                "id": "seg" + segment,
                                "type": "line",
                                "source": "seg" + segment,
                                "minzoom": 10,
                                "maxzoom": 22,
                                "paint": {
                                    "line-color": "#e254bd",
                                    "line-opacity": 1,
                                    "line-width": 1
                                }
                            });
                            // let dotMk = s.properties.objects.dotMarkup.features;
                            // let lineMk = s.properties.objects.lineMarkup.features;
                            // let lineMkLbl = s.properties.objects.textLabelsLineMarkup.features;
                            // let racks = s.properties.objects.racks;
                            // let stretches = s.properties.objects.stretches;
                            // if(dotMk) {
                            //     for (let i = 0, cnt = dotMk.length; i < cnt; i++) {
                            //         if(dotMk[i].geometry.coordinates) {
                            //             this.map.addSource("seg" + segment + "_dotMk" + i, {
                            //                 type: "geojson",
                            //                 data: dotMk[i]
                            //             });
                            //         }
                            //     }
                            // }
                            // if(lineMk) {
                            //     for (let i = 0, cnt = lineMk.length; i < cnt; i++) {
                            //         if(lineMk[i].geometry.coordinates) {
                            //             this.map.addSource("seg" + segment + "_lineMk" + i, {
                            //                 type: "geojson",
                            //                 data: lineMk[i]
                            //             });
                            //         }
                            //     }
                            // }
                            // if(lineMkLbl) {
                            //     for (let i = 0, cnt = lineMkLbl.length; i < cnt; i++) {
                            //         if(lineMkLbl[i].geometry.coordinates) {
                            //             this.map.addSource("seg" + segment + "_lineMkLbl" + i, {
                            //                 type: "geojson",
                            //                 data: lineMkLbl[i]
                            //             });
                            //         }
                            //     }
                            // }
                            // // console.log(racks);
                            // for (let k in racks) {
                            //     this.map.addSource("seg" + segment + "_rack" + k, {
                            //         type: "geojson",
                            //         data: {
                            //             "type": "Feature",
                            //             "geometry": racks[k].geometry
                            //         }
                            //     });
                            // }
                            // for (let k in stretches) {
                            //     console.log(stretches[k]);
                            //     // this.map.addSource("seg" + segment + "_stretch" + k, {
                            //     //     type: "geojson",
                            //     //     data: stretches[k]
                            //     // });
                            // }
                        }
                    } else {
                        // console.log(segment);
                        //this.downloadSingleSegment(segment);
                    }
                    // console.timeEnd("loadingSources" + segment);
                    // console.log(this.map.getSource("seg" + segment));
                }.bind(this));

            }.bind(this));
            console.timeEnd("loadingSources");

        //}.bind(this));
    };
    db.segments = segments;
    App.Ksodd = Ksodd;
    App.db = db;
    w.App = App;
})(window);