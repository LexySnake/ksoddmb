(function(w) {
    let App = w.App || {};
    function KsoddLeaflet() {
        this.map = {};
        this.divObj = {};
        this.distObj = {};
        this.strObj = {};
        this.zoneObj = {};
        this.sideObj = {};
    }
    KsoddLeaflet.prototype.setMap = function() {
        this.map = L.map("map", {
            center: [55.7522200, 37.6155600],
            attributionControl: false,
            zoomControl: true,
            minZoom: 10,
            maxZoom: 22,
            zoom: 11,
            closePopupOnClick: false
        });
        this.map.getRenderer(this.map).options.padding = 2000;
        L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
            attribution: "Map data &copy; <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"http://mapbox.com\">Mapbox</a>",
            maxZoom: 22,
            id: "mapbox.streets",
            accessToken: "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw"
        }).addTo(this.map);

        // TODO: доработать изменение zoomL
        let zoomBox = document.createElement("div");
        zoomBox.innerHTML = this.map.getZoom();
        let zoom = document.getElementsByClassName("leaflet-control-zoom");
        for (let z = 0, cnt = zoom.length; z < cnt; z++) {
            zoom[z].className += " mgtniip_zoom";
            zoom[z].appendChild(zoomBox);
        }

        let hash = w.location.hash.split("#")[1];
        let c = [];
        let z = 10;
        if (hash && hash.split(",")) {
            c = [hash.split(",")[0], hash.split(",")[1]];
            z = hash.split(",")[2];
        } else {
            c = [this.map.getCenter().lat, this.map.getCenter().lng];
            z = this.map.getZoom();
        }
        setPermalink(this.map, c, z, false);
        this.map.createPane("divAllPane");
        this.map.getPane("divAllPane").style.zIndex = "410";
        this.map.createPane("divStrokePane");
        this.map.getPane("divStrokePane").style.zIndex = "415";
        this.map.createPane("divUnitPane");
        this.map.getPane("divUnitPane").style.zIndex = "420";
        this.map.createPane("distAllPane");
        this.map.getPane("distAllPane").style.zIndex = "430";
        this.map.createPane("distStrokePane");
        this.map.getPane("distStrokePane").style.zIndex = "435";
        this.map.createPane("distUnitPane");
        this.map.getPane("distUnitPane").style.zIndex = "440";
        this.map.createPane("segAllPane");
        this.map.getPane("segAllPane").style.zIndex = "450";
        this.map.createPane("segStrokePane");
        this.map.getPane("segStrokePane").style.zIndex = "455";
        this.map.createPane("segUnitPane");
        this.map.getPane("segUnitPane").style.zIndex = "460";
        this.map.createPane("rsPane");
        this.map.getPane("rsPane").style.zIndex = "470";
        this.map.createPane("zPane");
        this.map.getPane("zPane").style.zIndex = "480";
        this.map.createPane("olPastPane");
        this.map.getPane("olPastPane").style.zIndex = "605";
        this.map.createPane("olCurrentPane");
        this.map.getPane("olCurrentPane").style.zIndex = "610";
        this.map.createPane("olFuturePane");
        this.map.getPane("olFuturePane").style.zIndex = "615";
    };
    KsoddLeaflet.prototype.getPage = function(dft) {
        // set user information
        document.getElementById("user_login").innerHTML = localStorage.getItem("user") || "MTP_user";
        document.getElementById("logout").addEventListener("click", function() {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            w.location = dft.serverLocation(App.FLAG).auth;
        });
        // download screen to png
        document.getElementById("png").addEventListener("click", function() {
            document.getElementById("analytics").classList.toggle("hidden");
            let node = document.body;
            domtoimage.toPng(node).then(function(pic) {
                let a = document.createElement("a");
                a.download = "ais_ksaoo.png";
                a.href = pic;
                a.click();
            }.bind(this));
        });
        // print page
        document.getElementById("print").addEventListener("click", function() {
            w.print();
        });

        // set path-link
        let link = document.getElementById("link");
        let linkPath = document.getElementById("link_path");
        link.addEventListener("click", function() {
            linkPath.value = w.location.href;
        });
        document.getElementById("copy_link").addEventListener("click", function() {
            linkPath.select();
            document.execCommand("copy");
        });
        console.log("Hello Leaflet!");
        this.setMap();
        // get segments;
        let segInt = new App.Segments("segment_intersections", "Перекрёсток");
        segInt.getData(this.map);
        let segSt = new App.Segments("segment_streets", "Пролёт");
        segSt.getData(this.map);
        // get road objexts
        setTimeout(() => {this.getRoadObjects(segInt.obj)}, 5000);
        setTimeout(() => {this.getRoadObjects(segSt.obj)}, 5000);
    };
    KsoddLeaflet.prototype.getRoadObjects = function(segment) {
        let loadSignsGroup = {};
        let loadLinearMarkGroup = {};
        let loadPointMarkGroup = {};
        let signsGroup = L.layerGroup();
        let pointMarkGroup = L.layerGroup();
        let linearMarkGroup = L.layerGroup();
        let linearMarkTextGroup = L.layerGroup();
        let signsEl = document.getElementById("signs");
        signsEl.checked = true;
        signsEl.onclick = function() {
            switchLayer.call(this, signsEl, signsGroup);
        }.bind(this);
        let linearMarkEl = document.getElementById("linear_mark");
        linearMarkEl.checked = true;
        linearMarkEl.onclick = switchLayer.call(this, linearMarkEl, pointMarkGroup);
        let pointMarkEl = document.getElementById("point_mark");
        pointMarkEl.checked = true;
        pointMarkEl.onclick = switchLayer.call(this, pointMarkEl, linearMarkGroup);
        this.map.eachLayer(function(l) {
            l.on("load", function () {
                let zoomLevel = this.map.getZoom();
                // zoomBox.innerHTML = zoomLevel;
                let zoomCoef = 0;
                let coefficient = 1;
                let lineAnchor = 0;
                let meterAnchor = 0;
                let signText = "";
                if (zoomLevel <= 18) {
                    signsGroup.clearLayers();
                    pointMarkGroup.clearLayers();
                    linearMarkGroup.clearLayers();
                    linearMarkTextGroup.clearLayers();
                } else {
                    switch (zoomLevel) {
                        case 19:
                            zoomCoef = 1;
                            coefficient = 1;
                            signText = "5px";
                            lineAnchor = 25;
                            meterAnchor = 2.2;
                            tileCoef = 0.001;
                            break;
                        case 20:
                            zoomCoef = 2;
                            coefficient = 2;
                            signText = "9px";
                            lineAnchor = 20;
                            meterAnchor = 1;
                            tileCoef = 0.0005;
                            break;
                        case 21:
                            zoomCoef = 3;
                            coefficient = 4;
                            signText = "14px";
                            lineAnchor = 10;
                            meterAnchor = 0.45;
                            tileCoef = 0.00025;
                            break;
                        case 22:
                            zoomCoef = 4;
                            coefficient = 6;
                            signText = "19px";
                            lineAnchor = 10;
                            meterAnchor = 0.15;
                            tileCoef = 0.00013;
                            break;
                        default:
                            zoomCoef = 1;
                            coefficient = 1;
                            lineAnchor = 0;
                            meterAnchor = 0;
                            tileCoef = 0.001;
                    }
                    getObjectsInBounds(segment, this, signsGroup, loadSignsGroup, zoomCoef, coefficient, signText);
                }
            }.bind(this));
        }.bind(this));
    };
    switchLayer = function(el, group) {
        (el.checked ? group.addTo(this.map) : group.remove());
    };
    getObjectsInBounds = function(segments, meme, signsGroup, loadSignsGroup, zoomCoef, coefficient, signText) {
        segments.features.forEach((s) => {
            L.geoJSON(s, {
                onEachFeature: function (feature, polygon) {
                    if (meme.map.getBounds().intersects(polygon.getBounds())) {
                        App.db.segments.getItem(s.properties.name).then(function (d) {
                            if (!d) {
                                s.properties.type = "Пролёт";
                                s.properties.cnt = {};
                                s.properties.objects = {};
                                (async () => {
                                    try {
                                        let segResponse = await fetch(App.DOMAIN + "PGsegmentBBoxData", {
                                            method: "POST",
                                            headers: {
                                                "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                                            },
                                            body: JSON.stringify({
                                                "racks": true,
                                                "stretches": true,
                                                "lineMarkup": true,
                                                "textLabelsLineMarkup": true,
                                                "dotMarkup": true,
                                                "names": [s.properties.name]
                                            })
                                        });
                                        let segData = await segResponse.json();
                                        s.properties.objects = segData[s.properties.name];
                                        App.db.segments.setItem(s.properties.name, s);
                                        if (meme.map.hasLayer(signsGroup) === true) {
                                            downloadSigns(s, meme.map, signsGroup, loadSignsGroup, zoomCoef, coefficient, signText);
                                        }
                                    } catch (e) {
                                        throw new Error(e);
                                    }
                                })();
                            } else {
                                console.log(d);
                                if (meme.map.hasLayer(signsGroup) === true) {
                                    downloadSigns(d, meme.map, signsGroup, loadSignsGroup, zoomCoef, coefficient, signText);
                                }
                            }
                        });
                    }
                }
            });
        });
    };
    App.KsoddLeaflet = KsoddLeaflet;
    w.App = App;
})(window);