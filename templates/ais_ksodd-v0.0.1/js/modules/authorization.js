(function(w) {
    let App = w.App || {};
    function Authorization() {
        this.token = "";
        this.expire = "";
        this.currentDate = new Date();
        this.path = {};
    }
    Authorization.prototype.authentication = function(btn, inpL, inpP, lctFn) {
        document.body.addEventListener("keydown", function (e) {
            if (e.key === "Enter") {
                e.preventDefault();
                submit.click();
            }
        });
        btn.addEventListener("click", function() {
            (async(meme) => {
                try {
                    let response = await fetch(App.DOMAIN + "doauth", {
                        method: "POST",
                        mode: "cors",
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            "username": inpL.value,
                            "password": inpP.value
                        })
                    });
                    let data = await response.json();
                    if (data.Error) {
                        alert("Ошибка авторизации!");
                        w.location.reload();
                    } else {
                        meme.token = data.token;
                        meme.expire = data.expire;
                        localStorage.setItem("token", JSON.stringify(data));
                        localStorage.setItem("user", inpL.value);
                        meme.getRole(lctFn);
                    }
                } catch(e) {
                    throw new Error(e);
                }
            })(this);
        }.bind(this));
    };
    Authorization.prototype.serverLocation = function(f) {
        (f ? path = {
            "auth": "https://niikeeper.com/authentication",
            "mapbox": "https://niikeeper.com/ais_ksodd/mapboxEdition",
            "leaflet": "https://niikeeper.com/ais_ksodd"
        } : path = {
            "auth": "ais_ksodd_auth.html",
            "mapbox": "ais_ksodd_index.html",
            "leaflet": "ais_ksodd_leaflet.html"
        });
        return path;
    };
    Authorization.prototype.verification = function(itm, lctFn, dft) {
        if (!localStorage.getItem(itm)) {
            w.location.assign(lctFn.auth);
        } else {
            this.token = JSON.parse(localStorage.getItem(itm)).token;
            this.expire = JSON.parse(localStorage.getItem(itm)).expire;
            let expireDate = new Date(this.expire);
            if(expireDate < this.currentDate) {
                localStorage.removeItem(itm);
                w.location.assign(lctFn.auth);
            } else {
                (async(meme) => {
                    try {
                        let response = await fetch(App.DOMAIN + "referenceInfo", {
                            method: "GET",
                            headers: {
                                "Authorization": "Bearer " + this.token
                            }
                        });
                        let data = await response.json();
                        App.refInfo = data;
                        if (data.Error) {
                            localStorage.removeItem(itm);
                            w.location.assign(lctFn.auth);
                        } else {
                            dft.getPage(meme);
                            // dft.setMap();
                            // dft.download();
                        }
                    } catch(e) {
                        throw new Error(e);
                        alert("Ошибка авторизации!<br>Вы будете перенаправлены<br>для повторной авторизации");
                        localStorage.removeItem(itm);
                        w.location.assign(lctFn.auth);
                    }
                })(this);
            }
        }
    };
    Authorization.prototype.getRole = function(lctFn) {
        (async() => {
            try {
                let response = await fetch(App.DOMAIN + "authrole", {
                    method: "GET",
                    headers: {
                        "Authorization": "Bearer " + this.token
                    }
                });
                let data = await response.json();
                (data.role === 1 ? w.location.assign(lctFn.mapbox) :
                    (data.role === 2 ? w.location.assign(lctFn.leaflet) :
                        alert("У пользователя не задана роль! Пожалуйста, обратитесь к администраторам сервиса.")
                    )
                );
            } catch(e) {
                throw new Error(e);
            }
        })();
    };
    App.Authorization = Authorization;
    w.App = App;
})(window);