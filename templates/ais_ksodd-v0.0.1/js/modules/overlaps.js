function getOlPopup(ol, c) {
    let box = document.createElement("div");
    box.className = "segment_popup";
    let table = document.createElement("table");
    let start = new Date(ol.properties.ts_from.slice(0,4), ol.properties.ts_from.slice(5,7), ol.properties.ts_from.slice(8,10), ol.properties.ts_from.slice(11,13), ol.properties.ts_from.slice(14,16), ol.properties.ts_from.slice(17,19));
    let end = new Date(ol.properties.ts_to.slice(0,4), ol.properties.ts_to.slice(5,7), ol.properties.ts_to.slice(8,10), ol.properties.ts_to.slice(11,13), ol.properties.ts_to.slice(14,16), ol.properties.ts_to.slice(17,19));
    let startD = (start.getDate() < 10 ? "0" + start.getDate().toString() : start.getDate().toString());
    let startMon = (start.getMonth()+1 < 10 ? "0" + (start.getMonth()+1).toString() : (start.getMonth()+1).toString());
    let startH = (start.getHours() < 10 ? "0" + start.getHours().toString() : start.getHours().toString());
    let startM = (start.getMinutes() < 10 ? "0" + start.getMinutes().toString() : start.getMinutes().toString());
    let startS = (start.getSeconds() < 10 ? "0" + start.getSeconds().toString() : start.getSeconds().toString());
    let startDate = startD + "." + startMon + "." + start.getFullYear() + " " + startH + ":" + startM;
    let endD = (end.getDate() < 10 ? "0" + end.getDate().toString() : end.getDate().toString());
    let endMon = (end.getMonth()+1 < 10 ? "0" + (end.getMonth()+1).toString() : (end.getMonth()+1).toString());
    let endH = (end.getHours() < 10 ? "0" + end.getHours().toString() : end.getHours().toString());
    let endM = (end.getMinutes() < 10 ? "0" + end.getMinutes().toString() : end.getMinutes().toString());
    let endS = (end.getSeconds() < 10 ? "0" + end.getSeconds().toString() : end.getSeconds().toString());
    let endDate = endD + "." + endMon + "." + end.getFullYear() + " " + endH + ":" + endM;
    table.style.marginBottom = "0";
    table.innerHTML =
        "<tbody>" +
        "<tr>" +
        "<th>Название:</th>" +
        "<td style=\"max-width:400px;\">" + text(ol.properties.name) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Время перекрытия:</th>" +
        "<td style=\"max-width:400px;\">" + startDate + " - " + endDate + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Тип:</th>" +
        "<td style=\"max-width:400px;\">" + ol.properties.event_type.name +", <span  style=\"font-weight:900; color:" + c + "\">" + ol.properties.event_work_type.name + "</span></td>" +
        "</tr>" +
        "<tr>" +
        "<th>Адрес:</th>" +
        "<td style=\"max-width:400px;\">" + text(ol.properties.address) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Описание:</th>" +
        "<td style=\"max-width:400px;\">" + text(ol.properties.descr) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Заказчик:</th>" +
        "<td style=\"max-width:400px;\">" + text(ol.properties.requester) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Подрядчик:</th>" +
        "<td style=\"max-width:400px;\">" + text(ol.properties.customer) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Тел.подрядчика:</th>" +
        "<td style=\"max-width:400px;\"><a href=\"tel:" + ol.properties.telephone_number + "\" style=\"text-decoration:none; color:inherit\">" + ol.properties.telephone_number + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Перекрыто полос:</th>" +
        "<td style=\"max-width:400px;\">" + num(ol.properties.lanes_closed) + "</td>" +
        "</tr>" +
        "<tr>" +
        "<th>Всего полос:</th>" +
        "<td style=\"max-width:400px;\">" + num(ol.properties.lanes_available) + "</td>" +
        "</tr>" +
        "</tbody>";
    box.appendChild(table);
    return box.outerHTML;
}
function text(data) {
    if(!data) {
        data = "−";
    }
    return data;
}
function num(data) {
    if(!data) {
        data = 0;
    }
    return data;
}
function drawOlCircle(lat, lng, coef, color, fillOpacity, popup, group, overlap, m) {
    let circle = L.circle(L.latLng(lat, lng), {
        radius: getOlProperties(map.getZoom())[1]/coef,
        weight: getOlProperties(map.getZoom())[0],
        color: color,
        fillOpacity: fillOpacity,
        opacity: m.opacity,
        pane: m.pane
    }).bindPopup(popup).addTo(group);
    map.on("zoomend", function () {
        circle.setRadius(getOlProperties(map.getZoom())[1]/coef);
        circle.setStyle({
            weight: getOlProperties(map.getZoom())[0]
        });
    });
    openOlPopup(circle, overlap, m, color);
}
function openOlPopup(el, overlap, m, color) {
    el.on("popupopen", function() {
        let popup = document.getElementsByClassName(overlap.properties.event_work_type.id + " " + m.period);
        for (let j = 0; j < popup.length; j++) {
            popup[j].firstElementChild.style.border = "2px" + m.line  + color;
            popup[j].children[1].style.borderRight = "2px" + m.line + color;
            popup[j].children[1].style.borderBottom = "2px" + m.line +  color;
        }
    });
}
function drawOl(o, meme, cnt, group) {
    if (!o.properties.confirmed) {
        for (let i = 0; i < meme.types.length; i++) {
            if (o.properties.event_work_type.id === meme.types[i]) {
                cnt[cnt.length - 1]++;
                let olPopup = L.popup({
                    className: "mgtniip_popup " + o.properties.event_work_type.id + " " + meme.period,
                    autoClose: true
                }).setContent(getOlPopup(o, meme.colors[i]));
                L.geoJSON(o, {
                    onEachFeature: function (feature, polyline) {
                        polyline.setStyle({
                            weight: getOlProperties(map.getZoom())[0],
                            color: meme.olColors[meme.olColors.length - 1],
                            pane: meme.pane,
                            dashArray: meme.dash,
                            opacity: meme.opacity
                        });
                        map.on("zoomend", function () {
                            polyline.setStyle({
                                weight: getOlProperties(map.getZoom())[0]
                            });
                        });
                        openOlPopup(polyline, o, meme, "#000");
                    }
                }).bindPopup(olPopup).addTo(group[group.length - 1]);
                drawOlCircle(o.geometry.coordinates[0][1], o.geometry.coordinates[0][0], 30, meme.olColors[meme.olColors.length - 1], meme.opacity, olPopup, group[group.length - 1], o, meme);
                drawOlCircle(o.geometry.coordinates[0][1], o.geometry.coordinates[0][0], 1, meme.olColors[meme.olColors.length - 1], 0.0, olPopup, group[group.length - 1], o, meme);
                drawOlCircle(o.geometry.coordinates[o.geometry.coordinates.length - 1][1], o.geometry.coordinates[o.geometry.coordinates.length - 1][0], 30, meme.olColors[meme.olColors.length - 1], meme.opacity, olPopup, group[group.length - 1], o, meme);
                drawOlCircle(o.geometry.coordinates[o.geometry.coordinates.length - 1][1], o.geometry.coordinates[o.geometry.coordinates.length - 1][0], 1, meme.olColors[meme.olColors.length - 1], 0.0, olPopup, group[group.length - 1], o, meme);
            }
        }
    } else {
        for (let i = 0; i < meme.types.length; i++) {
            if (o.properties.event_work_type.id === meme.types[i]) {
                cnt[i]++;
                let olPopup = L.popup({
                    className: "mgtniip_popup " + o.properties.event_work_type.id + " " + meme.period,
                    autoClose: true
                }).setContent(getOlPopup(o, meme.colors[i]));
                L.geoJSON(o, {
                    onEachFeature: function (feature, polyline) {
                        polyline.setStyle({
                            weight: getOlProperties(map.getZoom())[0],
                            color: meme.olColors[i],
                            pane: meme.pane,
                            dashArray: meme.dash,
                            opacity: meme.opacity
                        });
                        map.on("zoomend", function () {
                            polyline.setStyle({
                                weight: getOlProperties(map.getZoom())[0]
                            });
                        });
                        openOlPopup(polyline, o, meme, "#000");
                    }
                }).bindPopup(olPopup).addTo(group[i]);
                drawOlCircle(o.geometry.coordinates[0][1], o.geometry.coordinates[0][0], 30, meme.olColors[i], meme.opacity, olPopup, group[i], o, meme);
                drawOlCircle(o.geometry.coordinates[0][1], o.geometry.coordinates[0][0], 1, meme.olColors[i], 0.0, olPopup, group[i], o, meme);
                drawOlCircle(o.geometry.coordinates[o.geometry.coordinates.length - 1][1], o.geometry.coordinates[o.geometry.coordinates.length - 1][0], 30, meme.olColors[i], meme.opacity, olPopup, group[i], o, meme);
                drawOlCircle(o.geometry.coordinates[o.geometry.coordinates.length - 1][1], o.geometry.coordinates[o.geometry.coordinates.length - 1][0], 1, meme.olColors[i], 0.0, olPopup, group[i], o, meme);
            }
        }
    }
}
(function(w) {
    let App = w.App || {};
    function Overlaps(o, pane, dash, opacity, line, el, olColors = ["#4a90e2", "#bd10e0", "#d0021b", "#f5a623", "#3dc7a8", "#87df27", "#000000"]) {
        this.period = o;
        this.element = el;
        this.opacity = opacity;
        this.pane = pane;
        this.line = line;
        this.dash = dash;
        this.overlapsGroup = {
            olFull: {
                sToday: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                eToday: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                sTomorrow: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                other: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ]
            },
            olPart: {
                sToday: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                eToday: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                sTomorrow: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ],
                other: [L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup(),
                    L.layerGroup()
                ]
            }
        };
        this.types = ["8d5a5b04-f0fa-446b-b0fe-c0dd523fe11b",
            "44704f85-81d8-4efd-bc84-cbd1baa63213",
            "dcf4cde3-ccb8-4bb8-b5f0-a9bc112bc96e",
            "7bf73945-d0b2-41b7-9b80-179f241df25c",
            "d652b6e6-fd1b-43e9-8fed-464b4f7da484",
            "03bb9f19-a1a0-477f-8a3e-e1cdd98ca170"];
        this.colors = ["#4a90e2", "#bd10e0", "#d0021b", "#f5a623", "#3dc7a8", "#87df27", "#000000"];
        this.olColors = olColors;
        this.cnt = {
            cnt: 0,
            olFull: {
                cnt: 0,
                sToday: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                eToday: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                sTomorrow: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                other: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                }
            },
            olPart: {
                cnt: 0,
                sToday: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                eToday: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                sTomorrow: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                },
                other: {
                    cnt: 0,
                    types: [0, 0, 0, 0, 0, 0, 0]
                }
            }
        };
    }
    Overlaps.prototype.download = function(map) {
        (async(meme) => {
            try {
                let response = await fetch("https://niikeeper.com/ksoddAPI/v2.1.0/PGroadworks?roadworkPeriod=" + meme.period);
                let data = await response.json();
                console.log(data);
                data.features.forEach(function(o) {
                    if (o.geometry && o.geometry.type && (o.geometry.type !== "Point")) {
                        let today = new Date();
                        let tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
                        let dayStart = new Date(o.properties.ts_from.slice(0, 4), o.properties.ts_from.slice(5, 7), o.properties.ts_from.slice(8, 10));
                        let dayEnd = new Date(o.properties.ts_to.slice(0, 4), o.properties.ts_to.slice(5, 7), o.properties.ts_to.slice(8, 10));
                        if (o.properties.full_overlap) {
                            meme.cnt.olFull.cnt++;
                            if (dayStart === today) {
                                meme.cnt.olFull.sToday.cnt++;
                                drawOl(o, meme, meme.cnt.olFull.sToday.types, meme.overlapsGroup.olFull.sToday);
                            } else if (dayStart === tomorrow) {
                                meme.cnt.olFull.sTomorrow.cnt++;
                                drawOl(o, meme, meme.cnt.olFull.sTomorrow.types, meme.overlapsGroup.olFull.sTomorrow);
                            } else if (dayEnd === today) {
                                meme.cnt.olFull.eToday.cnt++;
                                drawOl(o, meme, meme.cnt.olFull.eToday.types, meme.overlapsGroup.olFull.eToday);
                            } else {
                                meme.cnt.olFull.other.cnt++;
                                drawOl(o, meme, meme.cnt.olFull.other.types, meme.overlapsGroup.olFull.other);
                            }
                        } else {
                            meme.cnt.olPart.cnt++;
                            if (dayStart === today) {
                                meme.cnt.olPart.sToday.cnt++;
                                drawOl(o, meme, meme.cnt.olPart.sToday.types, meme.overlapsGroup.olPart.sToday);
                            } else if (dayStart === tomorrow) {
                                meme.cnt.olPart.sTomorrow.cnt++;
                                drawOl(o, meme, meme.cnt.olPart.sTomorrow.types, meme.overlapsGroup.olPart.sTomorrow);
                            } else if (dayEnd === today) {
                                meme.cnt.olPart.eToday.cnt++;
                                drawOl(o, meme, meme.cnt.olPart.eToday.types, meme.overlapsGroup.olPart.eToday);
                            } else {
                                meme.cnt.olPart.other.cnt++;
                                drawOl(o, meme, meme.cnt.olPart.other.types, meme.overlapsGroup.olPart.other);
                            }
                        }
                    }
                });
                meme.cnt.cnt = meme.cnt.olFull.cnt + meme.cnt.olPart.cnt;
                meme.createCount(map);
            } catch(e) {
                throw new Error(e);
            }
        })(this);
    };
    Overlaps.prototype.createCount = function(map) {
        let overlaps = document.getElementById("overlaps");
        let ol = document.getElementsByClassName("ol");
        let olFull = document.getElementById("ol_full");
        let olPart = document.getElementById("ol_part");
        let sToday = document.getElementById("start_today");
        let eToday = document.getElementById("end_today");
        let sTomorrow = document.getElementById("start_tomorrow");
        let other = document.getElementById("other");
        acc_single(overlaps);
        let startElCnt = this.cnt.cnt;
        let elCnt = this.element.nextElementSibling.lastChild;
        function checkOl(meme, i, chk, type, sType, time, sTime) {
            if(chk) {
                if ((meme.element.checked) && (type.checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) + meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].addTo(map);
                }
            } else {
                if ((meme.element.checked) && (type.checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) - meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].remove();
                }
            }
        }
        function checkTime(meme, i, chk, type, sType, time, sTime) {
            if(chk) {
                if ((meme.element.checked) && (ol[i].checked) && (type.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) + meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].addTo(map);
                }
            } else {
                if ((meme.element.checked) && (ol[i].checked) && (type.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) - meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].remove();
                }
            }
        }
        function checkType(meme, i, chk, type, sType, time, sTime) {
            if(chk) {
                if ((meme.element.checked) && (ol[i].checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) + meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].addTo(map);
                }
            } else {
                if ((meme.element.checked) && (ol[i].checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) - meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].remove();
                }
            }
        }
        function check(meme, i, chk, type, sType, time, sTime) {
            if(chk) {
                if (meme.element === document.getElementById("ol_current")) {
                    sToday.disabled = false;
                    eToday.disabled = false;
                } else if (meme.element === document.getElementById("ol_future")) {
                    sTomorrow.disabled = false;
                }
                if ((ol[i].checked) && (type.checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) + meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) + meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].addTo(map);
                }
            } else {
                if (meme.element === document.getElementById("ol_current")) {
                    sToday.disabled = true;
                    eToday.disabled = true;
                } else if (meme.element === document.getElementById("ol_future")) {
                    sTomorrow.disabled = true;
                }
                if ((ol[i].checked) && (type.checked) && (time.checked)) {
                    elCnt.innerHTML = parseInt(elCnt.textContent) - meme.cnt[sType][sTime].types[i];
                    ol[i].nextElementSibling.lastElementChild.innerHTML = parseInt(ol[i].nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    type.nextElementSibling.lastElementChild.innerHTML = parseInt(type.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    time.nextElementSibling.lastElementChild.innerHTML = parseInt(time.nextElementSibling.lastElementChild.textContent) - meme.cnt[sType][sTime].types[i];
                    meme.overlapsGroup[sType][sTime][i].remove();
                }
            }
        }
        this.element.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                check(this, i, this.element.checked, olFull, "olFull", sToday, "sToday");
                check(this, i, this.element.checked, olFull, "olFull", eToday, "eToday");
                check(this, i, this.element.checked, olFull, "olFull", sTomorrow, "sTomorrow");
                check(this, i, this.element.checked, olFull, "olFull", other, "other");
                check(this, i, this.element.checked, olPart, "olPart", sToday, "sToday");
                check(this, i, this.element.checked, olPart, "olPart", eToday, "eToday");
                check(this, i, this.element.checked, olPart, "olPart", sTomorrow, "sTomorrow");
                check(this, i, this.element.checked, olPart, "olPart", other, "other");
            }
        }.bind(this));
        olFull.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkType(this, i, olFull.checked, olFull, "olFull", sToday, "sToday");
                checkType(this, i, olFull.checked, olFull, "olFull", eToday, "eToday");
                checkType(this, i, olFull.checked, olFull, "olFull", sTomorrow, "sTomorrow");
                checkType(this, i, olFull.checked, olFull, "olFull", other, "other");
            }
        }.bind(this));
        olPart.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkType(this, i, olPart.checked, olPart, "olPart", sToday, "sToday");
                checkType(this, i, olPart.checked, olPart, "olPart", eToday, "eToday");
                checkType(this, i, olPart.checked, olPart, "olPart", sTomorrow, "sTomorrow");
                checkType(this, i, olPart.checked, olPart, "olPart", other, "other");
            }
        }.bind(this));
        sToday.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkTime(this, i, sToday.checked, olFull, "olFull", sToday, "sToday");
                checkTime(this, i, sToday.checked, olPart, "olPart", sToday, "sToday");
            }
        }.bind(this));
        eToday.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkTime(this, i, eToday.checked, olFull, "olFull", eToday, "eToday");
                checkTime(this, i, eToday.checked, olPart, "olPart", eToday, "eToday");
            }
        }.bind(this));
        sTomorrow.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkTime(this, i, sTomorrow.checked, olFull, "olFull", sTomorrow, "sTomorrow");
                checkTime(this, i, sTomorrow.checked, olPart, "olPart", sTomorrow, "sTomorrow");
            }
        }.bind(this));
        other.addEventListener("click", function() {
            for (let i = 0, cnt = ol.length; i < cnt; i++) {
                checkTime(this, i, other.checked, olFull, "olFull", other, "other");
                checkTime(this, i, other.checked, olPart, "olPart", other, "other");
            }
        }.bind(this));
        for (let i = 0, cnt = ol.length; i < cnt; i++) {
            ol[i].addEventListener("click", function() {
                checkOl(this, i, ol[i].checked, olFull, "olFull", sToday, "sToday");
                checkOl(this, i, ol[i].checked, olFull, "olFull", eToday, "eToday");
                checkOl(this, i, ol[i].checked, olFull, "olFull", sTomorrow, "sTomorrow");
                checkOl(this, i, ol[i].checked, olFull, "olFull", other, "other");
                checkOl(this, i, ol[i].checked, olPart, "olPart", sToday, "sToday");
                checkOl(this, i, ol[i].checked, olPart, "olPart", eToday, "eToday");
                checkOl(this, i, ol[i].checked, olPart, "olPart", sTomorrow, "sTomorrow");
                checkOl(this, i, ol[i].checked, olPart, "olPart", other, "other");
            }.bind(this));
        }
        overlaps.addEventListener("click", function() {
            if (this.checked) {
                if (!document.getElementById("ol_current").checked) {
                    document.getElementById("ol_current").click();
                }
                if (!document.getElementById("ol_future").checked) {
                    document.getElementById("ol_future").click();
                }
            } else {
                if (document.getElementById("ol_current").checked) {
                    document.getElementById("ol_current").click();
                }
                if (document.getElementById("ol_future").checked) {
                    document.getElementById("ol_future").click();
                }
            }
        });
    };
    App.Overlaps = Overlaps;
    w.App = App;
})(window);