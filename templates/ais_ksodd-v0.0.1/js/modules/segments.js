(function(w) {
    let App = w.App || {};
    let Segments = function(type, name) {
        this.allColor = "#bb15d9";
        this.unitColor = "#ff3aea";
        this.type = type;
        this.name = name;
        this.obj = {};
    };
    Segments.prototype.getData = function(map) {
        (async(meme) => {
            try {
                let response = await fetch(App.DOMAIN + "PGgetPolygon?type=" + meme.type);
                meme.obj = await response.json();
                let cntResponse = await fetch(App.DOMAIN + "getSegmentsCount", {
                    headers: {
                        "Authorization": "Bearer " + JSON.parse(localStorage.getItem("token")).token
                    }
                });
                let cnt = await cntResponse.json();
                meme.drawPolygons(meme, map, cnt);
            } catch(e) {
                throw  new Error(e);
            }
        })(this);
    };
    Segments.prototype.drawPolygons = function(meme, map, cnt) {
        let g = L.layerGroup();
        meme.obj.features.forEach(function(s) {
            for (let k in cnt.Summary) {
                if (k === s.properties.name) s.properties.cnt = cnt.Summary[k];
            }
            L.geoJSON(s, {
                onEachFeature: function(feature, polygon) {
                    polygon.setStyle({
                        weight: getOpacity(map.getZoom())[1] - 2,
                        opacity: 1.0,
                        color: meme.allColor,
                        fillColor: meme.unitColor,
                        fillOpacity: getOpacity(map.getZoom())[2],
                        pane: "segAllPane"
                    }).bindPopup(meme.setPopup(s), {
                        autoPan: true,
                        className: "mtp_popup"
                    });
                    map.on("zoom", function () {
                        polygon.setStyle({
                            weight: getOpacity(map.getZoom())[1] - 2,
                            fillColor: meme.unitColor,
                            fillOpacity: getOpacity(map.getZoom())[2]
                        });
                    });
                }
            }).addTo(g);
        });
        document.getElementById(meme.type).addEventListener("click", function() {
            this.checked ? g.addTo(map) : g.remove();
        });
        console.log(meme.obj);
    };
    Segments.prototype.setPopup = function(s) {
        let imgName = "";
        let cnt = s.properties.cnt;
        (this.name === "Пролёт" ? imgName = "segSt" : imgName = "segInt");
        let box = document.createElement("div");
        box.className = "main_popup";
        let green = document.createElement("span");
        green.className = "status_bg_green";
        let orange = document.createElement("span");
        orange.className = "status_bg_orange";
        let red = document.createElement("span");
        red.className = "status_bg_red";
        let signs = "";
        //TODO: Получать статусы сегментов из БД после актуализации!
        let status = "";
        if (cnt) {
            let n = 0;
            let number = 0;
            for (let c in cnt) {
                (c === "Проектируемый" ? orange.innerHTML = cnt[c] :
                    (c === "Демонтируемый" ? red.innerHTML = cnt[c] :
                        n += parseInt(cnt[c])));
            }
            green.innerHTML = n;
            if ((orange.textContent) && (red.textContent)) {
                number = n + parseInt(orange.textContent) + parseInt(red.textContent);
                signs = number + " шт: " + green.outerHTML + " + " + orange.outerHTML + " + " + red.outerHTML;
                status = "<span class=\"status_txt_red\">Демонтируемый</span>";
            } else if (orange.textContent) {
                number = n + parseInt(orange.textContent);
                signs = number + " шт: " + green.outerHTML + " + " + orange.outerHTML;
                status = "<span class=\"status_txt_orange\">Проектируемый</span>";
            } else if (red.textContent) {
                number = n + parseInt(red.textContent);
                signs = number + " шт: " + green.outerHTML + " + " + red.outerHTML;
                status = "<span class=\"status_txt_red\">Демонтируемый</span>";
            } else {
                signs = green.outerHTML + " шт.";
                status = "<span class=\"status_txt_green\">Существующий</span>";
            }
        } else {
            signs = "0 шт.";
            status = "Знаки отсутсвуют";
        }
        let table = document.createElement("table");
        table.innerHTML =  `<tbody>
                            <tr><th>Сегмент:</th><td><div class="line" style="color: ${this.allColor}"><img src="${App.PATH}img/${imgName}_lil.svg" alt="Изображение сегмента типа ${this.name}"> ${s.properties.name}</div></td></tr>
                            <tr><th>Тип:</th><td>${this.name}</td></tr>
                            <tr><th>Статус:</th><td>${status}</td></tr>
                            <tr><th>Знаки:</th><td>${signs}</td></tr>
                            </tbody>`;
        let h4 = document.createElement("h4");
        h4.innerHTML = "Аналитика сегмента:";
        let analytics = ["Дорожные знаки", "Точечная разметка", "Линейная разметка"];
        let foo = [downloadCSVSigns, downloadCSVPoints, downloadCSVLines];
        let info = document.createElement("div");
        info.className = "part";
        info.appendChild(h4);
        for (let a of analytics) {
            let line = document.createElement("a");
            line.className = "line download";
            line.innerHTML = `<img src="${App.PATH}img/extension_csv.svg" alt="Загрузить аналитику сегмента по объекту ${a}">${a}`;
            line.onclick = function(e) {
                e.preventDefault();
                foo[analytics.indexOf(a)](s.properties.name);
            };
            info.appendChild(line);
        }
        box.appendChild(table);
        box.appendChild(info);
        return box;
    };
    App.Segments = Segments;
    w.App = App;
})(window);