// TODO: написать скрипт для кастомизации полос прокрутки

let controlBox = document.getElementsByClassName("control_box");
for (let i = 0, cnt = controlBox.length; i < cnt; i++) {
    let header = controlBox[i].firstElementChild;
    let content = controlBox[i].lastElementChild;
    if (!controlBox[i].classList.contains("nohidden")) {
        header.addEventListener("click", function() {
            this.parentElement.classList.toggle("hidden");
        });
    }
    for (let j = 0, chCnt = content.children.length; j < chCnt; j++) {
        if (content.children[j].classList.contains("scroll_bar")) {
            content.children[j].style.paddingRight = "8px";
        }
    }
}
let asideBox = document.getElementsByClassName("aside_box");
for (let i = 0, cnt = asideBox.length; i < cnt; i++) {
    let header = asideBox[i].firstElementChild;
    let content = asideBox[i].lastElementChild;
    if (!asideBox[i].classList.contains("nohidden")) {
        header.addEventListener("click", function() {
            this.parentElement.classList.toggle("hidden");
            let asideSib = document.getElementsByClassName("aside_box");
            for (let j = 0, cntSib = asideSib.length; j < cntSib; j++) {
                if((this.parentElement !== asideSib[j]) && !asideSib[j].classList.contains("hidden")) {
                    asideSib[j].classList.add("hidden");
                }
            }
        });
    }
    for (let j = 0, chCnt = content.children.length; j < chCnt; j++) {
        if (content.children[j].classList.contains("scroll_bar")) {
            content.children[j].style.paddingRight = "8px";
        }
    }
}

// Accordion
function acc() {
    let accordion = document.getElementsByClassName("accordion_box");
    for (let i = 0, cnt = accordion.length; i < cnt; i++) {
        if (accordion[i].children) {
            console.log(accordion[i].children);
            for (let j = 0, chCnt = accordion[i].children.length; j < chCnt; j+=2) {
                let item = accordion[i].children[j];
                let innerItem = item.nextElementSibling;
                item.addEventListener("click", function () {
                    for (let z = 0, zCnt = accordion[i].children.length; z < zCnt; z+=2) {
                        let sibling = accordion[i].children[z];
                        if ((sibling.classList.contains("open")) && (sibling !== item)) {
                            if (sibling.nextElementSibling.style.maxHeight) {
                                sibling.nextElementSibling.style.maxHeight = null;
                            }
                            sibling.classList.remove("open");
                        }
                    }
                    item.classList.toggle("open");
                    if (item.classList.contains("open")) {
                        innerItem.style.maxHeight = innerItem.scrollHeight + "px";
                    } else {
                        innerItem.style.maxHeight = 0;
                    }
                });
            }
        }
    }
}
// Permalink for map-location
// TODO: Оптимизировать функцию для leaflet/mapbox
function setPermalink(map, point, zoom, f) {
    if (f) {
        point = (point) ? point : [37.6155600, 55.7522200];
        zoom = (zoom) ? zoom : 11;
        let state = {
            "mapCenter": point,
            "mapZoom": zoom
        };
        let title = "Creating map state";
        let url = "#" +
            point[0] + ',' +
            point[1] + ',' +
            zoom;
        map.setCenter(point);
        map.setZoom(zoom);
        window.history.pushState(state, title, url);
        map.on("moveend", function () {
            let lng = Math.round(map.getCenter().lng * 100000) / 100000;
            let lat = Math.round(map.getCenter().lat * 100000) / 100000;
            point = [lng, lat];
            zoom = Math.round(map.getZoom());
            state = {
                "mapCenter": point,
                "mapZoom": zoom
            };
            url = "#" +
                point[0] + ',' +
                point[1] + ',' +
                zoom;
            title = "Changing map state";
            window.history.pushState(state, title, url);
        });
    } else {
        point = (point) ? point : [55.7522200, 37.6155600];
        zoom = (zoom) ? zoom : 11;
        let state = {
            "mapCenter": point,
            "mapZoom": zoom
        };
        let title = "Creating map state";
        let url = "#" +
            point[0] + ',' +
            point[1] + ',' +
            zoom;
        map.setView(point, zoom);
        window.history.pushState(state, title, url);
        map.on("moveend", function () {
            let lng = Math.round(map.getCenter().lng * 100000) / 100000;
            let lat = Math.round(map.getCenter().lat * 100000) / 100000;
            point = [lat, lng];
            zoom = Math.round(map.getZoom());
            state = {
                "mapCenter": point,
                "mapZoom": zoom
            };
            url = "#" +
                point[0] + ',' +
                point[1] + ',' +
                zoom;
            title = "Changing map state";
            window.history.pushState(state, title, url);
        });
    }
}

// Leaflet functions (mgtniip.js)
function getOpacity(z) {
    switch(z) {
        case 10:
            return [0.4, 3, 0.6];
        case 11:
            return [0.3, 4, 0.5];
        case 12:
            return [0.2, 5, 0.4];
        case 13:
            return [0.1, 5, 0.3];
        case 14:
            return [0.0, 5, 0.2];
        case 15:
            return [0.0, 5, 0.2];
        case 16:
            return [0.0, 5, 0.2];
        case 17:
            return [0.0, 5, 0.2];
        case 18:
            return [0.0, 5, 0.2];
        default:
            return [0.0, 5, 0.0];
    }
}
function getOlProperties(z) {
    switch(z) {
        case 10:
            return [1, 448];
        case 11:
            return [1, 224];
        case 12:
            return [1, 112];
        case 13:
            return [1.5, 56];
        case 14:
            return [2, 28];
        case 15:
            return [2.5, 14];
        case 16:
            return [3, 8];
        case 17:
            return [3, 4];
        case 18:
            return [4, 3];
        case 19:
            return [4, 2];
        case 20:
            return [4, 1.2];
        case 21:
            return [4, 1];
        case 22:
            return [4, 0.7];
        default:
            return [1, 1.5];
    }
}




function gradient3Color(objA, objS, maxN, number, maxColor, halfColor, minColor) {
    let minN = 0;
    let color = "";
    let maxArr = maxColor.trim().slice(4, maxColor.length-1).split(",");
    for (let i = 0, length = maxArr.length; i < length; i++) {
        maxArr[i] = parseInt(maxArr[i], 10);
    }
    let halfArr = halfColor.trim().slice(4, halfColor.length-1).split(",");
    for (let i = 0, length = halfArr.length; i < length; i++) {
        halfArr[i] = parseInt(halfArr[i], 10);
    }
    let minArr = minColor.trim().slice(4, minColor.length-1).split(",");
    for (let i = 0, length = minArr.length; i < length; i++) {
        minArr[i] = parseInt(minArr[i], 10);
    }
    let stepRedA = (halfArr[0] - minArr[0]) / 50;
    let stepRedB = (maxArr[0] - halfArr[0]) / 50;

    let stepGreenA = (halfArr[1] - minArr[1]) / 50;
    let stepGreenB = (maxArr[1] - halfArr[1]) / 50;

    let stepBlueA = (halfArr[2] - minArr[2]) / 50;
    let stepBlueB = (maxArr[2] - halfArr[2]) / 50;

    let rgb = [];
    objA.style.color = "rgb(" + maxArr[0] + "," + maxArr[1] + "," + maxArr[2] + ")";
    objA.innerHTML = maxN;

    function getColor(n) {
        if (n <= minN) {
            color = "rgb(" + minArr[0] + "," + minArr[1] + "," + minArr[2] + ")";
        } else if (n >= maxN) {
            color = "rgb(" + maxArr[0] + "," + maxArr[1] + "," + maxArr[2] + ")";
        } else if (n === (maxN / 2)) {
            color = "rgb(" + halfArr[0] + "," + halfArr[1] + "," + halfArr[2] + ")";
        } else  if (n < (maxN / 2)) {
            let percent = n / maxN * 100;
            rgb[0] = Math.floor(stepRedA * percent + minArr[0]);
            rgb[1] = Math.floor(stepGreenA * percent + minArr[1]);
            rgb[2] = Math.floor(stepBlueA * percent + minArr[2]);
            color = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
        } else  if (n > (maxN / 2)) {
            let percent = (n / maxN * 100) - 50;
            red = Math.floor(stepRedB * percent + halfArr[0]);
            green = Math.floor(stepGreenB * percent + halfArr[1]);
            blue = Math.floor(stepBlueB * percent + halfArr[2]);
            color = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
        }
    }
    getColor(number);

    objS.innerHTML = number;
    objS.style.color = color;
}


// translit en to ru //

function translit(inp) { //перевод "раскладки" получаемой строки
    let hash = {};
    let enKeymap = "qwertyuiop[]asdfghjkl;'zxcvbnm,./`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?~";
    let ruKeymap = "йцукенгшщзхъфывапролджэячсмитьбю.ёЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,Ё";
    for (let i in enKeymap){
        hash[enKeymap[i]] = ruKeymap[i];
    }
    let string = "";
    for (let i in inp) {
        if (hash[inp[i]]) {
            string += hash[inp[i]];
        }
        else {
            string += inp[i];
        }
    }
    return string;
}
// Accordion
function acc_single(el) {
    // Подразумевается, что есть чекбокс, состоящий из инпута и лейбла, а после нгего идет блок, который будет раскрываться по чеку.
    let sibling = el.parentElement.lastElementChild;
    el.addEventListener("click", function () {
        (el.checked ? sibling.style.maxHeight = sibling.scrollHeight + "px" : sibling.style.maxHeight = null);
    });
}

let searchBox = document.getElementsByClassName("search_box");
for (let i = 0, cnt = searchBox.length; i < cnt; i++) {
    let input = searchBox[i].firstElementChild.firstElementChild;
    let searchClose = searchBox[i].firstElementChild.lastElementChild;
    let result = searchBox[i].lastElementChild;
    input.addEventListener("keydown", function() {
        if (searchBox[i].classList.contains("active") === false) {
            searchBox[i].classList.add("active");
        }
        if (input.value.length <= 0) {
            searchBox[i].classList.add("hide_result");
        } else if (input.value.length > 0) {
            searchBox[i].classList.remove("hide_result");
        }
    });
    searchClose.addEventListener("click", function() {
        searchBox[i].classList.remove("active");
        input.value = "";
    });
}
function arrowU(c) {
    let row = document.getElementsByClassName(c);
    if (row) {
        let f = document.querySelector(":focus");
        let x = 0;
        for (let r = 0, rCnt = row.length; r < rCnt; r++) {
            if (f !== row[r]) {
                x++;
            }
        }
        if (x === row.length) {
            row[row.length - 1].focus();
        } else {
            for (let r = 0, rCnt = row.length; r < rCnt; r++) {
                if (f === row[r]) {
                    if (r !== 0) {
                        row[--r].focus();
                    } else {
                        r = rCnt - 1;
                        row[r].focus();
                    }
                    break;
                }
            }
        }
    }
}
function arrowD(c) {
    let row = document.getElementsByClassName(c);
    if (row) {
        let f = document.querySelector(":focus");
        let x = 0;
        for (let r = 0, rCnt = row.length; r < rCnt; r++) {
            if (f !== row[r]) {
                x++;
            }
        }
        if (x === row.length) {
            row[0].focus();
        } else {
            for (let r = 0, rCnt = row.length; r < rCnt; r++) {
                if (f === row[r]) {
                    if (r !== (rCnt - 1)) {
                        r = ++r;
                        row[r].focus();
                    } else {
                        r = 0;
                        row[r].focus();
                    }
                    break;
                }
            }
        }
    }
}




let popupInfo = document.getElementsByClassName("info_pic");
for (let i = 0, cnt = popupInfo.length; i < cnt; i++) {
    popupInfo[i].addEventListener("click", function() {
        this.classList.toggle("open");
    });
}

let selectBar = document.getElementsByClassName("mgtniip_selection_bar");
for (let i = 0, selectBarLength = selectBar.length; i < selectBarLength; i++) {
    selectBar[i].addEventListener("click", function() {
        this.classList.toggle("pressed");
    });
}