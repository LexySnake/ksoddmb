/***********************************************************************************************************************
*                                                  AIS KSODD v5.4.4                                                 *
*                                All rights reserved © 2018 | MosgortransNIIproekt, SUE                                *
***********************************************************************************************************************/

let projectPath = "../templates/ais_ksodd_l-v5.4.4/";

let newDomain = "https://niikeeper.com/ksoddAPI/v2.1.0/";
let currentDate = new Date();
let newToken = "";
let tokenExpire = "";
if (!localStorage.getItem("token")) {
    window.location.assign("ais_ksodd_auth.html");
    // window.location.assign("https://niikeeper.com/authentication");
} else {
    newToken = JSON.parse(localStorage.getItem("token")).token;
    tokenExpire = JSON.parse(localStorage.getItem("token")).expire;
    let usrLogin = document.getElementById("user_login");
    let usrLogout = document.getElementById("logout");
    usrLogin.innerHTML = localStorage.getItem("user") || "MTP_user";
    usrLogout.addEventListener("click", function() {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        window.location.assign("ais_ksodd_auth.html");
        // window.location.assign("https://niikeeper.com/authentication");
    });
    (async() => {
        try {
            let testResponse = await fetch(newDomain + "referenceInfo", {
                method: "GET",
                headers: {
                    "Authorization": "Bearer " + newToken
                }
            });
            let testData = await testResponse.json();
            if (testData.Error) {
                localStorage.removeItem("token");
                window.location.assign("ais_ksodd_auth.html");
                // window.location.assign("https://niikeeper.com/authentication");
            }
        } catch(e) {
            console.log(e);
        }
    })();
}
let expireDate = new Date(tokenExpire);
if (expireDate < currentDate) {
    localStorage.removeItem("token");
    window.location.assign("ais_ksodd_auth.html");
    // window.location.assign("https://niikeeper.com/authentication");
}

let checkbox = document.getElementsByClassName("checkbox");
let signsEl = document.getElementById("signs");
let linearMarkEl = document.getElementById("linear_mark");
let pointMarkEl = document.getElementById("point_mark");
let zones_3_27_El = document.getElementById("zones_3_27");
let roadSidesEl = document.getElementById("road_sides");
let segStEl = document.getElementById("segment_streets");
let segIntEl = document.getElementById("segment_intersections");
let zoomBox = document.createElement("div");
let parkingEl = document.getElementById("parking");
let parking;
zoomBox.id = "zoom_box";

let globalZoom = 0;
let domain = "https://niikeeper.com/nii_api/v0.5.9/";
let getPolygon = "get_polygon";
let token = "?token=xMKNeZPavLESGxkSJlGeDg8PwIYS54yz";
let tileCoef = 0.001;
let divObj = {
    "type": "FeatureCollection",
    "features": []
};
let distObj = {
    "type": "FeatureCollection",
    "features": []
};
let sO = {};

let refObj = {};
let strObj = {
    "type": "FeatureCollection",
    "features": []
};
let segObj = {
    "type": "FeatureCollection",
    "features": []
};
let objMap = {
    "division": {
        obj: divObj,
        weight: 2,
        color: "#49ee97",
        fill: "#00ff79",
        strokePane: "divStrokePane",
        pane: "divUnitPane",
        allColor: "#23bb6b",
        allPane: "divAllPane",
        hoverFill: "#23bb6b"
    },
    "district": {
        obj: distObj,
        weight: 0,
        color: "#45aef8",
        fill: "#00b4ff",
        strokePane: "distStrokePane",
        pane: "distUnitPane",
        allColor: "#3362ab",
        allPane: "distAllPane",
        hoverFill: "#3362ab"
    },
    "segment": {
        obj: segObj,
        weight: 0,
        color: "#ff3aea",
        fill: "#ff3aea",
        strokePane: "segStrokePane",
        pane: "segUnitPane",
        allColor: "#bb15d9",
        allPane: "segAllPane",
        hoverFill: "#ff3aea"
    }
};
let mappos = L.Permalink.getMapLocation(11, [55.7522200, 37.6155600]);
let map = L.map("map", {
    center: mappos.center,
    attributionControl: false,
    zoomControl: true,
    minZoom: 10,
    maxZoom: 22,
    zoom: mappos.zoom,
    closePopupOnClick: false
});
map.createPane("divAllPane");
map.getPane("divAllPane").style.zIndex = "410";
map.createPane("divStrokePane");
map.getPane("divStrokePane").style.zIndex = "415";
map.createPane("divUnitPane");
map.getPane("divUnitPane").style.zIndex = "420";
map.createPane("distAllPane");
map.getPane("distAllPane").style.zIndex = "430";
map.createPane("distStrokePane");
map.getPane("distStrokePane").style.zIndex = "435";
map.createPane("distUnitPane");
map.getPane("distUnitPane").style.zIndex = "440";
map.createPane("segAllPane");
map.getPane("segAllPane").style.zIndex = "450";
map.createPane("segStrokePane");
map.getPane("segStrokePane").style.zIndex = "455";
map.createPane("segUnitPane");
map.getPane("segUnitPane").style.zIndex = "460";
map.createPane("rsPane");
map.getPane("rsPane").style.zIndex = "470";
map.createPane("zPane");
map.getPane("zPane").style.zIndex = "480";
zoomBox.innerHTML = mappos.zoom;
let zoom = document.getElementsByClassName("leaflet-control-zoom");
for (let z = 0, cnt = zoom.length; z < cnt; z++) {
    zoom[z].className += " mgtniip_zoom";
    zoom[z].appendChild(zoomBox);
}

L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
    attribution: "Map data &copy; <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"http://mapbox.com\">Mapbox</a>",
    maxZoom: 22,
    id: "mapbox.streets",
    accessToken: "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw"
}).addTo(map);
L.Permalink.setup(map);
map.getRenderer(map).options.padding = 2000;
let currentOl = new window.App.Overlaps("current", "olCurrent", null, 1.0, " solid ", document.getElementById("ol_current"));
currentOl.download(map);
let futureOl = new window.App.Overlaps("new", "olFuture", [15,15], 0.5, " dashed ", document.getElementById("ol_future"));
futureOl.download(map);
let pastOl = new window.App.Overlaps("old", "olPast", null, 1.0, " solid ", document.getElementById("ol_past"), ["#aaa", "#aaa", "#aaa", "#aaa", "#aaa", "#aaa", "#aaa"]);
pastOl.download(map);

let renovations = new window.App.Renovations();
renovations.download(map);

let signsGroup = L.layerGroup();
let ololoGroup = {};
let tralalaGroup = {};
let pointMarkGroup = L.layerGroup();
let linearMarkGroup = L.layerGroup();
let linearMarkTextGroup = L.layerGroup();
let divisionsAll = L.layerGroup();
let divisionsUnit = L.layerGroup();
let districtsAll = L.layerGroup();
let districtsUnit = L.layerGroup();
let segIntGroup = L.layerGroup();
let segStGroup = L.layerGroup();
let streetsGroup = L.layerGroup();
let zones_3_27_Group = L.layerGroup();
let roadSidesGroup = L.layerGroup();
let sBox = document.getElementById("search");
let sInp = document.getElementById("search_string");
let sClose = sInp.nextElementSibling;
let sEmpty = document.getElementById("empty");
let sRes = document.getElementById("ksodd_result");
let yaBtn = document.getElementById("ya_btn");
let yaRes = document.getElementById("ya_result");
let searchGroup = L.layerGroup();
let hoverGroup = L.layerGroup();
let parkingGroup = L.layerGroup();


signsGroup.addTo(map);
signsEl.checked = true;
pointMarkGroup.addTo(map);
pointMarkEl.checked = true;
linearMarkGroup.addTo(map);
linearMarkEl.checked = true;
downloadPolygons(divObj, distObj, refObj, strObj, segObj, sO);
download_3_27_Zones(zones_3_27_Group);
downloadRoadSides(roadSidesGroup);


streetsGroup.remove();
segIntGroup.remove();
segStGroup.remove();
zones_3_27_Group.remove();
roadSidesGroup.remove();

map.createPane("divisionsAllPane");
map.getPane("divisionsAllPane").style.zIndex = "410";
map.createPane("divisionsUnitPane");
map.getPane("divisionsUnitPane").style.zIndex = "420";
map.createPane("districtsAllPane");
map.getPane("districtsAllPane").style.zIndex = "430";
map.createPane("districtsUnitPane");
map.getPane("districtsUnitPane").style.zIndex = "440";
map.createPane("olPast");
map.getPane("olPast").style.zIndex = "605";
map.createPane("olCurrent");
map.getPane("olCurrent").style.zIndex = "610";
map.createPane("olFuture");
map.getPane("olFuture").style.zIndex = "620";

map.on("layerremove", function (event) {
    switch (event.layer) {
        case linearMarkGroup:
            map.removeLayer(linearMarkTextGroup);
            break;
        default:
            break;
    }
});
map.on("zoomend", function () {
    signsGroup.clearLayers();
    for (let k in ololoGroup) {
        if (map.hasLayer(ololoGroup[k])) {
            ololoGroup[k].clearLayers();
            map.removeLayer(ololoGroup[k]);
        }
        ololoGroup = {};
    }
    linearMarkGroup.clearLayers();
    linearMarkTextGroup.clearLayers();
    for (let k in tralalaGroup) {
        if (map.hasLayer(tralalaGroup[k])) {
            tralalaGroup[k].clearLayers();
            map.removeLayer(tralalaGroup[k]);
        }
        tralalaGroup = {};
    }
});

downloadParking();

map.eachLayer(function (layer) {
    layer.on("load", function () {
        let zoomLevel = map.getZoom();
        globalZoom = zoomLevel;
        zoomBox.innerHTML = zoomLevel;
        let zoomCoef = 0;
        let coefficient = 1;
        let lineAnchor = 0;
        let meterAnchor = 0;
        let signText = "";
        if (zoomLevel < 14) {
            parkingEl.checked = false;
            parkingEl.disabled = true;
            parkingGroup.remove();
        } else {
            parkingEl.disabled = false;
        }
        if (zoomLevel <= 18) {
            signsGroup.clearLayers();
            pointMarkGroup.clearLayers();
            linearMarkGroup.clearLayers();
            linearMarkTextGroup.clearLayers();
            return;
        } else {
            signsEl.disabled = false;
            linearMarkEl.disabled = false;
            pointMarkEl.disabled = false;
            switch (zoomLevel) {
                case 19:
                    zoomCoef = 1;
                    coefficient = 1;
                    signText = "5px";
                    lineAnchor = 25;
                    meterAnchor = 2.2;
                    tileCoef = 0.001;
                    break;
                case 20:
                    zoomCoef = 2;
                    coefficient = 2;
                    signText = "9px";
                    lineAnchor = 20;
                    meterAnchor = 1;
                    tileCoef = 0.0005;
                    break;
                case 21:
                    zoomCoef = 3;
                    coefficient = 4;
                    signText = "14px";
                    lineAnchor = 10;
                    meterAnchor = 0.45;
                    tileCoef = 0.00025;
                    break;
                case 22:
                    zoomCoef = 4;
                    coefficient = 6;
                    signText = "19px";
                    lineAnchor = 10;
                    meterAnchor = 0.15;
                    tileCoef = 0.00013;
                    break;
                default:
                    zoomCoef = 1;
                    coefficient = 1;
                    lineAnchor = 0;
                    meterAnchor = 0;
                    tileCoef = 0.001;
            }
        }
        let reqData = {
            "type": "all",
            "zoom": zoomLevel,
            "leftbot": {
                "longitude": map.getBounds().getWest() - tileCoef,
                "latitude": map.getBounds().getSouth() - tileCoef
            },
            "topright": {
                "longitude": map.getBounds().getEast() + tileCoef,
                "latitude": map.getBounds().getNorth() + tileCoef
            }
        };
        if (map.hasLayer(signsGroup) === true) {
            downloadSigns(reqData, map, signsGroup, projectPath, zoomCoef, coefficient, signText, ololoGroup);
        }
        if (map.hasLayer(pointMarkGroup) === true) {
            downloadPointMark(reqData, pointMarkGroup, zoomLevel, projectPath);
        }
        if (map.hasLayer(linearMarkGroup) === true) {
            downloadLinearMark(reqData, linearMarkGroup, linearMarkTextGroup, zoomLevel, map, meterAnchor, tralalaGroup);
            linearMarkTextGroup.addTo(map);
        }
    });
});

for (let ch = 0, chCnt = checkbox.length; ch < chCnt; ch++) {
    checkbox[ch].addEventListener("click", function () {
        switch (this) {
            case signsEl:
                if (this.checked === true) {
                    signsGroup.addTo(map);
                } else {
                    signsGroup.remove();
                }
                break;
            case linearMarkEl:
                if (this.checked === true) {
                    linearMarkGroup.addTo(map);
                } else {
                    linearMarkGroup.remove();
                }
                break;
            case pointMarkEl:
                if (this.checked === true) {
                    pointMarkGroup.addTo(map);
                } else {
                    pointMarkGroup.remove();
                }
                break;
            case zones_3_27_El:
                if (this.checked === true) {
                    zones_3_27_Group.addTo(map);
                } else {
                    zones_3_27_Group.remove();
                }
                break;
            case roadSidesEl:
                if (this.checked === true) {
                    roadSidesGroup.addTo(map);
                } else {
                    roadSidesGroup.remove();
                }
                break;
            case segStEl:
                if (this.checked === true) {
                    segStGroup.addTo(map);
                } else {
                    segStGroup.remove();
                }
                break;
            case segIntEl:
                if (this.checked === true) {
                    segIntGroup.addTo(map);
                } else {
                    segIntGroup.remove();
                }
                break;
            case parkingEl:
                if ((this.checked === true) && (map.getZoom() > 13)) {
                    parkingGroup.addTo(map);
                } else {
                    parkingGroup.remove();
                }
                break;
            default:
                break;
        }
    });
}