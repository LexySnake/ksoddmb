let checkbox = document.getElementsByClassName("checkbox");

let zones_3_27_El = document.getElementById("zones_3_27");
let roadSidesEl = document.getElementById("road_sides");
let segStEl = document.getElementById("segment_streets");
let segIntEl = document.getElementById("segment_intersections");

let parkingEl = document.getElementById("parking");
let sBox = document.getElementById("search");
let sInp = document.getElementById("search_string");
let sClose = sInp.nextElementSibling;
let sEmpty = document.getElementById("empty");
let sRes = document.getElementById("ksodd_result");
let yaBtn = document.getElementById("ya_btn");
let yaRes = document.getElementById("ya_result");

let overlaps = document.getElementById("overlaps");
let ol = document.getElementsByClassName("ol");
let olFuture = document.getElementById("ol_future");
let olPast = document.getElementById("ol_past");
let olCurrent = document.getElementById("ol_current");
