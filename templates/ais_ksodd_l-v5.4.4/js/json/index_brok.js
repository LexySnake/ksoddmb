/***********************************************************************************************************************
*                                                  AIS KSODD v5.4.4                                                 *
*                                All rights reserved © 2018 | MosgortransNIIproekt, SUE                                *
***********************************************************************************************************************/

let projectPath = window.App.LPATH;
let domain = window.App.OLD_DOMAIN;
let getPolygon = "get_polygon";
let token = "?token=xMKNeZPavLESGxkSJlGeDg8PwIYS54yz";

let parking;

let globalZoom = 0;
let tileCoef = 0.001;

let divObj = {
    "type": "FeatureCollection",
    "features": []
};
let distObj = {
    "type": "FeatureCollection",
    "features": []
};
let sO = {}; // Поиск
let refObj = {};
let strObj = {
    "type": "FeatureCollection",
    "features": []
};
let segObj = {
    "type": "FeatureCollection",
    "features": []
};
let objMap = {
    "division": {
        obj: divObj,
        weight: 2,
        color: "#49ee97",
        fill: "#00ff79",
        strokePane: "divStrokePane",
        pane: "divUnitPane",
        allColor: "#23bb6b",
        allPane: "divAllPane",
        hoverFill: "#23bb6b"
    },
    "district": {
        obj: distObj,
        weight: 0,
        color: "#45aef8",
        fill: "#00b4ff",
        strokePane: "distStrokePane",
        pane: "distUnitPane",
        allColor: "#3362ab",
        allPane: "distAllPane",
        hoverFill: "#3362ab"
    },
    "segment": {
        obj: segObj,
        weight: 0,
        color: "#ff3aea",
        fill: "#ff3aea",
        strokePane: "segStrokePane",
        pane: "segUnitPane",
        allColor: "#bb15d9",
        allPane: "segAllPane",
        hoverFill: "#ff3aea"
    }
};


let zoomBox = document.createElement("div");
zoomBox.id = "zoom_box";
zoomBox.innerHTML = mappos.zoom;
let zoom = document.getElementsByClassName("leaflet-control-zoom");
for (let z = 0, cnt = zoom.length; z < cnt; z++) {
    zoom[z].className += " mgtniip_zoom";
    zoom[z].appendChild(zoomBox);
}

let currentOl = new window.App.Overlaps("current", "olCurrent", null, 1.0, " solid ");
currentOl.download(map);
let pastOl = new window.App.Past("old");
pastOl.download(map);
let futureOl = new window.App.Overlaps("new", "olFuture", [15,15], 0.5, " dashed ");
futureOl.download(map);
overlaps.addEventListener("click", function() {
    if (this.checked) {
        olCurrent.checked = true;
        olFuture.checked = true;
        for (let i = 0; i < ol.length - 1; i++) {
            ol[i].checked = true;
            currentOl.overlapsGroup[i].addTo(map);
            futureOl.overlapsGroup[i].addTo(map);
        }
        ol[ol.length-1].checked = true;
        currentOl.unconfirmedGroup.addTo(map);
        futureOl.unconfirmedGroup.addTo(map);
    } else {
        olCurrent.checked = false;
        olFuture.checked = false;
        olPast.checked = false;
        for (let i = 0; i < ol.length - 1; i++) {
            ol[i].checked = false;
            currentOl.overlapsGroup[i].remove();
            futureOl.overlapsGroup[i].remove();
            pastOl.overlapsGroup[i].remove();
        }
        ol[ol.length-1].checked = false;
        currentOl.unconfirmedGroup.remove();
        futureOl.unconfirmedGroup.remove();
        pastOl.unconfirmedGroup.remove();
    }
});
olCurrent.addEventListener("click", function() {
    if (this.checked) {
        for (let i = 0; i < ol.length - 1; i++) {
            if (ol[i].checked) {
                currentOl.overlapsGroup[i].addTo(map);
            } else {
                currentOl.overlapsGroup[i].remove();
            }
        }
        if (ol[ol.length-1].checked) {
            currentOl.unconfirmedGroup.addTo(map);
        } else {
            currentOl.unconfirmedGroup.remove();
        }
    } else {
        for (let i = 0; i < ol.length - 1; i++) {
            currentOl.overlapsGroup[i].remove();
        }
        currentOl.unconfirmedGroup.remove();
    }
});
olFuture.addEventListener("click", function() {
    if (this.checked) {
        for (let i = 0; i < ol.length - 1; i++) {
            if (ol[i].checked) {
                futureOl.overlapsGroup[i].addTo(map);
            } else {
                futureOl.overlapsGroup[i].remove();
            }
        }
        if (ol[ol.length-1].checked) {
            futureOl.unconfirmedGroup.addTo(map);
        } else {
            futureOl.unconfirmedGroup.remove();
        }
    } else {
        for (let i = 0; i < ol.length - 1; i++) {
            futureOl.overlapsGroup[i].remove();
        }
        futureOl.unconfirmedGroup.remove();
    }
});
olPast.addEventListener("click", function() {
    if (this.checked) {
        for (let i = 0; i < ol.length - 1; i++) {
            if (ol[i].checked) {
                pastOl.overlapsGroup[i].addTo(map);
            } else {
                pastOl.overlapsGroup[i].remove();
            }
        }
        if (ol[ol.length-1].checked) {
            pastOl.unconfirmedGroup.addTo(map);
        } else {
            pastOl.unconfirmedGroup.remove();
        }
    } else {
        for (let i = 0; i < ol.length - 1; i++) {
            pastOl.overlapsGroup[i].remove();
        }
        pastOl.unconfirmedGroup.remove();
    }
});
for (let i = 0, cnt = ol.length - 1; i < cnt; i++) {
    ol[i].addEventListener("click", function () {
        if (this.checked) {
            (olCurrent.checked ? currentOl.overlapsGroup[i].addTo(map) : currentOl.overlapsGroup[i].remove());
            (olFuture.checked ? futureOl.overlapsGroup[i].addTo(map) : futureOl.overlapsGroup[i].remove());
            (olPast.checked ? pastOl.overlapsGroup[i].addTo(map) : pastOl.overlapsGroup[i].remove());
        } else {
            currentOl.overlapsGroup[i].remove();
            futureOl.overlapsGroup[i].remove();
            pastOl.overlapsGroup[i].remove();
        }
    });
}
ol[ol.length-1].addEventListener("click", function () {
    if (this.checked) {
        (olCurrent.checked ? currentOl.unconfirmedGroup.addTo(map) : currentOl.unconfirmedGroup.remove());
        (olFuture.checked ? futureOl.unconfirmedGroup.addTo(map) : futureOl.unconfirmedGroup.remove());
        (olPast.checked ? pastOl.unconfirmedGroup.addTo(map) : pastOl.unconfirmedGroup.remove());
    } else {
        currentOl.unconfirmedGroup.remove();
        futureOl.unconfirmedGroup.remove();
        pastOl.unconfirmedGroup.remove();
    }
});
acc_single(overlaps);

signsGroup.addTo(map);
signsEl.checked = true;
pointMarkGroup.addTo(map);
pointMarkEl.checked = true;
linearMarkGroup.addTo(map);
linearMarkEl.checked = true;
downloadPolygons(divObj, distObj, refObj, strObj, segObj, sO);
download_3_27_Zones(zones_3_27_Group);
downloadRoadSides(roadSidesGroup);
downloadParking();
map.on("layerremove", function (event) {
    switch (event.layer) {
        case linearMarkGroup:
            map.removeLayer(linearMarkTextGroup);
            break;
        default:
            break;
    }
});
map.on("zoomend", function () {
    signsGroup.clearLayers();
    for (let k in ololoGroup) {
        if (map.hasLayer(ololoGroup[k])) {
            ololoGroup[k].clearLayers();
            map.removeLayer(ololoGroup[k]);
        }
        ololoGroup = {};
    }
    linearMarkGroup.clearLayers();
    linearMarkTextGroup.clearLayers();
    for (let k in tralalaGroup) {
        if (map.hasLayer(tralalaGroup[k])) {
            tralalaGroup[k].clearLayers();
            map.removeLayer(tralalaGroup[k]);
        }
        tralalaGroup = {};
    }
});
map.eachLayer(function (layer) {
    layer.on("load", function () {
        let zoomLevel = map.getZoom();
        globalZoom = zoomLevel;
        zoomBox.innerHTML = zoomLevel;
        let zoomCoef = 0;
        let coefficient = 1;
        let lineAnchor = 0;
        let meterAnchor = 0;
        let signText = "";
        if (zoomLevel < 14) {
            parkingEl.checked = false;
            parkingEl.disabled = true;
            parkingGroup.remove();
        } else {
            parkingEl.disabled = false;
        }
        if (zoomLevel <= 18) {
            signsGroup.clearLayers();
            pointMarkGroup.clearLayers();
            linearMarkGroup.clearLayers();
            linearMarkTextGroup.clearLayers();
            return;
        } else {
            signsEl.disabled = false;
            linearMarkEl.disabled = false;
            pointMarkEl.disabled = false;
            switch (zoomLevel) {
                case 19:
                    zoomCoef = 1;
                    coefficient = 1;
                    signText = "5px";
                    lineAnchor = 25;
                    meterAnchor = 2.2;
                    tileCoef = 0.001;
                    break;
                case 20:
                    zoomCoef = 2;
                    coefficient = 2;
                    signText = "9px";
                    lineAnchor = 20;
                    meterAnchor = 1;
                    tileCoef = 0.0005;
                    break;
                case 21:
                    zoomCoef = 3;
                    coefficient = 4;
                    signText = "14px";
                    lineAnchor = 10;
                    meterAnchor = 0.45;
                    tileCoef = 0.00025;
                    break;
                case 22:
                    zoomCoef = 4;
                    coefficient = 6;
                    signText = "19px";
                    lineAnchor = 10;
                    meterAnchor = 0.15;
                    tileCoef = 0.00013;
                    break;
                default:
                    zoomCoef = 1;
                    coefficient = 1;
                    lineAnchor = 0;
                    meterAnchor = 0;
                    tileCoef = 0.001;
            }
        }
        let reqData = {
            "type": "all",
            "zoom": zoomLevel,
            "leftbot": {
                "longitude": map.getBounds().getWest() - tileCoef,
                "latitude": map.getBounds().getSouth() - tileCoef
            },
            "topright": {
                "longitude": map.getBounds().getEast() + tileCoef,
                "latitude": map.getBounds().getNorth() + tileCoef
            }
        };
        if (map.hasLayer(signsGroup) === true) {
            downloadSigns(reqData, map, signsGroup, projectPath, zoomCoef, coefficient, signText, ololoGroup);
        }
        if (map.hasLayer(pointMarkGroup) === true) {
            downloadPointMark(reqData, pointMarkGroup, zoomLevel, projectPath);
        }
        if (map.hasLayer(linearMarkGroup) === true) {
            downloadLinearMark(reqData, linearMarkGroup, linearMarkTextGroup, zoomLevel, map, meterAnchor, tralalaGroup);
            linearMarkTextGroup.addTo(map);
        }
    });
});
for (let ch = 0, chCnt = checkbox.length; ch < chCnt; ch++) {
    checkbox[ch].addEventListener("click", function () {
        switch (this) {
            case signsEl:
                if (this.checked === true) {
                    signsGroup.addTo(map);
                } else {
                    signsGroup.remove();
                }
                break;
            case linearMarkEl:
                if (this.checked === true) {
                    linearMarkGroup.addTo(map);
                } else {
                    linearMarkGroup.remove();
                }
                break;
            case pointMarkEl:
                if (this.checked === true) {
                    pointMarkGroup.addTo(map);
                } else {
                    pointMarkGroup.remove();
                }
                break;
            case zones_3_27_El:
                if (this.checked === true) {
                    zones_3_27_Group.addTo(map);
                } else {
                    zones_3_27_Group.remove();
                }
                break;
            case roadSidesEl:
                if (this.checked === true) {
                    roadSidesGroup.addTo(map);
                } else {
                    roadSidesGroup.remove();
                }
                break;
            case segStEl:
                if (this.checked === true) {
                    segStGroup.addTo(map);
                } else {
                    segStGroup.remove();
                }
                break;
            case segIntEl:
                if (this.checked === true) {
                    segIntGroup.addTo(map);
                } else {
                    segIntGroup.remove();
                }
                break;
            case parkingEl:
                if ((this.checked === true) && (map.getZoom() > 13)) {
                    parkingGroup.addTo(map);
                } else {
                    parkingGroup.remove();
                }
                break;
            default:
                break;
        }
    });
}