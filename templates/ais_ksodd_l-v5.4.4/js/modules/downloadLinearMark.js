function downloadLinearMark(reqData, linearMarkGroup, linearMarkTextGroup, zoomLevel, map, meterAnchor, tralalaGroup) {
    (async() => {
        try {
            let response = await fetch(domain + "roadmarkup_go_box_post" + token, {
                method: "POST",
                body: JSON.stringify(reqData)
            });
            let data = await response.json();
            for (let k in data.data) {
                if(!map.hasLayer(tralalaGroup[k])) {
                    tralalaGroup[k] = L.layerGroup();
                    let line = data.data[k].linestrings;
                    let arc = data.data[k].arcs;
                    let lineNumber = data.data[k].road_markup_number;
                    let lineArr = [];
                    if (line.length !== 0) {
                        for (let i = 0, cnt = line.length; i < cnt; i++) {
                            lineArr.push([line[i][1], line[i][0]]);
                        }
                    }
                    if (lineArr.length !== 0) {
                        let pointPerMeter = L.GeometryUtil.destination(L.latLng(lineArr[0]), 0, 1);
                        let meter = L.GeometryUtil.distance(map, L.latLng(lineArr[0]), pointPerMeter); // длина 1 метра
                        let lineOptions = {
                            color: "#666",
                            weight: meter * 0.1,
                            opacity: 1
                        };
                        switch (lineNumber) {
                            case "1.1":
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.2.1":
                                alert("1.2.1");
                                lineOptions.weight = meter * 0.2;
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.2.2":
                                alert("1.2.2");
                                lineOptions.dashArray = (meter * 0.1) + "," + (meter * 0.2);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.3":
                                unitLineIcon(lineArr, meter, meterAnchor, double);
                                break;
                            case "1.4":
                                lineOptions.color = "#ff0";
                                lineOptions.weight = (meter * 0.15);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.5":
                                lineOptions.dashArray = meter + "," + (meter * 3);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.6":
                                lineOptions.dashArray = (meter * 3) + "," + meter;
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.7":
                                lineOptions.dashArray = (meter * 0.5) + "," + (meter * 0.5);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.8":
                                lineOptions.dashArray = meter + "," + (meter * 3);
                                lineOptions.weight = meter * 0.2;
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.9":
                                unitLineIcon(lineArr, meter, meterAnchor, doubleDashed);
                                break;
                            case "1.10":
                                lineOptions.color = "#ff0";
                                lineOptions.dashArray = meter + "," + meter;
                                lineOptions.weight = (meter * 0.15);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.11":
                                unitLineIcon(lineArr, meter, meterAnchor, doubleDoubleDashed);
                                break;
                            case "1.12":
                                lineOptions.weight = (meter * 0.4);
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                            case "1.13":
                                reverseLineIcon(lineArr, meter, -90, triangle);
                                break;
                            case "1.15":
                                fullLineIcon(lineArr, meter, 90, 1.8 * meter, bike);
                                break;
                            case "1.17":
                                fullLineIcon(lineArr, meter, 90, 2 * meter, bus);
                                break;
                            case "1.25":
                                fullLineIcon(lineArr, meter, 90, 0.8 * meter, chess);
                                break;
                            default:
                                L.polyline(lineArr, lineOptions).addTo(linearMarkGroup);
                                break;
                        }
                    }
                    if ((arc) && (arc.length !== 0)) {
                        let latlng1 = [arc[0][0][0], arc[0][0][1]].reverse();
                        let latlng2 = [arc[0][2][0], arc[0][2][1]].reverse();
                        let latlngMID = [arc[0][1][0], arc[0][1][1]].reverse();
                        let pointPerMeter = L.GeometryUtil.destination(L.latLng(latlng1), 0, 1);
                        let meter = L.GeometryUtil.distance(map, L.latLng(latlng1), pointPerMeter); // длина 1 метра
                        let lineOptions = {
                            color: "#666",
                            weight: meter * 0.1,
                            opacity: 1
                        };
                        switch (lineNumber) {
                            case "1.1":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    opacity: 1
                                };
                                break;
                            case "1.2.1":
                                alert("arc 1.2.1");
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.2,
                                    opacity: 1
                                };
                                break;
                            case "1.2.2":
                                alert("arc 1.2.2");
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    dashArray: (meter * 0.1) + "," + (meter * 0.2),
                                    opacity: 1
                                };
                                break;
                            case "1.4":
                                lineOptions = {
                                    color: "#ff0",
                                    weight: meter * 0.15,
                                    opacity: 1
                                };
                                break;
                            case "1.5":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    dashArray: meter + "," + (meter * 3),
                                    opacity: 1
                                };
                                break;
                            case "1.6":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    dashArray: (meter * 3) + "," + meter,
                                    opacity: 1
                                };
                                break;
                            case "1.7":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    dashArray: (meter * 0.5) + "," + (meter * 0.5),
                                    opacity: 1
                                };
                                break;
                            case "1.8":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.2,
                                    dashArray: meter + "," + (meter * 3),
                                    opacity: 1
                                };
                                break;
                            case "1.10":
                                lineOptions = {
                                    color: "#ff0",
                                    weight: meter * 0.15,
                                    dashArray: meter + "," + meter,
                                    opacity: 1
                                };
                                break;
                            case "1.12":
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.4,
                                    opacity: 1
                                };
                                break;
                            default:
                                lineOptions = {
                                    color: "#666",
                                    weight: meter * 0.1,
                                    opacity: 1
                                };
                                break;
                        }
                        let curvedPath = L.curve(
                            [
                                "M", latlng1,
                                "Q", latlngMID,
                                latlng2
                            ], lineOptions);
                        L.featureGroup([new L.Polyline(lineArr, lineOptions), curvedPath]).addTo(linearMarkGroup);
                    }
                    tralalaGroup[k].addTo(map);
                }
            }
        } catch(e) {
            console.log("catch", e);
        }
    })();
    (async() => {
        try {
            let response = await fetch(domain + "textlabels_go_box_post" + token, {
                method: "POST",
                body: JSON.stringify(reqData)
            });
            let responseData = await response.json();
            if (responseData.length !== 0) {
                linearMarkTextGroup.clearLayers();
            } else {
                return;
            }
            for (let k in responseData.data){
                if (responseData.data.hasOwnProperty(k)) {
                    let textLabels = responseData.data[k].text_labels[0];
                    let textGeomLength = textLabels.geom_label.length;
                    let pointList = [];
                    for (let i = 0; i < textGeomLength; i++) {
                        let geom = [];
                        for (let j = 0; j < 2; j++) {
                            let textGeom = textLabels.geom_label[i][j];
                            geom.push(textGeom);
                        }
                        pointList.push(geom.reverse());
                    }
                    let polylineOptions = {
                        color: "#000000",
                        weight: 1,
                        opacity: 1
                    };
                    let labelPoint = pointList[1]; // Берём серединную точку засечки, т.к. точка в поле geom_text не всегда корректна (бывают случаи, когда сервер отдаёт точку [100, 100])
                    let textRise = 0, textTurn = 180;
                    let x1 = pointList[0][0]; 
                    let x2 = pointList[1][0]; 
                    let x3 = pointList[pointList.length-1][0];
                    let y1 = pointList[0][1];
                    let y2 = pointList[1][1];
                    let y3 = pointList[pointList.length-1][1];
                    let D = (x3 - x1) * (y2 - y1) - (y3 - y1) * (x2 - x1);
                    if ((D < 0)) {
                        textRise = 18;
                        textTurn = 0;
                    }
                    let htmlText =  "<div style=\"font-size:" + (zoomLevel / 2) + "pt; width: 100%;\">"
                                    + "<div>" + textLabels.text + "</div>"
                                    + "</div>";
                    let labelPopup =    "<div>"
                                        + "<div>" + textLabels.status + "</div>"
                                        + "<div>" + textLabels.text + "</div>"
                                        + "<div>" + textLabels.segment_id +"</div>"
                                        + "<div>";
                    let polyline = new L.Polyline(pointList, polylineOptions).bindPopup(labelPopup);
                    let labelIcon = L.divIcon({
                        className: "iconClass",
                        iconAnchor: [0, textRise],
                        html: htmlText
                    });
                    let labelMarker = L.marker(labelPoint, {
                        rotationAngle: (textLabels.angle * 180 / Math.PI) * (-1),
                        icon: labelIcon
                    });
                    L.featureGroup([labelMarker, polyline]).addTo(linearMarkTextGroup,);
                }
            }
        } catch(e) {
            console.log("catch", e);
        }
    })();
}

function double(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*0.3);
    svg.setAttribute("class", "svg");
    let line1 = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line1.setAttribute("x1", 0);
    line1.setAttribute("y1", svgH*0.05);
    line1.setAttribute("x2", svgW);
    line1.setAttribute("y2", svgH*0.05);
    line1.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH*0.1);
    svg.appendChild(line1);
    let line2 = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line2.setAttribute("x1", 0);
    line2.setAttribute("y1", svgH*0.25);
    line2.setAttribute("x2", svgW);
    line2.setAttribute("y2", svgH*0.25);
    line2.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH*0.1);
    svg.appendChild(line2);
    return svg.outerHTML;
}
function doubleDashed(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*0.3);
    svg.setAttribute("class", "svg");
    for (let i = 0; i <= svgW; i = i+svgH*4) {
        let line1 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line1.setAttribute("x1", i);
        line1.setAttribute("y1", svgH*0.05);
        line1.setAttribute("x2", i+(svgH*3));
        line1.setAttribute("y2", svgH*0.05);
        line1.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH*0.1);
        svg.appendChild(line1);
        let line2 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line2.setAttribute("x1", i);
        line2.setAttribute("y1", svgH*0.25);
        line2.setAttribute("x2", i+(svgH*3));
        line2.setAttribute("y2", svgH*0.25);
        line2.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH*0.1);
        svg.appendChild(line2);
    }
    return svg.outerHTML;
}
function doubleDoubleDashed(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*0.3);
    svg.setAttribute("class", "svg");
    for (let i = 0; i <= svgW; i = i+svgH*4) {
        let line1 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line1.setAttribute("x1", i);
        line1.setAttribute("y1", svgH * 0.05);
        line1.setAttribute("x2", i + (svgH * 3));
        line1.setAttribute("y2", svgH * 0.05);
        line1.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH * 0.1);
        svg.appendChild(line1);
    }
    let line2 = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line2.setAttribute("x1", 0);
    line2.setAttribute("y1", svgH*0.25);
    line2.setAttribute("x2", svgW);
    line2.setAttribute("y2", svgH*0.25);
    line2.setAttribute("style", "stroke:#666; stroke-linecap:round; stroke-width:" + svgH*0.1);
    svg.appendChild(line2);
    return svg.outerHTML;
}
function triangle(svgW, svgH) {
    let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.setAttribute('width', svgW);
    svg.setAttribute('height', svgH);
    svg.setAttribute('class', 'svg');
    for (let i=0; i<=svgW; i=i+(svgH*0.5)) {
        let triangle = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
        triangle.setAttribute('points', i+',0 '+(i+svgH*0.3)+',0 '+(i+svgH*0.15)+','+(svgH*0.6));
        triangle.setAttribute('style', 'stroke:#666; fill:#666; stroke-width:' + 0.1);
        svg.appendChild(triangle);
    }
    return svg.outerHTML;
}
function bike(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*1.8);
    svg.setAttribute("class", "svg");
    for (let i = 0; i <= svgW; i = i + svgH*0.8) {
        let line1 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line1.setAttribute("x1", i);
        line1.setAttribute("y1", svgH*0.2);
        line1.setAttribute("x2", i+(svgH*0.4));
        line1.setAttribute("y2", svgH*0.2);
        line1.setAttribute("style", "stroke:#666; stroke-width:" + svgH*0.4);
        svg.appendChild(line1);
        let line2 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line2.setAttribute("x1", i);
        line2.setAttribute("y1", svgH*1.6);
        line2.setAttribute("x2", i+(svgH*0.4));
        line2.setAttribute("y2", svgH*1.6);
        line2.setAttribute("style", "stroke:#666; stroke-width:" + svgH*0.4);
        svg.appendChild(line2);
    }
    return svg.outerHTML;
}
function bus(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*2);
    svg.setAttribute("class", "svg");
    let w = svgW - svgH*4*Math.floor(svgW/(svgH*4));
    let line1 = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line1.setAttribute("x1", w/2);
    line1.setAttribute("y1", 0);
    line1.setAttribute("x2", w/2);
    line1.setAttribute("y2", 2*svgH);
    line1.setAttribute("style", "stroke:#ff0; stroke-linecap:round; stroke-width:" + svgH*0.1);
    svg.appendChild(line1);
    for(let i = w/2; i<=svgW-w; i=i+(svgH*4)) {
        let line2 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line2.setAttribute("x1", i);
        line2.setAttribute("y1", 0);
        line2.setAttribute("x2", i+(svgH*2));
        line2.setAttribute("y2", 2*svgH);
        line2.setAttribute("style", "stroke:#ff0; stroke-linecap:round; stroke-width:" + svgH*0.1);
        svg.appendChild(line2);
        let line3 = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line3.setAttribute("x1", i+(svgH*2));
        line3.setAttribute("y1", 2*svgH);
        line3.setAttribute("x2", i+(svgH*4));
        line3.setAttribute("y2", 0);
        line3.setAttribute("style", "stroke:#ff0; stroke-linecap:round; stroke-width:" + svgH*0.1);
        svg.appendChild(line3);
    }
    let line4 = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line4.setAttribute("x1", svgW-w/2);
    line4.setAttribute("y1", 0);
    line4.setAttribute("x2", svgW-w/2);
    line4.setAttribute("y2", 2*svgH);
    line4.setAttribute("style", "stroke:#ff0; stroke-linecap:round; stroke-width:" + svgH*0.1);
    svg.appendChild(line4);
    return svg.outerHTML;
}
function chess(svgW, svgH) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", svgH*0.8);
    svg.setAttribute("class", "svg");
    for (let i=0; i<=svgW; i=i+svgH*0.8) {
        let chess1 = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        chess1.setAttribute("x", i);
        chess1.setAttribute("y", svgH*0.4);
        chess1.setAttribute("width", svgH*0.4);
        chess1.setAttribute("height", svgH*0.4);
        chess1.setAttribute("style", "fill:#666;");
        svg.appendChild(chess1);
        let chess2 = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        chess2.setAttribute("x", i+svgH*0.4);
        chess2.setAttribute("y", 0);
        chess2.setAttribute("width", svgH*0.4);
        chess2.setAttribute("height", svgH*0.4);
        chess2.setAttribute("style", "fill:#666;");
        svg.appendChild(chess2);
    }
    return svg.outerHTML;
}
function unitLineIcon(lineArr, meter, meterAnchor, f) {
    for (let i = 0, cnt = lineArr.length - 1; i < cnt; i++) {
        let lineAngle = L.GeometryUtil.bearing(L.latLng(lineArr[i]), L.latLng(lineArr[i+1])) - 90;
        let lineLength = L.GeometryUtil.distance(map, L.latLng(lineArr[i+1]), L.latLng(lineArr[i]));
        if (lineLength > 0) {
            let lineIcon = L.divIcon({
                border: 0,
                iconSize: [0, 0],
                iconAnchor: [lineLength, meterAnchor*meter],
                html: f(lineLength, meter)
            });
            L.marker(lineArr[i+1], {
                rotationAngle: lineAngle,
                icon: lineIcon
            }).addTo(linearMarkGroup);
        }
    }
}
function reverseLineIcon(lineArr, meter, angle, f) {
    let lineAngle = L.GeometryUtil.bearing(L.latLng(lineArr[0]), L.latLng(lineArr[lineArr.length - 1])) - angle;
    let lineLength = L.GeometryUtil.distance(map, L.latLng(lineArr[lineArr.length - 1]), L.latLng(lineArr[0]));
    if (lineLength > 0) {
        let lineIcon = L.divIcon({
            iconSize: [0, 0],
            iconAnchor: [0, 0],
            html: f(lineLength, meter)
        });
        L.marker(lineArr[lineArr.length - 1], {
            rotationAngle: lineAngle,
            icon: lineIcon
        }).addTo(linearMarkGroup);
    }
}
function fullLineIcon(lineArr, meter, angle, anchor, f) {
    let lineAngle = L.GeometryUtil.bearing(L.latLng(lineArr[0]), L.latLng(lineArr[lineArr.length - 1])) - angle;
    let lineLength = L.GeometryUtil.distance(map, L.latLng(lineArr[lineArr.length - 1]), L.latLng(lineArr[0]));
    if (lineLength > 0) {
        let lineIcon = L.divIcon({
            iconSize: [0, 0],
            iconAnchor: [0, anchor],
            html: f(lineLength, meter)
        });
        L.marker(lineArr[0], {
            rotationAngle: lineAngle,
            icon: lineIcon
        }).addTo(linearMarkGroup);
    }
}