function downloadSigns(reqData, map, signsGroup, projectPath, zoomCoef, coefficient, signText, ololoGroup) {
    let requestTime = new Date().getTime();
    (async() => {
        try {

            let response = await fetch(domain + "signs_go_box_post_geo" + token, {
                method: "POST",
                body: JSON.stringify(reqData)
            });
            let data = await response.json();
            console.log("Signs request has taken " + (new Date().getTime() - requestTime) +" ms");
            for (let k in data) {
                if(!map.hasLayer(ololoGroup[k])) {
                    ololoGroup[k] = L.layerGroup();
                    let rack = data[k].geoJSON.geometry.coordinates;
                    let rackProp = data[k].geoJSON.properties;
                    let rackType = data[k].geoJSON.properties.installationType;
                    let signsArr = data[k].signs;
                    let signsCnt = signsArr.length;
                    let signCoef = coefficient * 10;
                    let bilatH = zoomCoef * 4 + "px";
                    if (rackType === "Растяжка") {
                        console.log("Растяжечка", data);
                    } else {
                        new L.Circle([rack[0][1], rack[0][0]], 0.1, {
                            color: "#000",
                            fillColor: "#000",
                            fillOpacity: 1
                        }).addTo(signsGroup);

                        L.geoJSON(data[k].geoJSON, {
                            style: {
                                color: "#000",
                                weight: 0.4,
                                opacity: 1
                            }
                        }).addTo(signsGroup);
                        if (rackType === "Консоль") {
                            drawConsole(map, signsGroup, signCoef, signText, projectPath, zoomCoef, bilatH, rack, rackType, signsArr, signsCnt, k, rackProp);
                        } else {
                            drawRack(map, signsGroup, signCoef, signText, projectPath, zoomCoef, bilatH, rack, rackType, signsArr, signsCnt, k, rackProp);
                        }
                    }
                    ololoGroup[k].addTo(map);
                }
            }
        }catch(e) {
            console.log("catch", e);
        }
    })();
}
function drawRackPillar(k, coef, projectPath, signsArr, signsCnt, zoomCoef) {
    // cnvH, h -   высота изображения со знаком
    // cnvW, w -   максимальное значение ширины знака = ширина полотна canvas
    // cnvY, y -   точка начала рисования следующего знака относительно предыдущего,
    //             последнее значение = высота полотна canvas
    let cnvH = 0, cnvW = 0, cnvY = 0;
    function canvasSize(n, signsArr) {
        for (let s = 0; s < signsCnt; s++) {
            let x = 0;
            for (let key in signSizes) {
                if (signSizes[key].sNumbers.indexOf(signsArr[s].properties.signNumber) !== -1) {
                    if (signsArr[s].properties.signType === "Двухсторонний") {
                        cnvW = Math.max(cnvW, n * signSizes[key].sWidth) + (n / 10);
                        cnvH = n * signSizes[key].sHeight + (n / 10);
                        cnvY += cnvH;
                        x = 1;
                    } else {
                        cnvW = Math.max(cnvW, n * signSizes[key].sWidth);
                        cnvH = n * signSizes[key].sHeight;
                        cnvY += cnvH;
                        x = 1;
                    }
                }
            }
            if (x === 0) {
                // console.log("Знак № " + signsArr[s].sign_number + " не найден!");
                if (signsArr[s].properties.signType === "Двухсторонний") {
                    cnvW = Math.max(cnvW, n) + (n / 10);
                    cnvH = n + (n / 10);
                    cnvY += cnvH;
                } else {
                    cnvW = Math.max(cnvW, n);
                    cnvH = n;
                    cnvY += cnvH;
                }
            }
        }
    }
    let s = 0, h = 0, y = 0, w = coef, imgWay = projectPath + "images/icons_nii/signs/",
        imgExp = ".svg";
    let canvas = document.getElementById(k);
    canvasSize(coef, signsArr);
    canvas.height = coef / 5 + cnvY;
    canvas.width = cnvW;
    function drawSign() {
        let obj = signsArr[s++];
        y += h;
        let x = 0;
        for (let key in signSizes) {
            if (obj && (signSizes[key].sNumbers.indexOf(obj.properties.signNumber) !== -1)) {
                if (obj.properties.interactive !== "0") {
                    if (obj.properties.signType === "Двухсторонний") {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        ctx.beginPath();
                        ctx.moveTo((canvas.width - w) / 2 - zoomCoef, y);
                        ctx.lineTo((canvas.width - w) / 2 + w - zoomCoef, y);
                        ctx.lineTo((canvas.width - w) / 2 + w - zoomCoef, y + h);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef, y + h);
                        ctx.shadowBlur = 1;
                        ctx.shadowColor = "#000000";
                        ctx.shadowOffsetX = coef / 10;
                        ctx.shadowOffsetY = coef / 10;
                        ctx.fillStyle = "rgb(255,255,20)";
                        ctx.fill();
                        ctx.closePath();
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, ((canvas.width - (coef / 10) - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                            ctx.closePath();
                            h = h + (coef / 10);
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    } else {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        ctx.beginPath();
                        ctx.moveTo((canvas.width - w) / 2 - zoomCoef, y);
                        ctx.lineTo((canvas.width - w) / 2 + w, y);
                        ctx.lineTo((canvas.width - w) / 2 + w, y + h);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef, y + h);
                        ctx.shadowBlur = 0;
                        ctx.shadowColor = "transparent";
                        ctx.shadowOffsetX = 0;
                        ctx.shadowOffsetY = 0;
                        ctx.fillStyle = "rgb(255,255,20)";
                        ctx.fill();
                        ctx.closePath();
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, ((canvas.width - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                            ctx.closePath();
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    }
                } else {
                    if (obj.properties.signType === "Двухсторонний") {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 1;
                            ctx.shadowColor = "#000000";
                            ctx.shadowOffsetX = coef / 10;
                            ctx.shadowOffsetY = coef / 10;
                            ctx.drawImage(this, ((canvas.width - coef / 10) - w) / 2, y, w, h);
                            ctx.closePath();
                            h = h + (coef / 10);
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    } else {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, (canvas.width - w) / 2, y, w, h);
                            ctx.closePath();
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    }
                }
            }
        }
        if (obj && (x === 0)) {
            if (obj.properties.interactive === "1") {
                h = coef;
                w = coef;
                ctx.beginPath();
                ctx.moveTo(0, y);
                ctx.lineTo(canvas.width, y);
                ctx.lineTo(canvas.width, y + h);
                ctx.lineTo(0, y + h);
                ctx.shadowBlur = 0;
                ctx.shadowColor = "transparent";
                ctx.shadowOffsetX = 0;
                ctx.shadowOffsetY = 0;
                ctx.fillStyle = "rgb(255,127,20)";
                ctx.fill();
                ctx.closePath();
                let image = new Image();
                image.onload = function () {
                    ctx.drawImage(this, ((canvas.width - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                    drawSign();
                };
                image.src = projectPath + "images/icons_nii/signs/404.png";
            } else if (obj.properties.interactive === "0") {
                h = coef;
                w = coef;
                let image = new Image();
                image.onload = function () {
                    ctx.drawImage(this, (canvas.width - w) / 2, y, w, h);
                    drawSign();
                };
                image.src = projectPath + "images/icons_nii/signs/404.png";
            }
        }
    }
    let ctx = canvas.getContext("2d");
    ctx.beginPath();
    drawSign();
    ctx.closePath();
    ctx.beginPath();
    ctx.shadowBlur = 0;
    ctx.shadowColor = "transparent";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.moveTo(canvas.width / 4, canvas.height);
    ctx.lineTo((canvas.width / 4) * 3, canvas.height);
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, canvas.height - coef / 5);
    ctx.stroke();
    ctx.closePath();
}
function drawRack(map, signsGroup, signCoef, signText, projectPath, zoomCoef, bilatH, rack, rackType, signsArr, signsCnt, k, rackProp) {
    let rackAngle = L.GeometryUtil.bearing(L.latLng(rack[1][1], rack[1][0]), L.latLng(rack[2][1], rack[2][0]));
    let pillarAngle = rackAngle - 90;
    let numBack = "", numStraight = "", pHeight;
    for (let i = 0; i < signsCnt; i++) {
        let x = 0;
        for (let key in signSizes) {
            if (signSizes[key].sNumbers.indexOf(signsArr[i].properties.signNumber) !== -1) {
                x = 1;
                pHeight = signCoef * signSizes[key].sHeight;
                if (signsArr[i].properties.signType === "Прямой") {
                    numStraight += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px;\"></div>";
                } else if (signsArr[i].properties.signType === "Обратный") {
                    numStraight += "<div style=\"height: " + pHeight + "px;\"></div>";
                    numBack += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.1")) {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.2" +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.1" +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.2")) {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.1" +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.2" +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                } else if (signsArr[i].properties.signType === "Двухсторонний") {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + signsArr[i].properties.signNumber +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + signsArr[i].properties.signNumber +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                }
            }
        }
        if (x === 0) {
            pHeight = signCoef;
            if (signsArr[i].properties.signType === "Прямой") {
                numStraight += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                numBack += "<div style=\"height: " + pHeight + "px;\"></div>";
            } else if (signsArr[i].properties.signType === "Обратный") {
                numStraight += "<div style=\"height: " + pHeight + "px;\"></div>";
                numBack += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
            } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.1")) {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.2" +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.1" +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.2")) {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.1" +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.2" +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            } else if (signsArr[i].properties.signType === "Двухсторонний") {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + signsArr[i].properties.signNumber +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + signsArr[i].properties.signNumber +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            }
        }
    }
    let pointX = rack[1][1] - (rack[1][1] - rack[2][1]) / 2;
    let pointY = rack[1][0] - (rack[1][0] - rack[2][0]) / 2;
    let mPosX = (-map.latLngToLayerPoint(rack[1]).x + map.latLngToLayerPoint([pointY, pointX]).x);
    let mPosY = (-map.latLngToLayerPoint(rack[1]).y + map.latLngToLayerPoint([pointY, pointX]).y);
    let signIcon = L.divIcon({
        className: "iconClass",
        iconAnchor: [0, 0],
        popupAnchor: [0, 0],
        // popupAnchor: [mPosY, mPosX], // x - положительный сдвиг влево, y - положительный сдвиг вверх
        html: "<div class=\"signsIcon\" id=\"div" + k + "\" style=\"transform: rotate(" + pillarAngle + "deg);\">" +
        "<div class=\"numBack\">" + numBack + "</div>" +
        "<canvas id=\"" + k + "\"></canvas>" +
        "<div class=\"numStraight\">" + numStraight + "</div>" +
        "</div>"
    });
    let pBox = document.createElement("div");
    createPopup(signsArr, pBox, rackProp);

    let signPopup = L.popup({
        className: "mgtniip_popup",
        closeButton: true,
        autoClose: false
    }).setContent(
        pBox.outerHTML
    );
    let m = L.marker([pointX, pointY], {
        icon: signIcon
    }).bindPopup(signPopup).addTo(signsGroup);
    m.on("popupopen", function(popup) {
        let startWidth = popup.popup._container.offsetWidth;
        let startHeight = popup.popup._container.offsetHeight;
        console.log(startHeight);
        popup.popup._container.children[1].style.left = startWidth/2 - popup.popup._container.children[1].offsetWidth/2 + "px";
        let line = popup.popup._container.children[0].children[0].children[0].children;
        for (let l = 0, cnt = line.length; l < cnt; l++) {
            line[l].addEventListener("click", function() {
                line[l].children[0].classList.toggle("open");
                line[l].style.background = "white";
                if(line[l].children[0].className === "info_pic open") {
                    line[l].children[1].style.display = "none";
                    line[l].children[2].style.display = "table";
                    line[l].children[2].style.background = "white";
                    line[l].parentElement.lastChild.style.position = "absolute";
                    if ((startHeight - 70) > line[l].children[2].offsetHeight) {
                    	line[l].parentElement.style.height = startHeight - 40 + "px";
                        line[l].children[2].children[0].lastChild.lastChild.style.height = (startHeight - 70) - (line[l].children[2].offsetHeight - 19) + "px";
                    } else {
                        line[l].parentElement.style.height = line[l].children[2].offsetHeight + 30 + "px";
                    }
                    line[l].parentElement.style.width = (line[l].children[2].offsetWidth + 60) + "px";
                    line[l].children[2].style.left = "60px";
                    line[l].style.borderRadius = "4px 0 0 4px";
                    line[l].style.minWidth = "80px";
                    line[l].style.alignItems = "flex-start";

                    for (let i = 0; i < line[l].parentElement.children.length - 1; i++) {
                        if (line[l].parentElement.children[i] !== line[l]) {
                            line[l].parentElement.children[i].children[0].className = "info_pic";
                            line[l].parentElement.children[i].children[0].style = "";
                            line[l].parentElement.children[i].style = "";
                            line[l].parentElement.children[i].children[1].style.display = "table";
                            line[l].parentElement.children[i].children[2].style.display = "none";
                            popup.popup._container.style.width = startWidth;
                        }
                    }
                } else if (line[l].children[0].className === "info_pic"){
                    line[l].style = "";
                    line[l].children[0].style = "";
                    line[l].children[1].style.display = "table";
                    line[l].children[2].style.display = "none";
                    line[l].parentElement.style = "";
                    line[l].parentElement.lastChild.style = "";

                }

            });
        }
    });
    drawRackPillar(k, signCoef, projectPath, signsArr, signsCnt, zoomCoef);
    let divIc = document.getElementById("div" + k);
    let dh = divIc.offsetHeight;
    let dw = divIc.offsetWidth;
    divIc.style.bottom = dh + "px";
    divIc.style.right = dw / 2 + "px";
    let e = document.getElementsByClassName("signsIcon");
    let ee = document.getElementsByClassName("numBilat");
    for (i = 0; i < e.length; i++) {
        e[i].style.fontSize = signText;
    }
    for (i = 0; i < ee.length; i++) {
        ee[i].style.lineHeight = bilatH;
    }
}
function drawConsolePillar(k, coef, projectPath, signsArr, signsCnt, zoomCoef) {
    // cnvH, h -   высота изображения со знаком
    // cnvW, w -   максимальное значение ширины знака = ширина полотна canvas
    // cnvY, y -   точка начала рисования следующего знака относительно предыдущего,
    //             последнее значение = высота полотна canvas
    let cnvH = 0, cnvW = 0, cnvY = 0;
    function canvasSize(n, signsArr) {
        for (let s = 0; s < signsCnt; s++) {
            let x = 0;
            for (let key in signSizes) {
                if (signSizes[key].sNumbers.indexOf(signsArr[s].properties.signNumber) !== -1) {
                    if (signsArr[s].properties.signType === "Двухсторонний") {
                        cnvW = Math.max(cnvW, n * signSizes[key].sWidth) + (n / 10);
                        cnvH = n * signSizes[key].sHeight + (n / 10);
                        cnvY += cnvH;
                        x = 1;
                    } else {
                        cnvW = Math.max(cnvW, n * signSizes[key].sWidth);
                        cnvH = n * signSizes[key].sHeight;
                        cnvY += cnvH;
                        x = 1;
                    }
                }
            }
            if (x === 0) {
                // console.log("Знак № " + signsArr[s].sign_number + " не найден!");
                if (signsArr[s].properties.signType === "Двухсторонний") {
                    cnvW = Math.max(cnvW, n) + (n / 10);
                    cnvH = n + (n / 10);
                    cnvY += cnvH;
                } else {
                    cnvW = Math.max(cnvW, n);
                    cnvH = n;
                    cnvY += cnvH;
                }
            }
        }
    }
    let s = 0, h = 0, y = 0, w = coef, imgWay = projectPath + "images/icons_nii/signs/",
        imgExp = ".svg";
    let canvas = document.getElementById(k);
    canvasSize(coef, signsArr);
    canvas.height = cnvY;
    canvas.width = cnvW;
    function drawSign() {
        let obj = signsArr[s++];
        y += h;
        let x = 0;
        for (let key in signSizes) {
            if (obj && (signSizes[key].sNumbers.indexOf(obj.properties.signNumber) !== -1)) {
                if (obj.properties.interactive !== "0") {
                    if (obj.properties.signType === "Двухсторонний") {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        ctx.beginPath();
                        ctx.moveTo((canvas.width - w) / 2 - zoomCoef, y);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef + w, y);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef + w, y + h);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef, y + h);
                        ctx.shadowBlur = 1;
                        ctx.shadowColor = "#000000";
                        ctx.shadowOffsetX = coef / 10;
                        ctx.shadowOffsetY = coef / 10;
                        ctx.fillStyle = "rgb(255,255,20)";
                        ctx.fill();
                        ctx.closePath();
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, ((canvas.width - (coef / 10) - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                            ctx.closePath();
                            h = h + (coef / 10);
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    } else {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        ctx.beginPath();
                        ctx.moveTo((canvas.width - w) / 2 - zoomCoef, y);
                        ctx.lineTo((canvas.width - w) / 2 + w, y);
                        ctx.lineTo((canvas.width - w) / 2 + w, y + h);
                        ctx.lineTo((canvas.width - w) / 2 - zoomCoef, y + h);
                        ctx.shadowBlur = 0;
                        ctx.shadowColor = "transparent";
                        ctx.shadowOffsetX = 0;
                        ctx.shadowOffsetY = 0;
                        ctx.fillStyle = "rgb(255,255,20)";
                        ctx.fill();
                        ctx.closePath();
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, ((canvas.width - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                            ctx.closePath();
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    }
                } else {
                    if (obj.properties.signType === "Двухсторонний") {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 1;
                            ctx.shadowColor = "#000000";
                            ctx.shadowOffsetX = coef / 10;
                            ctx.shadowOffsetY = coef / 10;
                            ctx.drawImage(this, ((canvas.width - coef / 10) - w) / 2, y, w, h);
                            ctx.closePath();
                            h = h + (coef / 10);
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    } else {
                        w = coef * signSizes[key].sWidth;
                        h = coef * signSizes[key].sHeight;
                        let image = new Image();
                        image.onload = function () {
                            ctx.beginPath();
                            ctx.shadowBlur = 0;
                            ctx.shadowColor = "transparent";
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            ctx.drawImage(this, (canvas.width - w) / 2, y, w, h);
                            ctx.closePath();
                            drawSign();
                        };
                        image.src = imgWay + obj.properties.signNumber + imgExp;
                        x = 1;
                    }
                }
            }
        }
        if (obj && (x === 0)) {
            if (obj.properties.interactive === "1") {
                h = coef;
                w = coef;
                ctx.beginPath();
                ctx.moveTo(0, y);
                ctx.lineTo(canvas.width, y);
                ctx.lineTo(canvas.width, y + h);
                ctx.lineTo(0, y + h);
                ctx.shadowBlur = 0;
                ctx.shadowColor = "transparent";
                ctx.shadowOffsetX = 0;
                ctx.shadowOffsetY = 0;
                ctx.fillStyle = "rgb(255,127,20)";
                ctx.fill();
                ctx.closePath();
                let image = new Image();
                image.onload = function () {
                    ctx.drawImage(this, ((canvas.width - w) / 2) + zoomCoef, y + zoomCoef, w - (2 * zoomCoef), h - (2 * zoomCoef));
                    drawSign();
                };
                image.src = projectPath + "images/icons_nii/signs/404.png";
            } else if (obj.properties.interactive === "0") {
                h = coef;
                w = coef;
                let image = new Image();
                image.onload = function () {
                    ctx.drawImage(this, (canvas.width - w) / 2, y, w, h);
                    drawSign();
                };
                image.src = projectPath + "images/icons_nii/signs/404.png";
            }
        }
    }
    let ctx = canvas.getContext("2d");
    ctx.beginPath();
    drawSign();
    ctx.closePath();
}
function drawConsole(map, signsGroup, signCoef, signText, projectPath, zoomCoef, bilatH, rack, rackType, signsArr, signsCnt, k, rackProp) {
    let rackAngle = L.GeometryUtil.bearing(L.latLng(rack[2][1], rack[2][0]), L.latLng(rack[3][1], rack[3][0]));
    let numBack = "", numStraight = "", pHeight;
    for (let i = 0; i < signsCnt; i++) {
        let x = 0;
        for (let key in signSizes) {
            if (signSizes[key].sNumbers.indexOf(signsArr[i].properties.signNumber) !== -1) {
                x = 1;
                pHeight = signCoef * signSizes[key].sHeight;
                if (signsArr[i].properties.signType === "Прямой") {
                    numStraight += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px;\"></div>";
                } else if (signsArr[i].properties.signType === "Обратный") {
                    numStraight += "<div style=\"height: " + pHeight + "px;\"></div>";
                    numBack += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.1")) {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.2" +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.1" +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.2")) {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.1" +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + "5.19.2" +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                } else if (signsArr[i].properties.signType === "Двухсторонний") {
                    numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                        + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + signsArr[i].properties.signNumber +
                        "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                        + signsArr[i].properties.signNumber +
                        "</p>" +
                        "</div>";
                    numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
                }
            }
        }
        if (x === 0) {
            pHeight = signCoef;
            if (signsArr[i].properties.signType === "Прямой") {
                numStraight += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
                numBack += "<div style=\"height: " + pHeight + "px;\"></div>";
            } else if (signsArr[i].properties.signType === "Обратный") {
                numStraight += "<div style=\"height: " + pHeight + "px;\"></div>";
                numBack += "<div style=\"height: " + pHeight + "px; line-height: " + pHeight + "px\">" + signsArr[i].properties.signNumber + "</div>";
            } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.1")) {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.2" +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.1" +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            } else if ((signsArr[i].properties.signType === "Двухсторонний") && (signsArr[i].properties.signNumber === "5.19.2")) {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.1" +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + "5.19.2" +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            } else if (signsArr[i].properties.signType === "Двухсторонний") {
                numStraight += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\">"
                    + "<p class=\"numBilatReverse\" style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + signsArr[i].properties.signNumber +
                    "</p><p style=\"height: " + (pHeight / 2) + "px; line-height: " + (pHeight / 2) + "px\">"
                    + signsArr[i].properties.signNumber +
                    "</p>" +
                    "</div>";
                numBack += "<div style=\"height: " + pHeight + "px; margin-bottom: " + (signCoef / 10) + "px;\"></div>";
            }
        }
    }
    let pointX = rack[2][1] - (rack[2][1] - rack[3][1]);
    let pointY = rack[2][0] - (rack[2][0] - rack[3][0]);
    let mPosX = (-map.latLngToLayerPoint(rack[1]).x + map.latLngToLayerPoint([pointY, pointX]).x);
    let mPosY = (-map.latLngToLayerPoint(rack[1]).y + map.latLngToLayerPoint([pointY, pointX]).y);
    let signIcon = L.divIcon({
        className: "iconClass",
        iconAnchor: [0, 0],
        popupAnchor: [0, 0],
        // popupAnchor: [mPosY, mPosX], // x - положительный сдвиг влево, y - положительный сдвиг вверх
        html: "<div class=\"signsIcon consoleIcon\" id=\"div" + k + "\" >" +
        "<div class=\"numBack\">" + numBack + "</div>" +
        "<canvas id=\"" + k + "\"></canvas>" +
        "<div class=\"numStraight\">" + numStraight + "</div>" +
        "</div>"
    });
    let pBox = document.createElement("div");
    createPopup(signsArr, pBox, rackProp);

    let signPopup = L.popup({
        className: "mgtniip_popup " + k,
        closeButton: true,
        autoClose: false
    }).setContent(
        pBox.outerHTML
    );
    let m = L.marker([pointX, pointY], {
        icon: signIcon,
        rotationAngle: rackAngle + 180
    }).bindPopup(signPopup).addTo(signsGroup);
    m.on("popupopen", function(popup) {
        // map.getPane("dis").style.display = "block";
        console.log(this);
        let startWidth = popup.popup._container.offsetWidth;
        let startHeight = popup.popup._container.offsetHeight;
        popup.popup._container.children[1].style.left = startWidth/2 - popup.popup._container.children[1].offsetWidth/2 + "px";
        // let line = document.getElementsByClassName("info_line");
        let line = popup.popup._container.children[0].children[0].children[0].children;
        for (let l = 0, cnt = line.length; l < (cnt-1); l++) {
            line[l].addEventListener("click", function () {
                line[l].children[0].classList.toggle("open");
                line[l].style.background = "white";
                if (line[l].children[0].className === "info_pic open") {
                    line[l].children[1].style.display = "none";
                    line[l].children[2].style.display = "table";
                    line[l].children[2].style.background = "white";
                    line[l].parentElement.lastChild.style.position = "absolute";
                    if ((startHeight - 70) > line[l].children[2].offsetHeight) {
                        line[l].parentElement.style.height = startHeight - 40 + "px";
                        line[l].children[2].children[0].lastChild.lastChild.style.height = (startHeight - 70) - (line[l].children[2].offsetHeight - 19) + "px";
                    } else {
                        line[l].parentElement.style.height = line[l].children[2].offsetHeight + 30 + "px";
                    }
                    line[l].parentElement.style.width = (line[l].children[2].offsetWidth + 60) + "px";
                    line[l].children[2].style.left = "60px";
                    line[l].style.borderRadius = "4px 0 0 4px";
                    line[l].style.minWidth = "80px";
                    line[l].style.alignItems = "flex-start";
                    for (let i = 0; i < line[l].parentElement.children.length - 1; i++) {
                        if (line[l].parentElement.children[i] !== line[l]) {
                            line[l].parentElement.children[i].children[0].className = "info_pic";
                            line[l].parentElement.children[i].children[0].style = "";
                            line[l].parentElement.children[i].style = "";
                            line[l].parentElement.children[i].children[1].style.display = "table";
                            line[l].parentElement.children[i].children[2].style.display = "none";
                            popup.popup._container.style.width = startWidth;
                        }
                    }
                } else if (line[l].children[0].className === "info_pic") {
                    line[l].style = "";
                    line[l].children[0].style = "";
                    line[l].children[1].style.display = "table";
                    line[l].children[2].style.display = "none";
                    line[l].parentElement.style = "";
                    line[l].parentElement.lastChild.style = "";
                }
            });
        }
    });
    // m.on("popupclose", function() {
    //     map.getPane("dis").style.display = "none";
    // });
    drawConsolePillar(k, signCoef, projectPath, signsArr, signsCnt, zoomCoef);
    let divIc = document.getElementById("div" + k);
    let dw = divIc.offsetWidth;
    divIc.style.right = dw / 2 + "px";
    let e = document.getElementsByClassName("signsIcon");
    let ee = document.getElementsByClassName("numBilat");
    for (i = 0; i < e.length; i++) {
        e[i].style.fontSize = signText;
    }
    for (i = 0; i < ee.length; i++) {
        ee[i].style.lineHeight = bilatH;
    }
}
function createPopup(signsArr, pBox, rackProp) {
    for (let i in signsArr) {
        let interactive = "", defects = "", angle = "", textColor = "", boxColor = "";
        (Math.floor((rackProp.angle * 180) / Math.PI) >= 0 ? (angle = Math.floor((rackProp.angle * 180) / Math.PI)) : (angle = 360 + Math.floor((rackProp.angle * 180) / Math.PI)));
        let sNumber = signsArr[i].properties.signNumber;
        let sType = ["Прямой", "Обратный"];
        (signsArr[i].properties.interactive === "1" ? interactive = "Да" : interactive = "Нет");
        (signsArr[i].properties.defects === "1" ? defects = "Да" : defects = "Нет");
        if (signsArr[i].properties.status === "Демонтируемый") {
            textColor = "#e52329";
            boxColor = "transparent"
        } else if (signsArr[i].properties.status === "Проектируемый") {
            textColor = "#eaad08";
            boxColor = "transparent"
        } else if (signsArr[i].properties.status.slice(0, 10) === "Временный") {
            textColor = "#75b62e";
            boxColor = "#eaad08";
        } else {
            textColor = "#75b62e";
            boxColor = "transparent";
        }
        if ((signsArr[i].properties.signType === "Двухсторонний") && (sNumber === "5.19.1")) {
            for (let j = 0; j < 2; j++) {
                let pLine = document.createElement("div");
                pLine.className = "info_line";
                let pPic = document.createElement("div");
                pPic.className = "info_pic";
                let pImg = document.createElement("img");
                if (signsArr[i].properties.interactive === "1") {
                    interactive = "Да";
                    pImg.style.background = "#f7e700";
                }
                pImg.src = projectPath + "images/icons_nii/signs/" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) + j) + ".svg";
                let tableMin = document.createElement("table");
                tableMin.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) + j) + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:"+boxColor+";color:"+textColor+";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "</tbody>";
                let tableFull = document.createElement("table");
                tableFull.className = "popup_full_table";
                tableFull.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) + j) + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:"+boxColor+";color:"+textColor+";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Тип знака:</th>" +
                    "<td>" + sType[j] + "</td>" +
                    "</tr>" +
                    "<th>Высота установки:</th>" +
                    "<td>" + signsArr[i].properties.height + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Видимость:</th>" +
                    "<td>" + signsArr[i].properties.visibility + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Жёлтая окантовка:</th>" +
                    "<td>" + interactive + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Дефекты:</th>" +
                    "<td>" + defects + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Угол поворота:</th>" +
                    "<td>" + angle + "°</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Параметры:</th>" +
                    "<td></td>" +
                    "</tr>" +
                    "</tbody>";
                pPic.appendChild(pImg);
                pLine.appendChild(pPic);
                pLine.appendChild(tableMin);
                pLine.appendChild(tableFull);
                pBox.appendChild(pLine);
            }
        } else if ((signsArr[i].properties.signType === "Двухсторонний") && (sNumber === "5.19.2")) {
            for (let j = 0; j < 2; j++) {
                let pLine = document.createElement("div");
                pLine.className = "info_line";
                let pPic = document.createElement("div");
                pPic.className = "info_pic";
                let pImg = document.createElement("img");
                if (signsArr[i].properties.interactive === "1") {
                    interactive = "Да";
                    pImg.style.background = "#f7e700";
                }
                pImg.src = projectPath + "images/icons_nii/signs/" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) - j) + ".svg";
                let tableMin = document.createElement("table");
                tableMin.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) - j) + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:" + boxColor + ";color:" + textColor + ";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "</tbody>";
                let tableFull = document.createElement("table");
                tableFull.className = "popup_full_table";
                tableFull.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber.slice(0, sNumber.length - 1) + (parseInt(sNumber.slice(sNumber.length - 1)) - j) + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:" + boxColor + ";color:" + textColor + ";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Тип знака:</th>" +
                    "<td>" + sType[j] + "</td>" +
                    "</tr>" +
                    "<th>Высота установки:</th>" +
                    "<td>" + signsArr[i].properties.height + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Видимость:</th>" +
                    "<td>" + signsArr[i].properties.visibility + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Жёлтая окантовка:</th>" +
                    "<td>" + interactive + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Дефекты:</th>" +
                    "<td>" + defects + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Угол поворота:</th>" +
                    "<td>" + angle + "°</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Параметры:</th>" +
                    "<td></td>" +
                    "</tr>" +
                    "</tbody>";
                pPic.appendChild(pImg);
                pLine.appendChild(pPic);
                pLine.appendChild(tableMin);
                pLine.appendChild(tableFull);
                pBox.appendChild(pLine);
            }
        } else if (signsArr[i].properties.signType === "Двухсторонний") {
            for (let j = 0; j < 2; j++) {
                let pLine = document.createElement("div");
                pLine.className = "info_line";
                let pPic = document.createElement("div");
                pPic.className = "info_pic";
                let pImg = document.createElement("img");
                if (signsArr[i].properties.interactive === "1") {
                    interactive = "Да";
                    pImg.style.background = "#f7e700";
                }
                pImg.src = projectPath + "images/icons_nii/signs/" + sNumber + ".svg";
                let tableMin = document.createElement("table");
                tableMin.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:" + boxColor + ";color:" + textColor + ";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "</tbody>";
                let tableFull = document.createElement("table");
                tableFull.className = "popup_full_table";
                tableFull.innerHTML =
                    "<tbody>" +
                    "<tr>" +
                    "<th>№:</th>" +
                    "<td>" + sNumber + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Статус:</th>" +
                    "<td style=\"background:" + boxColor + ";color:" + textColor + ";\">" + signsArr[i].properties.status + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Тип знака:</th>" +
                    "<td>" + sType[j] + "</td>" +
                    "</tr>" +
                    "<th>Высота установки:</th>" +
                    "<td>" + signsArr[i].properties.height + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Видимость:</th>" +
                    "<td>" + signsArr[i].properties.visibility + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Жёлтая окантовка:</th>" +
                    "<td>" + interactive + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Дефекты:</th>" +
                    "<td>" + defects + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Угол поворота:</th>" +
                    "<td>" + angle + "°</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Параметры:</th>" +
                    "<td></td>" +
                    "</tr>" +
                    "</tbody>";
                pPic.appendChild(pImg);
                pLine.appendChild(pPic);
                pLine.appendChild(tableMin);
                pLine.appendChild(tableFull);
                pBox.appendChild(pLine);
            }
        } else {
            let pLine = document.createElement("div");
            pLine.className = "info_line";
            let pPic = document.createElement("div");
            pPic.className = "info_pic";
            let pImg = document.createElement("img");
            if (signsArr[i].properties.interactive === "1") {
                pImg.style.background = "#ffee00";
            }
            pImg.src = projectPath + "images/icons_nii/signs/" + signsArr[i].properties.signNumber + ".svg";
            let tableMin = document.createElement("table");
            tableMin.innerHTML =
                "<tbody>" +
                "<tr>" +
                "<th>№:</th>" +
                "<td>" + signsArr[i].properties.signNumber + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Статус:</th>" +
                "<td style=\"background:"+boxColor+";color:"+textColor+";\">" + signsArr[i].properties.status + "</td>" +
                "</tr>" +
                "</tbody>";
            let tableFull = document.createElement("table");
            tableFull.className = "popup_full_table";
            tableFull.innerHTML =
                "<tbody>" +
                "<tr>" +
                "<th>№:</th>" +
                "<td>" + signsArr[i].properties.signNumber + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Статус:</th>" +
                "<td style=\"background:"+boxColor+";color:"+textColor+";\">" + signsArr[i].properties.status + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Тип знака:</th>" +
                "<td>" + signsArr[i].properties.signType + "</td>" +
                "</tr>" +
                "<th>Высота установки:</th>" +
                "<td>" + signsArr[i].properties.height + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Видимость:</th>" +
                "<td>" + signsArr[i].properties.visibility + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Жёлтая окантовка:</th>" +
                "<td>" + interactive + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Дефекты:</th>" +
                "<td>" + defects + "</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Угол поворота:</th>" +
                "<td>" + angle + "°</td>" +
                "</tr>" +
                "<tr>" +
                "<th>Параметры:</th>" +
                "<td></td>" +
                "</tr>" +
                "</tbody>";
            pPic.appendChild(pImg);
            pLine.appendChild(pPic);
            pLine.appendChild(tableMin);
            pLine.appendChild(tableFull);
            pBox.appendChild(pLine);
        }
    }
    let type = document.createElement("div");
    type.className = "info_type";
    type.innerHTML = "<span>Тип установки:</span>" + rackProp.installationType;
    pBox.appendChild(type);
}