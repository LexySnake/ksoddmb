function getSO(sO, segObj) {
    console.time("sO");
    for (let div in refObj) {
        sO[div] = {};
        sO[div]["obj"] = {};
        sO[div]["obj"] = refObj;
        sO[div]["type"] = "division";
        sO[div]["parent"] = null;
        for (let dist in refObj[div]) {
            sO[dist] = {};
            sO[dist]["obj"] = {};
            sO[dist]["obj"] = refObj[div];
            sO[dist]["type"] = "district";
            sO[dist]["parent"] = div;
            for (let st in refObj[div][dist]) {
                sO[st] = {};
                sO[st]["obj"] = {};
                sO[st]["obj"] = refObj[div][dist];
                sO[st]["type"] = "street";
                sO[st]["parent"] = dist;

                segObj.features.forEach(function (seg) {
                    let segParse = seg.properties.name.match(/[\dа-яёА-ЯЁ]+/gi);
                    for (let i = 0, cnt = segParse.length; i < cnt; i++) {
                        if (segParse[i] === refObj[div][dist][st]) {
                            let segm = seg.properties.name;
                            sO[segm] = {};
                            sO[segm]["obj"] = refObj[div][dist][st];
                            sO[segm]["type"] = "segment";
                            sO[segm]["parent"] = st;
                        }
                    }
                });
            }
        }
    }
    console.log(sO);
    console.timeEnd("sO");
    return sO;
}
sClose.addEventListener("click", function() {
    sRes.innerHTML = "";
    yaRes.innerHTML = "";
    searchGroup.clearLayers();
    hoverGroup.clearLayers();
});
document.body.addEventListener("keydown", function(e) {
    if (e.key === "Escape") {
        sClose.click();
        searchGroup.clearLayers();
        hoverGroup.clearLayers();
        sInp.blur();
    } else if (e.key === "Tab") {
        e.preventDefault();
        arrowD("result_row");
    } else if (e.key === "ArrowDown") {
        e.preventDefault();
        arrowD("result_row");
    } else if (e.key === "ArrowUp") {
        e.preventDefault();
        arrowU("result_row");
    } else {
        sInp.focus();
    }
});
function sView(finRes, bounds) {
    let obj = objMap[finRes.type];
    obj.obj.features.forEach(function(o) {
        if (o.properties.name === finRes.result.input) {
            let strokeLayer = L.layerGroup([
                L.geoJSON(o, {
                    onEachFeature: function (feature, polygon) {
                        polygon.setStyle({
                            weight: getOpacity(map.getZoom())[1] + obj.weight,
                            opacity: 1.0,
                            color: "#ffffff",
                            fillColor: obj.fill,
                            fillOpacity: getOpacity(map.getZoom())[0],
                            pane: obj.strokePane,
                            interactive: false
                        });
                        map.on("zoomend", function () {
                            polygon.setStyle({
                                weight: getOpacity(map.getZoom())[1] + obj.weight,
                                fillOpacity: getOpacity(map.getZoom())[0]
                            });
                        });
                        if (finRes.type === "segment") {
                            polygon.setStyle({
                                fillOpacity: getOpacity(map.getZoom())[2]
                            });
                            map.on("zoomend", function () {
                                polygon.setStyle({
                                    fillOpacity: getOpacity(map.getZoom())[2]
                                });
                            });
                        }
                    }
                })
            ]);
            let foundLayer = L.layerGroup([
                L.geoJSON(o, {
                    onEachFeature: function (feature, polygon) {
                        bounds.push(polygon.getLatLngs());
                        polygon.setStyle({
                            weight: getOpacity(map.getZoom())[1] + obj.weight - 2,
                            opacity: 1.0,
                            color: obj.color,
                            pane: obj.pane,
                            fillOpacity: 0.0,
                            interactive: false
                        });
                        map.on("zoomend", function () {
                            polygon.setStyle({
                                weight: getOpacity(map.getZoom())[1] + obj.weight - 2
                            });
                        });
                    }
                })
            ]);
            strokeLayer.addTo(searchGroup);
            foundLayer.addTo(searchGroup);
            searchGroup.addTo(map);
            sInp.value = finRes.result.input;
            sInp.blur();
            sBox.classList.add("hide_result");
        }
    });
}

//Отрисовка объектов по событию "hover" - очень сильно лагает при отрисовке улицы
function sHover(finRes) {
    let obj = objMap[finRes.type];
    obj.obj.features.forEach(function(o) {
        if (o.properties.name === finRes.result.input) {
            let foundLayer = L.layerGroup([
                L.geoJSON(o, {
                    onEachFeature: function (feature, polygon) {
                        polygon.setStyle({
                            opacity: 0.0,
                            color: obj.color,
                            fillOpacity: 0.3,
                            fillColor: obj.hoverFill,
                            pane: obj.allPane
                        });
                        if (finRes.type === "segment") {
                            polygon.setStyle({
                                weight: getOpacity(map.getZoom())[1] + obj.weight - 2,
                                opacity: 1.0,
                                color: obj.color,
                                fillOpacity: 1.0
                            });
                        }
                    }
                })
            ]);

            foundLayer.addTo(hoverGroup);
            hoverGroup.addTo(map);
        }
    });
}


function search(val) {
    console.time("search");
    if ((refObj !== {}) && (sO !== {})) {
        console.log(refObj);
        console.log(sO);
        if ((val.trim().length <= 1) && (sBox.classList.contains("hide_result") === false)) {
            sBox.classList.add("hide_result");
        } else if (val.trim().length > 1) {
            let regexResult;
            let translitValue = translit(val).trim();
            let value = "";
            for (let s = 0, cnt = translitValue.length; s < cnt; s++) {
                if ((translitValue[s] === "(") || (translitValue[s] === ")")) {
                    value += "\\" + translitValue[s];
                } else {
                    value += translitValue[s];
                }
            }
            let regex = new RegExp("(\w\d\s)*" + value + "(\w\d\s)*", "i");
            sRes.innerHTML = "";
            yaRes.innerHTML = "";
            yaRes.style.marginTop = "0";
            yaBtn.style.display = "flex";
            let startResult = {};
            let finishResult = [];
            let maxIndex = 0;

            function searchResult(key, type, obj, parent) {
                regexResult = regex.exec(key);
                if (regexResult) {
                    startResult[key] = {};
                    startResult[key]["result"] = regexResult;
                    startResult[key]["type"] = type;
                    if (parent) {
                        startResult[key]["parent"] = parent;
                    }
                    if (type === "street") {
                        startResult[key]["segments"] = [];
                        segObj.features.forEach(function (seg) {
                            let segParse = seg.properties.name.match(/[\dа-яёА-ЯЁ\(\)]+/gi);
                            if (segParse[0] === obj[key]) {
                                startResult[key]["segments"].push(seg.properties.name);
                            } else {
                                for (let i = 1, cnt = segParse.length; i < cnt; i++) {
                                    if (segParse[i][0] === "(") {
                                        break;
                                    } else if (segParse[i] === obj[key]) {
                                        startResult[key]["segments"].push(seg.properties.name);
                                    }
                                }
                            }
                        });
                    }
                }
            }
            console.time("foo");
            // for (let divK in refObj) {
            //     searchResult(divK, "division", refObj, null);
            //     for (let distK in refObj[divK]) {
            //         searchResult(distK, "district", refObj[divK], divK);
            //         for (let strK in refObj[divK][distK]) {
            //             searchResult(strK, "street", refObj[divK][distK], [distK]);
            //             segObj.features.forEach(function (seg) {
            //                 let segParse = seg.properties.name.match(/[\dа-яёА-ЯЁ]+/gi);
            //                 for (let i = 0, cnt = segParse.length; i < cnt; i++) {
            //                     if (segParse[i] === refObj[divK][distK][strK]) {
            //                         searchResult(seg.properties.name, "segment", refObj[divK][distK][strK], [strK]);
            //                     }
            //                 }
            //             });
            //         }
            //     }
            // }
            for (let k in sO) {
                searchResult(k, sO[k]["type"], sO[k]["obj"], sO[k]["parent"]);
            }
            console.timeEnd("foo");
            console.time("sort");
            for (let k in startResult) {
                for (let i = 0, cnt = Object.keys(startResult).length; i < cnt; i++) {
                    maxIndex = Math.max(maxIndex, startResult[k].result.index);
                }
            }
            let sortResult = Object.keys(startResult).sort();
            for (let i = 0; i <= maxIndex; i++) {
                for (let j = 0, cnt = sortResult.length; j < cnt; j++) {
                    if (startResult[sortResult[j]].result.index === i) {
                        finishResult.push(startResult[sortResult[j]]);
                    }
                }
            }
            console.timeEnd("sort");
            let index = 1;
            for (let i = 0, cntI = finishResult.length; i < cntI; i++) {
                let result = finishResult[i].result;
                let row = document.createElement("div");
                row.classList = "result_row search_div " + finishResult[i].type;
                row.setAttribute("tabindex", index);
                index++;
                row.innerHTML =
                    result.input.slice(0, result.index) +
                    "<b>" + result.input.slice(result.index, result.index + result[0].length) + "</b>" +
                    result.input.slice(result.index + result[0].length, result.input.length);
                if (finishResult[i].parent) {
                    let span = document.createElement("span");
                    span.innerHTML = " • " + finishResult[i].parent;
                    row.appendChild(span);
                }
                sRes.appendChild(row);
                if (!finishResult[i].segments) {
                    row.addEventListener("focusin", function () {
                        row.addEventListener("keydown", function (e) {
                            if (e.key === "Enter") {
                                searchGroup.clearLayers();
                                let bounds = [];
                                sView(finishResult[i], bounds);
                                map.fitBounds(bounds);
                            }
                        });
                    });
                    row.addEventListener("click", function () {
                        searchGroup.clearLayers();
                        let bounds = [];
                        sView(finishResult[i], bounds);
                        map.fitBounds(bounds);
                    });
                } else {
                    row.addEventListener("focusin", function () {
                        row.addEventListener("keydown", function (e) {
                            if (e.key === "Enter") {
                                searchGroup.clearLayers();
                                let bounds = [];
                                for (let j = 0, cntJ = finishResult[i].segments.length; j < cntJ; j++) {
                                    let segRes = {};
                                    segRes.parent = finishResult[i].result.input;
                                    segRes.result = [];
                                    segRes.result["input"] = finishResult[i].segments[j];
                                    segRes.type = "segment";
                                    sView(segRes, bounds);
                                }
                                sInp.value = finishResult[i].result.input;
                                let lastBounds = [];
                                for (let i = 0; i < bounds.length; i++) {
                                    for (let j = 0; j < bounds[i][0][0].length; j++) {
                                        lastBounds.push(bounds[i][0][0][j]);
                                    }
                                }
                                map.fitBounds(lastBounds);
                            }
                        });
                    });
                    row.addEventListener("click", function () {
                        searchGroup.clearLayers();
                        let bounds = [];
                        for (let j = 0, cntJ = finishResult[i].segments.length; j < cntJ; j++) {
                            let segRes = {};
                            segRes.parent = finishResult[i].result.input;
                            segRes.result = [];
                            segRes.result["input"] = finishResult[i].segments[j];
                            segRes.type = "segment";
                            sView(segRes, bounds);
                        }
                        sInp.value = finishResult[i].result.input;
                        let lastBounds = [];
                        for (let i = 0; i < bounds.length; i++) {
                            for (let j = 0; j < bounds[i][0][0].length; j++) {
                                lastBounds.push(bounds[i][0][0][j]);
                            }
                        }
                        map.fitBounds(lastBounds);
                    });
                }
                if ((finishResult.length === 1) && (finishResult[i].segments)) {
                    for (let j = 0, cntJ = finishResult[i].segments.length; j < cntJ; j++) {
                        let segRes = {};
                        segRes.parent = finishResult[i].result.input;
                        segRes.result = [];
                        segRes.result["input"] = finishResult[i].segments[j];
                        segRes.type = "segment";
                        let segRow = document.createElement("div");
                        segRow.classList = "result_row search_div segment";
                        segRow.setAttribute("tabindex", index);
                        segRow.style.marginLeft = "8px";
                        segRow.innerHTML = finishResult[i].segments[j];
                        sRes.appendChild(segRow);
                        segRow.addEventListener("focusin", function () {
                            segRow.addEventListener("keydown", function (e) {
                                if (e.key === "Enter") {
                                    searchGroup.clearLayers();
                                    let bounds = [];
                                    sView(segRes, bounds);
                                    map.fitBounds(bounds);
                                }
                            });
                        });
                        segRow.addEventListener("click", function () {
                            searchGroup.clearLayers();
                            let bounds = [];
                            sView(segRes, bounds);
                            map.fitBounds(bounds);
                        });
                        index++;
                    }
                }
            }
            if (sRes.hasChildNodes() === false) {
                sEmpty.style.display = "block";
            } else {
                sEmpty.style.display = "none";
            }
        }
        function yaShow() {
            let yaVal = sInp.value;
            yaRes.innerHTML = "";
            sInp.value = "";
            (async () => {
                try {
                    if (yaVal !== "") {
                        let response = await fetch("https://geocode-maps.yandex.ru/1.x/?&apikey=f1d03e63-82b9-4586-8be2-5723669ef7eb&bbox=36.888006,55.490751~37.967819,56.020919&rspn=1&format=json&geocode=" + yaVal);
                        let data = await response.json();
                        let index = document.getElementsByClassName("result_row").length;
                        if (data.response.GeoObjectCollection.featureMember.length !== 0) {
                            data.response.GeoObjectCollection.featureMember.forEach(function(d) {
                                function yaReverse(d) {
                                    d[0] = parseFloat(d[0]);
                                    d[1] = parseFloat(d[1]);
                                    d = d.reverse();
                                    return d;
                                }
                                index++;
                                let pos = d.GeoObject.Point.pos.split(" ");
                                yaReverse(pos);
                                let lower = d.GeoObject.boundedBy.Envelope.lowerCorner.split(" ");
                                yaReverse(lower);
                                let upper = d.GeoObject.boundedBy.Envelope.upperCorner.split(" ");
                                yaReverse(upper);
                                let row = document.createElement("div");
                                row.classList = "result_row ya_row search_div";
                                row.setAttribute("tabindex", index);
                                row.setAttribute("title", d.GeoObject.description);
                                row.innerHTML = d.GeoObject.name + ", " + d.GeoObject.description;
                                yaRes.appendChild(row);
                                yaRes.style.marginTop = "11px";
                                yaBtn.style.display = "none";
                                function yaView() {
                                    searchGroup.clearLayers();
                                    map.fitBounds([upper, lower]);
                                    sInp.value = d.GeoObject.name;
                                    sBox.classList.add("hide_result");
                                    sRes.innerHTML = "";
                                    yaRes.innerHTML = "";
                                    yaVal = "";
                                    let yaIcon = L.divIcon({
                                        className: "ya_icon",
                                        iconSize: [27, 27],
                                        iconAnchor: [0, 0],
                                        popupAnchor: [13.5, 3],
                                        html: "<img src=\""+ projectPath +"images/ya.svg\" alt=\"Яндекс поиск\">"
                                    });
                                    let yaPopup = L.popup({
                                        className: "mgtniip_popup ya_popup",
                                        closeButton: true,
                                        autoClose: true
                                    }).setContent(
                                        "<div>" + d.GeoObject.name + "</div>" +
                                        "<div style=\"margin-top:5px; color:#95a6b1;\">" + d.GeoObject.metaDataProperty.GeocoderMetaData.text + "</span></div>"
                                    );
                                    L.marker(pos, {icon: yaIcon}).bindPopup(yaPopup).addTo(searchGroup);
                                    searchGroup.addTo(map);
                                    map.openPopup(yaPopup, pos);
                                }
                                row.addEventListener("focusin", function () {
                                    row.addEventListener("keydown", function (e) {
                                        if (e.key === "Enter") {
                                            yaView();
                                        }
                                    });
                                });
                                row.addEventListener("click", function() {
                                    yaView();
                                });
                            });
                        }
                        sInp.value = yaVal;
                    }
                } catch (e) {
                    console.log(e);
                }
            })();
        }
        yaBtn.addEventListener("click", function() {
            yaShow();
        });
        sInp.addEventListener("keydown", function(e) {
            if (e.key === "Enter") {
                yaShow();
            }
        });
    }
    console.timeEnd("search");
}