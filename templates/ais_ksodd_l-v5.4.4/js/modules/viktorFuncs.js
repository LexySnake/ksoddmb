/* add by ma2dy */

function SaveAsFile(t,f,m) {
            try {
                var b = new Blob([t],{type:m});
                saveAs(b, f);
            } catch (e) {
                window.open("data:"+m+"," + encodeURIComponent(t), '_blank','');
            }
        }


function downloadCSVSigns(segment_id) 
{
	 let requestTime = new Date().getTime();
	 let reqData = {
             "type": "segments",
             "zoom": 21,
             "names": [segment_id]
         };
	 (async() => {
	        try {
	            let response = await fetch('https://niikeeper.com/nii_api/v0.5.9/signs_go_box_post_geo?token=xMKNeZPavLESGxkSJlGeDg8PwIYS54yz', {
	                method: "POST",
	                body: JSON.stringify(reqData)
	            });
	            let data = await response.json();
	            console.log("CSV Signs request has taken " + (new Date().getTime() - requestTime) +" ms");
	            // Очищение группы при движении карты
	            if (data.length !== 0) {
	              //console.log(data);
	            } else {
	                return;
	            }
	            let csv_data ='';
	            
	            for (let k in data) {
	            	//console.log(data[k]);
	                let rack = data[k].geoJSON.geometry.coordinates;
	                let rackType = data[k].geoJSON.properties.installationType;
	            	let division=data[k].geoJSON.properties.division;
                	let district=data[k].geoJSON.properties.district;
                	let street=data[k].geoJSON.properties.street;
                	let item_seg_id=data[k].geoJSON.properties.segmentId;
	                let signsArr = data[k].signs;
	                let signsCnt = signsArr.length;
	                
	                /*
	                 *	bilateral:""
						defects:"0"
						height:""
						interactive:"1"
						mirror:"0"
						signNumber:"5.19.1"
						signType:"Двухсторонний"
						status:"Существующий"
						visibility:""
	                 * 
	                 */
	                for (let m in signsArr) {
	                	//console.log(signsArr[m]);
	                		let signNumber= signsArr[m].properties.signNumber;
	                		let signDefects= signsArr[m].properties.defects;
	                		let signMirror= signsArr[m].properties.mirror;
	                		let signType= signsArr[m].properties.signType;
	                		let signStatus= signsArr[m].properties.status;
	                		let signInteractive= signsArr[m].properties.interactive;

	                	
	                		 csv_data=csv_data+division+';'+district+';'+street+';'+item_seg_id+';'+signNumber+';'+signStatus+';'+signType+';'+signInteractive+';'+signDefects+';'+'\n\r';
	 	                	
	                }
	                 
	                
	            }
	            
	            
	            	let csv_header ='Адм. округ;Адм. район;Улица;Сегмент;N по ГОСТ;Статус;Тип;!!Информативность;Дефекты;\n\r';
	            
	            let csv_text=csv_header+csv_data;
	        	let filename="signs_"+segment_id+".csv";
	            
	        	SaveAsFile(csv_text,filename,"text/csv;charset=utf-8");
	             
	        }catch(e) {
	            console.log("catch", e);
	        }
	    })();
	        
	
	
	let text ='Номер;Название;Дата;Тип\n\r'
		+'1;Знак;01.04.2018;Стандарт\n\r'
		+'2;Знак пешеход;02/04/2018;Стойка\n\r'
		;
	
	
	//SaveAsFile(text,"signs.csv","text/csv;charset=utf-8");

	//alert('downloadCSVSigns: id '+segment_id);
}

function downloadCSVPoints(segment_id) 
{
	 let requestTime = new Date().getTime();
	 let reqData = {
             "type": "segments",
             "zoom": 21,
             "names": [segment_id]
         };
	 (async() => {
	        try {
	            let response = await fetch('https://niikeeper.com/nii_api/v0.5.9/roadpoints_go_box_post?token=xMKNeZPavLESGxkSJlGeDg8PwIYS54yz', {
	                method: "POST",
	                body: JSON.stringify(reqData)
	            });
	            let data = await response.json();
	            console.log("CSV Points request has taken " + (new Date().getTime() - requestTime) +" ms");
	             
	            if (data.length !== 0) {
	             // console.log(data);
	            } else {
	                return;
	            }
	            let csv_data ='';
	            for (let k in data.data) {
	                if (data.data.hasOwnProperty(k)) {
	                	
	                	//console.log(data.data[k]);
	                	
	                	let id=data.data[k].id;	
	                	let division=data.data[k].division;
	                	let district=data.data[k].district;
	                	let street=data.data[k].street;
	                	let item_seg_id=data.data[k].segment_id;
	                    let pointMarkName = "";
	                       if (data.data[k].road_point_number === "1.24.1") {
	                           if (data.data[k].param1 === "") {
	                               pointMarkName = data.data[k].road_point_number;
	                           } else {
	                               pointMarkName = data.data[k].param1;
	                           }
	                       } else {
	                           pointMarkName = data.data[k].road_point_number;
	                       }
	                       let status=data.data[k].status;
	                       let defects=data.data[k].defects;
	                       let angle=data.data[k].angle;
	                       let coords = data.data[k].geom_point;
	                       let lat =coords[0];
	                       let lon =coords[1];
	                       
	                       csv_data=csv_data+id+';'+division+';'+district+';'+street+';'+item_seg_id+';'+pointMarkName+';'+status+';'+defects+';'+angle+';'+lat+';'+lon+';'+'\n\r';
	                	
	                }
	                
	            }   
	            
	            let csv_header ='Id;Адм. округ;Адм. район;Улица;Сегмент;N по ГОСТ;Статус;Дефекты;Угол;Широта;Долгота\n\r';
	            
	            let csv_text=csv_header+csv_data;
	        	let filename="points_"+segment_id+".csv";
	            
	        	SaveAsFile(csv_text,filename,"text/csv;charset=utf-8");
	             
	        }catch(e) {
	            console.log("catch", e);
	        }
	    })();
	        
	
	
	 
}

function downloadCSVLines(segment_id) 
{
	
	 let requestTime = new Date().getTime();
	 let reqData = {
             "type": "segments",
             "zoom": 21,
             "names": [segment_id]
         };
	 (async() => {
	        try {
	            let response = await fetch('https://niikeeper.com/nii_api/v0.5.9/roadmarkup_go_box_post?token=xMKNeZPavLESGxkSJlGeDg8PwIYS54yz', {
	                method: "POST",
	                body: JSON.stringify(reqData)
	            });
	            let data = await response.json();
	            console.log("CSV Linear Road Markup request has taken " + (new Date().getTime() - requestTime) +" ms");
	            // Очищение группы при движении карты
	            if (data.length !== 0) {
	             // console.log(data);
	            } else {
	                return;
	            }
	            let csv_data ='';
	            for (let k in data.data) {
	                if (data.data.hasOwnProperty(k)) {
	                	
	                	
	                	//console.log(data.data[k]);
	                	
	                	 
	                	let division=data.data[k].division;
	                	let district=data.data[k].district;
	                	let street=data.data[k].street;
	                	let item_seg_id=data.data[k].segment;
	                	let lineStringsData = data.data[k].linestrings;
	                    let arcsData = data.data[k].arcs;
	                    let lineStrings = [];
	                    let arcs = [];
	                    let markupNumber = data.data[k].road_markup_number;
	                    let status=data.data[k].status;
	                    let defects=data.data[k].defects;
	                    let lineLength = 0;
	                    let linearMarkAngle;
	                    /*
	                    if (lineStringsData[0] !== undefined) {
	                        linearMarkAngle = L.GeometryUtil.bearing(L.latLng(lineStringsData[0][1], lineStringsData[0][0]), L.latLng(lineStringsData[1][1], lineStringsData[1][0])) - 90;
	                        let lsCnt = lineStringsData.length;
	                        if (lsCnt === 0) {
	                            continue;
	                        }
	                        for (let i = 0; i < lsCnt; i ++) {
	                            lineStrings.push([lineStringsData[i][1], lineStringsData[i][0]]);
	                        }
	                        let linePosx = (map.latLngToLayerPoint(lineStrings[lineStrings.length - 1]).x - map.latLngToLayerPoint(lineStrings[0]).x);
	                        let linePosY = (map.latLngToLayerPoint(lineStrings[lineStrings.length - 1]).y - map.latLngToLayerPoint(lineStrings[0]).y);
	                        lineLength = Math.sqrt(Math.pow(linePosx, 2) + Math.pow(linePosY, 2));

	                    }
	                    if (arcsData !== null) {
	                        let arcCnt = arcsData.length;
	                        if (arcCnt === 0) {
	                            continue;
	                        }
	                        for (let i = 0; i < arcCnt; i ++) {
	                            for (let j = 0; j < arcsData[i]; j++) {
	                                arcs.push([arcsData[i][j][1], arcsData[i][j][0]]);
	                            }
	                        }
	                    }
	                    */
	                       /*
	                      
	                       let angle=data.data[k].angle;
	                       let coords = data.data[k].geom_point;
	                       let lat =coords[0];
	                       let lon =coords[1];
	                       
	                       
	                	*/
	                    
	                    csv_data=csv_data+division+';'+district+';'+street+';'+item_seg_id+';'+markupNumber+';'+status+';'+defects+';'+'\n\r';
	                }
	                
	            }   
	            
	            let csv_header ='Адм. округ;Адм. район;Улица;Сегмент;N по ГОСТ;Статус;Дефекты;\n\r';
	            
	            let csv_text=csv_header+csv_data;
	        	let filename="lines_"+segment_id+".csv";
	            
	        	SaveAsFile(csv_text,filename,"text/csv;charset=utf-8");
	            
	             
	        }catch(e) {
	            console.log("catch", e);
	        }
	    })();
	
 
 
}

function showSignWindow(segment_id) 
{
	alert('downloadCSVLines: id '+segment_id);
}
function closePopupWindow()
{
	// console.log('test');
	var x = document.getElementsByClassName("leaflet-popup-close-button"); 
	console.log(x);
	x[0].click();
	}

function hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}


function showSignInfo(btn,i)
{
	
	 var t_info=document.getElementById("trdinfo"+i);
	 
	 if(hasClass(t_info, "hidetd")) {
		    
		 
		 btn.innerHTML='-';
		 t_info.classList.remove("hidetd");
		}
	 else
		{ 
		 
		 btn.innerHTML='+';
		 t_info.classList.add("hidetd");
		}
 
}