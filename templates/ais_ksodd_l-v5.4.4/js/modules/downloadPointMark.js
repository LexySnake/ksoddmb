function downloadPointMark(reqData, pointMarkGroup, zoomLevel, projectPath) {
    let requestTime = new Date().getTime();
    (async() => {
        try {
            let response = await fetch(domain + "roadpoints_go_box_post" + token, {
                method: "POST",
                body: JSON.stringify(reqData)
            });
            let data = await response.json();
            console.log("Point Road Markup request has taken " + (new Date().getTime() - requestTime) +" ms");
            if (data.length !== 0) {
                pointMarkGroup.clearLayers();
            } else {
                return;
            }
            for (let k in data.data) {
                if (data.data.hasOwnProperty(k)) {
                    let pointMarkAngle = data.data[k].angle;
                    let coords = data.data[k].geom_point.reverse();
                    let pointMarkName = "";
                    if (data.data[k].road_point_number === "1.24.1") {
                        if (data.data[k].param1 === "") {
                            pointMarkName = data.data[k].road_point_number;
                        } else {
                            pointMarkName = data.data[k].param1;
                        }
                    } else {
                        pointMarkName = data.data[k].road_point_number;
                    }
                    let iconUrl = projectPath + "images/icons_nii/roadmarkup_names/" + pointMarkName + ".svg";
                    let iconSize = Math.pow(zoomLevel/10, zoomLevel/4.2)-10;
                    let transform_scale_x = 1.1;
                    let transform_scale_y = 3.6;
                    if (data.data[k].road_point_number === "1.24.3") {
                        transform_scale_y = 1.3;
                    }
                    let rpoint = L.marker(coords, {
                        rotationAngle: -1*(pointMarkAngle*180/Math.PI),
                        icon: L.divIcon({
                            className: "pointRoadMarkup",
                            iconAnchor: [iconSize/2, iconSize/2],
                            html:"<div><img style=\"-webkit-transform:scale(" + transform_scale_x + "," + transform_scale_y + ") ;width:"+(iconSize)+"px;height:"+(iconSize)+"px;\" src=\"" + iconUrl + "\" /></div>"
                        })
                    }).bindPopup(data.data[k].road_point_number);
                    L.featureGroup([rpoint]).addTo(pointMarkGroup);
                }
            }
        } catch(e) {
            console.log("catch", e);
        }
    })();
}