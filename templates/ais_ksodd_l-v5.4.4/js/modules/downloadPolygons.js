function downloadPolygons(divObj, distObj, refObj, strObj, segObj, sO) {
    // Initialization divisions
    (async() => {
        try {
            let responseCnt = await fetch(domain + "get_divisions_cnt" + token);
            let dataCnt = await responseCnt.json();
            divObj.features = divJSON.features;
            divObj.features.forEach(function(d) {
                for (let k in dataCnt.Summary) {
                    if (k === d.properties.name) {
                        d.properties["cnt"] = dataCnt.Summary[k];
                    }
                }
            });
            pView(map, divisionsAll, null, objMap.division, "divisions_all");
        } catch(e) {
            console.log(e);
        }
    })();

    // Initialization districts
    (async() => {
        try {
            let responseCnt = await fetch(domain + "get_districts_cnt" + token);
            let dataCnt = await responseCnt.json();
            distObj.features = distJSON.features;
            distObj.features.forEach(function(d) {
                for (let k in dataCnt.Summary) {
                    if (k === d.properties.name) {
                        d.properties["cnt"] = dataCnt.Summary[k];
                    }
                }
            });
            pView(map, districtsAll, null, objMap.district, "districts_all");
            let sBox = document.getElementById("search");
            sBox.style.display = "block";
            downloadMenu(divObj, distObj);
        } catch(e) {
            console.log(e);
        }
    })();

    // Initialization streets and reference objects
    (async() => {
        try {
            // Streets
            let response = await fetch(domain + getPolygon + token + "&type=streets");
            let data = await response.json();
            let responseCnt = await fetch(domain + "get_streets_cnt" + token);
            let dataCnt = await responseCnt.json();
            strObj.features = data.Features;
            strObj.features.forEach(function(d) {
                for (let k in dataCnt.Summary) {
                    if (k === d.properties.name) {
                        d.properties["cnt"] = dataCnt.Summary[k];
                    }
                }
            });
            // Reference objects
            let refResponse = await fetch(domain + "reference_children" + token, {
                headers: {
                    "content-type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    "type": "all",
                    "name": ""
                })
            });
            let refData = await refResponse.json();
            for (let k in refData.Data) {
                refObj[k] = refData.Data[k];
            }
            for (let div in refObj) {
                for (let dist in refObj[div]) {
                    for (let str in refObj[div][dist]) {
                        strObj.features.forEach(function(d) {
                            if (str === d.properties.name) {
                                refObj[div][dist][str] = d.properties.number;
                            }
                        });
                    }
                    for (let str in refObj[div][dist]) {
                        if (typeof(refObj[div][dist][str]) === "object") {
                            refObj[div][dist][str] = refObj[div][dist][str][0].split("_")[0];
                        }
                    }
                }
            }

        } catch(e) {
            console.log(e);
        }
    })();

    // Initialization segments
    (async() => {
        try {
            let responseInt = await fetch(domain + getPolygon + token + "&type=segment_intersections");
            let dataInt = await responseInt.json();
            let responseSt = await fetch(domain + getPolygon + token + "&type=segment_streets");
            let dataSt = await responseSt.json();
            let responseCnt = await fetch(domain + "get_segments_cnt" + token);
            let dataCnt = await responseCnt.json();
            dataInt.features.forEach(function(d) {
                segObj.features.push(d);
            });
            dataSt.features.forEach(function(d) {
                segObj.features.push(d);
            });
            segObj.features.forEach(function(d) {
                d.properties["cnt"] = {};
                for (let k in dataCnt.Summary) {
                    if (d.properties.name === k) {
                        d.properties["cnt"] = dataCnt.Summary[k];
                    }
                }
                if ((d.properties.name.split("_")[1]) && (d.properties.name.split("_")[1] !== "") && (d.properties.name.split("_")[1].slice(0,1) === "(")) {
                    d.properties["type"] = "Пролёт";
                } else {
                    d.properties["type"] = "Перекрёсток";
                }
            });
            pView(map, null, true, objMap.segment, null);
            getSO(sO, segObj);
        } catch(e) {
            console.log(e);
        }
    })();
}
function pView(map, group, popup, obj, domId) {
    obj.obj.features.forEach(function(element) {
        let g;
        L.geoJSON(element, {
            onEachFeature: function(feature, polygon) {
                polygon.setStyle({
                    weight: getOpacity(map.getZoom())[1] + obj.weight - 2,
                    opacity: 1.0,
                    color: obj.allColor,
                    fillOpacity: 0.0,
                    pane: obj.allPane
                });
                map.on("zoom", function() {
                    polygon.setStyle({
                        weight: getOpacity(map.getZoom())[1] + obj.weight - 2
                    });
                });
                if (!group) {
                    if (element.properties.type === "Пролёт") {
                        g = segStGroup;
                    } else if (element.properties.type === "Перекрёсток") {
                        g = segIntGroup;
                    }
                    polygon.setStyle({
                        fillColor: obj.fill,
                        fillOpacity: getOpacity(map.getZoom())[2]
                    });
                    map.on("zoom", function() {
                        polygon.setStyle({
                            fillColor: obj.fill,
                            fillOpacity: getOpacity(map.getZoom())[2]
                        });
                    });
                } else {
                    g = group;
                }
                if (popup) {
                    // TODO(Leksa): Оптимизировать popup, сделать более универсальными
                    polygon.bindPopup(function() {
                        let cnt = element.properties.cnt;
                        let div = document.createElement("div");
                        div.className = "segment_popup";
                        let table = document.createElement("table");
                        let box = document.createElement("div");
                        let h4 = document.createElement("h4");
                        h4.innerHTML = "Аналитика сегмента:";
                        let btn1 = document.createElement("div");
                        btn1.className = "line";
                        let img1 = document.createElement("img");
                        let img2 = document.createElement("img");
                        let img3 = document.createElement("img");
                        img2.src = projectPath + "images/icons_nii/extension_csv.svg";
                        img3.src = projectPath + "images/icons_nii/extension_csv.svg";
                        img1.src = projectPath + "images/icons_nii/extension_csv.svg";
                        let t1 = document.createTextNode("Дорожные знаки");
                        btn1.appendChild(img1);
                        btn1.appendChild(t1);
                        btn1.onclick = function(){ downloadCSVSigns(element.properties.name)};
                        let btn2 = document.createElement("div");
                        btn2.className = "line";
                        let t2 = document.createTextNode("Точечная");
                        btn2.appendChild(img2);
                        btn2.appendChild(t2);
                        btn2.onclick = function(){ downloadCSVPoints(element.properties.name)};
                        let btn3 = document.createElement("div");
                        btn3.className = "line";
                        let t3 = document.createTextNode("Линейная");
                        btn3.appendChild(img3);
                        btn3.appendChild(t3);
                        btn3.onclick = function(){ downloadCSVLines(element.properties.name)};
                        box.appendChild(h4);
                        box.appendChild(btn1);
                        box.appendChild(btn2);
                        box.appendChild(btn3);
                        table.className = "segment";
                        let green = document.createElement("span");
                        green.className = "green";
                        let orange = document.createElement("span");
                        orange.className = "orange";
                        let red = document.createElement("span");
                        red.className = "red";
                        let string = "";
                        if (cnt) {
                            let n = 0;
                            let number = 0;
                            for (let c in cnt) {
                                (c === "Проектируемый" ? orange.innerHTML = cnt[c] :
                                    (c === "Демонтируемый" ? red.innerHTML = cnt[c] :
                                        n += parseInt(cnt[c])));
                            }
                            green.innerHTML = n;
                            if ((orange.textContent) && (red.textContent)) {
                                number = n + parseInt(orange.textContent) + parseInt(red.textContent);
                                string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML + " + " + red.outerHTML;
                            } else if (orange.textContent) {
                                number = n + parseInt(orange.textContent);
                                string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML;
                            } else if (red.textContent) {
                                number = n + parseInt(red.textContent);
                                string = number + " шт: " + green.outerHTML + " + " + red.outerHTML;
                            } else {
                                string = green.outerHTML + " шт.";
                            }
                        } else {
                            string = "Информация отсутсвует";
                        }
                        let imgName = "";
                        (element.properties.type === "Пролёт" ? imgName = "segSt" : imgName = "segInt");
                        table.innerHTML =
                            "<tbody>" +
                            "<tr>" +
                            "<th>Сегмент:</th>" +
                            "<td style=\"color:#bb15d9;\">" +
                                "<img src=" + projectPath + "images/icons_nii/" + imgName + "_lil.svg> " +
                                element.properties.name +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th>Тип:</th>" +
                            "<td>" + element.properties.type + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th>Статус:</th>" +
                            "<td style=\"color:#75b62e;\">Существующий </td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th>Знаки:</th>" +
                            "<td>" + string + "</td>" +
                            "</tr>" +
                            "</tbody>";
                        div.appendChild(table);
                        div.appendChild(box);
                        return div;
                    }, {
                        autoPan: true,
                        className: "mgtniip_popup"
                    });
                }
            }
        }).addTo(g);
    });
    if (domId && group) {
        let chBox = document.getElementById(domId);
        chBox.addEventListener("click", function () {
            (chBox.checked === true ? group.addTo(map) : group.remove());
        });
    }
}
function download_3_27_Zones(zones_3_27_Group) {
    (async() => {
        try {
            let response = await fetch(domain + getPolygon + token + "&type=3_27_sign_zones");
            let data = await response.json();
            data.Features.forEach(function(element) {
                L.geoJSON(element, {
                    onEachFeature: function(feature, polyline) {
                        polyline.setStyle({
                            color: "#FF0000",
                            weight: 7,
                            className: "potatoline"
                        });
                    }
                }).addTo(zones_3_27_Group);
            });
        } catch(e) {
            console.log(e);
        }
    })();
}
function downloadRoadSides(roadSidesGroup) {
    roadSideJSON.features.forEach(function(element) {
        L.geoJSON(element, {
            onEachFeature: function(feature, polyline) {
                let roadSideColor = "#262626";
                if (feature.properties.roadsideType !== "Обычный") {
                    roadSideColor = "#008000";
                }
                polyline.setStyle({
                    color: roadSideColor,
                    weight: 4,
                    className: "roadside-line"
                });
            }
        }).addTo(roadSidesGroup);
    });
}