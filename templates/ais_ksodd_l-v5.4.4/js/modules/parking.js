function downloadParking() {
    let parkingOptions = {
        rendererFactory: L.canvas.tile,
        vectorTileLayerStyles: {
            polygons: function(properties, zoom) {
                return {
                    fillColor: "#40aaff",
                    fill: true,
                    fillOpacity: .4,
                    weight: 2,
                    color: "#40aaff",
                    opacity: 1.0
                }
            },
            lines: function(properties, zoom) {
                return {
                    fill: false,
                    weight: 1,
                    color: "#f00",
                    opacity: 1.0
                }
            },
            points: function(properties, zoom) {
                let r = 1;
                switch(zoom) {
                    case 17:
                        r = 2;
                        break;
                    case 18:
                        r = 4;
                        break;
                    case 19:
                        r = 8;
                        break;
                    case 20:
                        r = 16;
                        break;
                    case 21:
                        r = 18;
                        break;
                    case 22:
                        r = 18;
                        break;
                    default:
                        r = 1;
                        break;
                }
                return {
                    fill: false,
                    radius: r,
                    weight: 1,
                    color: "#2a20ff",
                    opacity: 1.0
                }
            }
        }
    };
    parking = L.vectorGrid.protobuf("https://niikeeper.com/nii_api/v0.5.9/parkings/{z}/{x}/{y}", parkingOptions);
    parking.addTo(parkingGroup);
}