function downloadMenu(divObj, distObj) {
    let divisionsUnit = L.layerGroup();
    let districtsUnit = L.layerGroup();
    let menu = document.getElementById("menu");
    let divisionsProperties = {
        "ЦАО": {
            name: "Центральный АО",
            center: {lat: 55.753668000000005, lng: 37.6139225}, //self
            districts: ["Арбат", "Басманный", "Замоскворечье", "Красносельский", "Мещанский", "Пресненский", "Таганский", "Тверской", "Хамовники", "Якиманка"],
            exceptions: ["Беговой"]
        },
        "ЗелАО": {
            name: "Зеленоградский АО",
            center: {lat: 55.984407000000004, lng: 37.199783}, //self
            districts: ["Матушкино", "Савёлки", "Старое Крюково", "Силино", "Крюково"],
            exceptions: []
        },
        "САО": {
            name: "Северный АО",
            center: {lat: 55.86887185750015, lng: 37.488012191500204},
            // districts: ["Аэропорт", "Беговой", "Бескудниковский", "Войковский", "Восточное Дегунино",
            //     Головинский район
            //     Дмитровский район
            //     Западное Дегунино
            //     Коптево
            //     Левобережный
            //     Молжаниновский район
            //     Савёловский район
            //     Сокол
            //     Тимирязевский район
            //     Ховрино
            //     Хорошёвский район
            // ],
            exceptions: ["Куркино", "Покровское-Стрешнево", "Северное Тушино", "Строгино", "Щукино", "Южное Тушино"]
        },
        "СВАО": {
            name: "Северо-Восточный АО",
            center: {lat: 55.870078, lng: 37.626433}, //self
            // districts: [Алексеевский район
            //     Алтуфьевский район
            //     Бабушкинский район
            //     Бибирево
            //     Бутырский район
            //     Лианозово
            //     Лосиноостровский район
            //     Марфино
            //     Марьина Роща
            //     Останкинский район
            //     Отрадное
            //     Ростокино
            //     Свиблово
            //     Северный
            //     Северное Медведково
            //     Южное Медведково
            //     Ярославский район],
            exceptions: ["Бескудниковский", "Восточное Дегунино", "Савеловский", "Тимирязьевский"]
        },
        "ВАО": {
            name: "Восточный АО",
            center: {lat: 55.781257489750146, lng: 37.77435418550015},
            // districts: [Богородское
            //     Вешняки
            //     Восточный
            //     Восточное Измайлово
            //     Гольяново
            //     Ивановское
            //     Измайлово
            //     Косино-Ухтомский
            //     Метрогородок
            //     Новогиреево
            //     Новокосино
            //     Перово
            //     Преображенское
            //     Северное Измайлово
            //     Соколиная Гора
            //     Сокольники],
            exceptions: ["Лефортово", "Лосиноостровский", "Нижегородский", "Рязанский", "Южнопортовый", "Ярославский"]
        },
        "ЮВАО": {
            name: "Юго-Восточный АО",
            center: {lat: 55.698718089000096, lng: 37.7762957190001},
            // districts: [Выхино-Жулебино
            //     Капотня
            //     Кузьминки
            //     Лефортово
            //     Люблино
            //     Марьино
            //     Некрасовка
            //     Нижегородский район
            //     Печатники
            //     Рязанский район
            //     Текстильщики
            //     Южнопортовый район],
            exceptions: ["Вешняки", "Косино-Ухтомский", "Новогиреево", "Новокосино", "Перово", "Печатники"]
        },
        "ЮАО": {
            name: "Южный АО",
            center: {lat: 55.648215, lng: 37.6815965}, //self
            // districts: [Бирюлёво Восточное
            //     Бирюлёво Западное
            //     Братеево
            //     Даниловский район
            //     Донской район
            //     Зябликово
            //     Москворечье-Сабурово
            //     Нагатино-Садовники
            //     Нагатинский Затон
            //     Нагорный район
            //     Орехово-Борисово Северное
            //     Орехово-Борисово Южное
            //     Царицыно
            //     Чертаново Северное
            //     Чертаново Центральное
            //     Чертаново Южное],
            exceptions: ["Котловка", "Марьино", "Текстильщики"]
        },
        "ЮЗАО": {
            name: "Юго-Западный АО",
            center: {lat: 55.602503, lng: 37.535295500000004}, //self
            // districts: [Академический район
            //     Гагаринский район
            //     Зюзино
            //     Коньково
            //     Котловка
            //     Ломоносовский район
            //     Обручевский район
            //     Северное Бутово
            //     Тёплый Стан
            //     Черёмушки
            //     Южное Бутово
            //     Ясенево],
            exceptions: ["Проспект Вернадского"]
        },
        "ЗАО": {
            name: "Западный АО",
            center: {lat: 55.703120546, lng: 37.390555406000104},
            // districts: [Внуково
            //     Дорогомилово
            //     Крылатское
            //     Кунцево
            //     Можайский район
            //     Ново-Переделкино
            //     Очаково-Матвеевское
            //     Проспект Вернадского
            //     Раменки
            //     Солнцево
            //     Тропарёво-Никулино
            //     Филёвский Парк
            //     Фили-Давыдково],
            exceptions: ["Коньково", "Ломоносовский", "Обручевский", "Теплый Стан", "Хорошево-Мневники", "Хорошевский"]
        },
        "СЗАО": {
            name: "Северо-Западный АО",
            center: {lat: 55.827298, lng: 37.4269485}, //self
            // districts: [Куркино
            //     Митино
            //     Покровское-Стрешнево
            //     Северное Тушино
            //     Строгино
            //     Хорошёво-Мнёвники
            //     Щукино
            //     Южное Тушино],
            exceptions: ["Крылатское", "Левобережный", "Сокол", "Ховрино"]
        }
    };
    menu.addEventListener("click", function() {
        let units = document.getElementById("control_units");
        if (units.hasChildNodes() === false) {
            divObj.features.forEach(function(div) {
                let divLine = document.createElement("div");
                divLine.className = "line";
                let divUnit = document.createElement("div");
                divUnit.className = "div_unit";
                divUnit.innerHTML = divisionsProperties[div.properties.name].name;
                let divInfo = document.createElement("div");
                divInfo.className = "info";
                let distBox = document.createElement("div");
                distBox.className = "dist_box";
                divUnit.addEventListener("click", function() {
                    map.closePopup();
                    let divSib = document.getElementsByClassName("div_active");
                    for (let i  = 0, cnt = divSib.length; i < cnt; i++) {
                        if ((divSib[i] !== divUnit) && (divSib[i].classList.contains("div_active"))) {
                            if (divSib[i].parentElement.nextElementSibling.style.maxHeight) {
                                divSib[i].parentElement.nextElementSibling.style.maxHeight = null;
                            }
                            divSib[i].classList.remove("div_active");
                        }
                    }
                    divUnit.classList.toggle("div_active");
                    if (divUnit.classList.contains("div_active") === true) {
                        divisionsUnit.clearLayers();
                        let divStrokeLayer = L.layerGroup([
                            L.geoJSON(div, {
                                onEachFeature: function (feature, divPolygon) {
                                    divPolygon.setStyle({
                                        weight: getOpacity(map.getZoom())[1] + 2,
                                        opacity: 1.0,
                                        color: "#ffffff",
                                        fillColor: "#00ff79",
                                        fillOpacity: getOpacity(map.getZoom())[0],
                                        pane: "divisionsUnitPane",
                                        interactive: false
                                    });
                                    map.on("zoomend", function() {
                                        divPolygon.setStyle({
                                            weight: getOpacity(map.getZoom())[1] + 2,
                                            fillOpacity: getOpacity(map.getZoom())[0]
                                        });
                                    });
                                }
                            })
                        ]);
                        let divFoundLayer = L.layerGroup([
                            L.geoJSON(div, {
                                onEachFeature: function (feature, divPolygon) {
                                    map.fitBounds(divPolygon.getBounds());
                                    divPolygon.setStyle({
                                        weight: getOpacity(map.getZoom())[1],
                                        opacity: 1.0,
                                        color: "#49ee97",
                                        pane: "divisionsUnitPane",
                                        fillOpacity: 0.0,
                                        interactive: false
                                    });
                                    map.on("zoomend", function() {
                                        divPolygon.setStyle({
                                            weight: getOpacity(map.getZoom())[1]
                                        });
                                    });
                                    if (distBox.hasChildNodes() === false) {
                                        distObj.features.forEach(function(dist) {
                                            L.geoJSON(dist, {
                                                onEachFeature: function (feature, distPolygon) {
                                                    if ((divPolygon.getBounds().contains(distPolygon.getBounds()) === true) && (divisionsProperties[div.properties.name].exceptions.indexOf(dist.properties.name) === (-1))) {
                                                        let distLine = document.createElement("div");
                                                        distLine.className = "line";
                                                        let distUnit = document.createElement("div");
                                                        distUnit.className = "dist_unit";
                                                        let distInfo = document.createElement("div");
                                                        distInfo.className = "info";
                                                        distUnit.innerHTML = dist.properties.name;
                                                        distUnit.style.opacity = ".2";
                                                        for (let k in refObj) {
                                                            for (let kk in refObj[k]) {
                                                                if (kk === dist.properties.name) {
                                                                    let x = 0;
                                                                    for (let kkk in refObj[k][kk]) {
                                                                        x++;
                                                                    }
                                                                    if (x > 20) {
                                                                        distUnit.style.opacity = "1";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        distLine.appendChild(distUnit);
                                                        distLine.appendChild(distInfo);
                                                        distBox.appendChild(distLine);
                                                    }
                                                }
                                            });
                                        });
                                    }
                                }
                            })
                        ]);
                        distBox.style.maxHeight = distBox.scrollHeight + "px";
                        divStrokeLayer.addTo(divisionsUnit);
                        divFoundLayer.addTo(divisionsUnit);
                        divisionsUnit.addTo(map);
                        distObj.features.forEach(function(dist) {
                            let distUnit = document.getElementsByClassName("dist_unit");
                            for (let i = 0, cnt = distUnit.length; i < cnt; i++) {
                                if (distUnit[i].textContent === dist.properties.name) {
                                    distUnit[i].addEventListener("click", function() {
                                        map.closePopup();
                                        let distSib = document.getElementsByClassName("dist_active");
                                        for (let j = 0, cnt = distSib.length; j < cnt; j++) {
                                            if ((distSib[j] !== distUnit[i]) && (distSib[j].classList.contains("dist_active"))) {
                                                distSib[j].classList.remove("dist_active");
                                            }
                                        }
                                        distUnit[i].classList.toggle("dist_active");
                                        if (distUnit[i].classList.contains("dist_active") === true) {
                                            districtsUnit.clearLayers();
                                            let distStrokeLayer = L.layerGroup([
                                                L.geoJSON(dist, {
                                                    onEachFeature: function (feature, distPolygon) {
                                                        distPolygon.setStyle({
                                                            weight: getOpacity(map.getZoom())[1],
                                                            opacity: 1.0,
                                                            color: "#ffffff",
                                                            fillOpacity: getOpacity(map.getZoom())[0],
                                                            fillColor: "#00b4ff",
                                                            pane: "districtsUnitPane",
                                                            interactive: false
                                                        });
                                                        map.on("zoomend", function () {
                                                            distPolygon.setStyle({
                                                                weight: getOpacity(map.getZoom())[1],
                                                                fillOpacity: getOpacity(map.getZoom())[0]
                                                            });
                                                        });
                                                    }
                                                })
                                            ]);
                                            let distFoundLayer = L.layerGroup([
                                                L.geoJSON(dist, {
                                                    onEachFeature: function (feature, distPolygon) {
                                                        map.fitBounds(distPolygon.getBounds());
                                                        distPolygon.setStyle({
                                                            weight: getOpacity(map.getZoom())[1] - 2,
                                                            opacity: 1.0,
                                                            color: "#45aef8",
                                                            fillOpacity: 0.0,
                                                            pane: "districtsUnitPane",
                                                            interactive: false
                                                        });
                                                        map.on("zoomend", function () {
                                                            distPolygon.setStyle({
                                                                weight: getOpacity(map.getZoom())[1] - 2
                                                            });
                                                        });
                                                    }
                                                })
                                            ]);
                                            distStrokeLayer.addTo(districtsUnit);
                                            distFoundLayer.addTo(districtsUnit);
                                            districtsUnit.addTo(map);
                                        } else {
                                            districtsUnit.clearLayers();
                                            districtsUnit.remove();
                                        }
                                    });

                                    distUnit[i].parentElement.addEventListener("mouseover", function() {
                                        hoverGroup.clearLayers();
                                        let foundLayer = L.layerGroup([
                                            L.geoJSON(dist, {
                                                onEachFeature: function (feature, distPolygon) {
                                                    distPolygon.setStyle({
                                                        opacity: 0.0,
                                                        color: "#3362ab",
                                                        fillOpacity: 0.2,
                                                        fillColor: "#3362ab",
                                                        pane: "districtsAllPane"
                                                    });
                                                }
                                            })
                                        ]);
                                        foundLayer.addTo(hoverGroup);
                                        hoverGroup.addTo(map);
                                    });
                                    distUnit[i].parentElement.addEventListener("mouseout", function() {
                                        hoverGroup.clearLayers();
                                    });
                                    distUnit[i].nextElementSibling.addEventListener("click", function() {
                                        for (let j = 0, cnt = distUnit[i].parentElement.parentElement.children.length; j < cnt; j++) {
                                            if (distUnit[i].parentElement.parentElement.children[j].children[0].classList.contains("dist_active")) {
                                                distUnit[i].parentElement.parentElement.children[j].children[0].classList.remove("dist_active");
                                            }
                                        }
                                        divisionsUnit.clearLayers();
                                        districtsUnit.clearLayers();
                                        map.closePopup();
                                        L.geoJSON(dist, {
                                            onEachFeature: function (feature, distPolygon) {
                                                map.fitBounds(distPolygon.getBounds());
                                                distPolygon.setStyle({
                                                    weight: 0,
                                                    opacity: 0.0,
                                                    color: "#00b4ff",
                                                    fillOpacity: 0.4,
                                                    fillColor: "#00b4ff",
                                                    pane: "districtsUnitPane",
                                                    interactive: false
                                                }).addTo(districtsUnit);
                                                districtsUnit.addTo(map);
                                                let distPopupBox = document.createElement("div");
                                                createDistPopup(distObj, dist, distPopupBox);
                                                map.openPopup(distPopupBox.outerHTML, distPolygon.getBounds().getCenter(), {
                                                    className: "mgtniip_popup",
                                                    closeButton: true,
                                                    autoClose: true
                                                });
                                            }
                                        });
                                    });
                                }
                            }
                        });
                    } else {
                        distBox.style.maxHeight = null;
                        divisionsUnit.clearLayers();
                        divisionsUnit.remove();
                        districtsUnit.clearLayers();
                        for (let i = 0, cnt = divUnit.parentElement.nextElementSibling.children.length; i < cnt; i++) {
                            if (divUnit.parentElement.nextElementSibling.children[i].children[0].classList.contains("dist_active")) {
                                divUnit.parentElement.nextElementSibling.children[i].children[0].classList.remove("dist_active");
                            }
                        }
                    }
                });
                divLine.appendChild(divUnit);
                divLine.appendChild(divInfo);
                units.appendChild(divLine);
                units.appendChild(distBox);
                divUnit.parentElement.addEventListener("mouseover", function () {
                    hoverGroup.clearLayers();
                    let foundLayer = L.layerGroup([
                        L.geoJSON(div, {
                            onEachFeature: function (feature, divPolygon) {
                                divPolygon.setStyle({
                                    opacity: 0.0,
                                    color: "#23bb6b",
                                    fillOpacity: 0.3,
                                    fillColor: "#23bb6b",
                                    pane: "divisionsAllPane"
                                });
                            }
                        })
                    ]);
                    foundLayer.addTo(hoverGroup);
                    hoverGroup.addTo(map);
                });
                divUnit.parentElement.addEventListener("mouseout", function () {
                    hoverGroup.clearLayers();
                });
                divInfo.addEventListener("click", function() {
                    for (let i = 0, cnt = divUnit.parentElement.children.length; i < cnt; i++) {
                        if (divUnit.parentElement.children[i].classList.contains("div_active")) {
                            divUnit.parentElement.children[i].classList.remove("div_active");
                        }
                    }
                    divInfo.style.opacity = "1";
                    divInfo.style.display = "block";
                    divisionsUnit.clearLayers();
                    districtsUnit.clearLayers();
                    map.closePopup();
                    L.geoJSON(div, {
                        onEachFeature: function (feature, divPolygon) {
                            map.fitBounds(divPolygon.getBounds());
                            divPolygon.setStyle({
                                weight: 0,
                                opacity: 0.0,
                                color: "#00ff79",
                                fillColor: "#00ff79",
                                fillOpacity: 0.4,
                                pane: "divisionsUnitPane",
                                interactive: false
                            }).addTo(divisionsUnit);
                            divisionsUnit.addTo(map);
                            let divPopupBox = document.createElement("div");
                            createDivPopup(divisionsProperties, divObj, div, divPopupBox);
                            map.openPopup(divPopupBox.outerHTML, divisionsProperties[div.properties.name].center, {
                                className: "mgtniip_popup",
                                closeButton: true,
                                autoClose: true
                            });
                        }
                    });
                });

                map.on("click", function() {
                    map.closePopup();
                });
                map.on("popupclose", function() {
                    map.closePopup();
                    divisionsUnit.clearLayers();
                    districtsUnit.clearLayers();
                    divInfo.style = "";
                });

            });
        }
    });
}
function createDivPopup(divisionsProperties, divObj, div, divPopupBox) {
    let cnt = div.properties.cnt;
    let caption = document.createElement("h3");
    caption.innerHTML = divisionsProperties[div.properties.name].name;
    divPopupBox.className = "segment_popup";
    let table = document.createElement("table");
    let box = document.createElement("div");
    let h4 = document.createElement("h4");
    h4.innerHTML = "Аналитика сегмента:";

    let btn1 = document.createElement("div");
    btn1.className = "line";
    let img1 = document.createElement("img");
    let img2 = document.createElement("img");
    let img3 = document.createElement("img");
    img2.src = projectPath + "images/icons_nii/extension_csv.svg";
    img3.src = projectPath + "images/icons_nii/extension_csv.svg";
    img1.src = projectPath + "images/icons_nii/extension_csv.svg";
    let t1 = document.createTextNode("Дорожные знаки");
    btn1.appendChild(img1);
    btn1.appendChild(t1);
    btn1.onclick = function(){ downloadCSVSigns(div.properties.name)};
    let btn2 = document.createElement("div");
    btn2.className = "line";
    let t2 = document.createTextNode("Точечная");
    btn2.appendChild(img2);
    btn2.appendChild(t2);
    btn2.onclick = function(){ downloadCSVPoints(div.properties.name)};
    let btn3 = document.createElement("div");
    btn3.className = "line";
    let t3 = document.createTextNode("Линейная");
    btn3.appendChild(img3);
    btn3.appendChild(t3);
    btn3.onclick = function(){ downloadCSVLines(div.properties.name)};
    box.appendChild(h4);
    box.appendChild(btn1);
    box.appendChild(btn2);
    box.appendChild(btn3);

    table.className = "segment";
    let green = document.createElement("span");
    green.className = "green";
    let orange = document.createElement("span");
    orange.className = "orange";
    let red = document.createElement("span");
    red.className = "red";
    if (cnt) {
        let n = 0;
        let number = 0;
        let string = "";
        for (let c in cnt) {
            (c === "Проектируемый" ? orange.innerHTML = cnt[c] :
                (c === "Демонтируемый" ? red.innerHTML = cnt[c] :
                    n += parseInt(cnt[c])));
        }
        green.innerHTML = n;
        if ((orange.textContent) && (red.textContent)) {
            number = n + parseInt(orange.textContent) + parseInt(red.textContent);
            string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML + " + " + red.outerHTML;
        } else if (orange.textContent) {
            number = n + parseInt(orange.textContent);
            string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML;
        } else if (red.textContent) {
            number = n + parseInt(red.textContent);
            string = number + " шт: " + green.outerHTML + " + " + red.outerHTML;
        } else {
            string = green.outerHTML + " шт.";
        }
        table.innerHTML =
            "<tbody>" +
            "<tr>" +
            "<th>Районы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Улицы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Сегменты:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Знаки:</th>" +
            "<td>" + string + "</td>" +
            "</tr>" +
            "</tbody>";
    } else {
        table.innerHTML =
            "<tbody>" +
            "<tr>" +
            "<th>Районы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Улицы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Сегменты:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Знаки:</th>" +
            "<td>Информация отсутсвует</td>" +
            "</tr>" +
            "</tbody>";
    }
    divPopupBox.appendChild(caption);
    divPopupBox.appendChild(table);
    divPopupBox.appendChild(box);
}
function createDistPopup(distObj, dist, distPopupBox) {
    let cnt = dist.properties.cnt;
    let caption = document.createElement("h3");
    caption.innerHTML = dist.properties.name;
    distPopupBox.className = "segment_popup";
    let table = document.createElement("table");
    let box = document.createElement("div");
    let h4 = document.createElement("h4");
    h4.innerHTML = "Аналитика сегмента:";

    let btn1 = document.createElement("div");
    btn1.className = "line";
    let img1 = document.createElement("img");
    let img2 = document.createElement("img");
    let img3 = document.createElement("img");
    img2.src = projectPath + "images/icons_nii/extension_csv.svg";
    img3.src = projectPath + "images/icons_nii/extension_csv.svg";
    img1.src = projectPath + "images/icons_nii/extension_csv.svg";
    let t1 = document.createTextNode("Дорожные знаки");
    btn1.appendChild(img1);
    btn1.appendChild(t1);
    btn1.onclick = function(){ downloadCSVSigns(dist.properties.name)};
    let btn2 = document.createElement("div");
    btn2.className = "line";
    let t2 = document.createTextNode("Точечная");
    btn2.appendChild(img2);
    btn2.appendChild(t2);
    btn2.onclick = function(){ downloadCSVPoints(dist.properties.name)};
    let btn3 = document.createElement("div");
    btn3.className = "line";
    let t3 = document.createTextNode("Линейная");
    btn3.appendChild(img3);
    btn3.appendChild(t3);
    btn3.onclick = function(){ downloadCSVLines(dist.properties.name)};
    box.appendChild(h4);
    box.appendChild(btn1);
    box.appendChild(btn2);
    box.appendChild(btn3);

    table.className = "segment";
    let green = document.createElement("span");
    green.className = "green";
    let orange = document.createElement("span");
    orange.className = "orange";
    let red = document.createElement("span");
    red.className = "red";
    if (cnt) {
        let n = 0;
        let number = 0;
        let string = "";
        for (let c in cnt) {
            (c === "Проектируемый" ? orange.innerHTML = cnt[c] :
                (c === "Демонтируемый" ? red.innerHTML = cnt[c] :
                    n += parseInt(cnt[c])));
        }
        green.innerHTML = n;
        if ((orange.textContent) && (red.textContent)) {
            number = n + parseInt(orange.textContent) + parseInt(red.textContent);
            string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML + " + " + red.outerHTML;
        } else if (orange.textContent) {
            number = n + parseInt(orange.textContent);
            string = number + " шт: " + green.outerHTML + " + " + orange.outerHTML;
        } else if (red.textContent) {
            number = n + parseInt(red.textContent);
            string = number + " шт: " + green.outerHTML + " + " + red.outerHTML;
        } else {
            string = green.outerHTML + " шт.";
        }
        table.innerHTML =
            "<tbody>" +
            "<tr>" +
            "<th>Улицы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Сегменты:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Знаки:</th>" +
            "<td>" + string + "</td>" +
            "</tr>" +
            "</tbody>";
    } else {
        table.innerHTML =
            "<tbody>" +
            "<tr>" +
            "<th>Улицы:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Сегменты:</th>" +
            "<td></td>" +
            "</tr>" +
            "<tr>" +
            "<th>Знаки:</th>" +
            "<td>Информация отсутсвует</td>" +
            "</tr>" +
            "</tbody>";
    }
    distPopupBox.appendChild(caption);
    distPopupBox.appendChild(table);
    distPopupBox.appendChild(box);
}